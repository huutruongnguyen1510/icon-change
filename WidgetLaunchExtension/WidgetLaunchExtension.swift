//
//  WidgetLaunchExtension.swift
//  WidgetLaunchExtension
//
//  Created by Huu Truong Nguyen on 10/13/21.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    public typealias Entry = WidgetContent
    
    func placeholder(in context: Context) -> WidgetContent {
        WidgetContent(date: Date(), arrayIconObj: [], size: context.displaySize)
    }
    
    func getSnapshot(in context: Context, completion: @escaping (WidgetContent) -> ()) {
        getTimeline(in: context) { _ in
            let content = readContents(context: context)[0]
            let entry = WidgetContent(date: content.date,
                                      backgroundImage: content.backgroundImage,
                                      textIconColor: content.textIconColor,
                                      backgroundColor: content.backgroundColor,
                                      arrayIconObj: content.arrayIconObj,
                                      size: context.displaySize)
            completion(entry)
        }
        
        let content = readContents(context: context)[0]
        let entry = WidgetContent(date: content.date,
                                  backgroundImage: content.backgroundImage,
                                  textIconColor: content.textIconColor,
                                  backgroundColor: content.backgroundColor,
                                  arrayIconObj: content.arrayIconObj,
                                  size: context.displaySize)
        completion(entry)
    }
    
    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        let entries: [WidgetContent] = readContents(context: context).map {
            WidgetContent(date: $0.date,
                          backgroundImage: $0.backgroundImage,
                          textIconColor: $0.textIconColor,
                          backgroundColor: $0.backgroundColor,
                          arrayIconObj: $0.arrayIconObj,
                          size: context.displaySize)
        }

        let timeline = Timeline(entries: entries, policy: .never)
        completion(timeline)
    }
    
    func readContents(context: Context) -> [WidgetContent] {
        var contents: [WidgetContent] = []
        print(context.family, context.displaySize)
        var pathComponent: String = ""
        switch context.family {
        case .systemMedium:
            pathComponent = PathContentWidgetType.medium
        case .systemLarge:
            pathComponent = PathContentWidgetType.large
        default: break
        }
        let archiveURL =
            FileManager.sharedContainerURL()
            .appendingPathComponent(pathComponent)
        print(">>> \(archiveURL)")
        
        let decoder = JSONDecoder()
        if let codeData = try? Data(contentsOf: archiveURL) {
            do {
                contents = try decoder.decode([WidgetContent].self, from: codeData)
            } catch {
                print("Error: Can't decode contents")
            }
        }
        return contents.isEmpty ? [WidgetContent(arrayIconObj: [])] : contents
    }
}

@main
struct WidgetApp: Widget {
    let kind: String = "WidgetApp"
    
    @Environment(\.widgetFamily) var widgetFamily
    
    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            WidgetView(data: entry)
        }
        .supportedFamilies([.systemMedium, .systemLarge])
        .configurationDisplayName("Launcher Widget")
        .description("Icon Pack - Themes & Shortcut")
    }
}

struct WidgetApp_Previews: PreviewProvider {
    static var previews: some View {
        WidgetView(data: WidgetContent(arrayIconObj: [IconInWidgetModel(appIcon: Data(),
                                                                        appName: "123",
                                                                        appRelateScheme: "!23",
                                                                        launcherType: .App)]))
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}


struct WidgetView: View {
    let data: Provider.Entry
    
    var gridItemLayout = [GridItem(.flexible()),
                          GridItem(.flexible()),
                          GridItem(.flexible()),
                          GridItem(.flexible())]
    
    @Environment(\.widgetFamily) var widgetFamily
    
    var body: some View {
        ZStack {
            if let dataImage = data.backgroundImage {
                Image(uiImage: UIImage(data: dataImage) ?? UIImage())
                    .resizable()
            }
            
            VStack {
                if data.arrayIconObj.count > 0 {
                    LazyVGrid(columns: gridItemLayout, spacing: 10) {
                        ForEach((0...data.arrayIconObj.count - 1), id: \.self) {
                            ButtonView(title: data.arrayIconObj[$0].appName,
                                       scheme: data.arrayIconObj[$0].appRelateScheme,
                                       appIcon: data.arrayIconObj[$0].appIcon,
                                       textIconColor: data.textIconColor,
                                       size: data.size,
                                       launcherType: data.arrayIconObj[$0].launcherType)
                        }
                    }
                    .padding([.leading, .trailing, .top], /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                    Spacer()
                } else {
                    VStack(alignment: .center, spacing: 0, content: {
                        Text("You haven't configured this widget yet")
                            .font(Font.custom("Nunito-SemiBoldItalic", size: 12))
                            .foregroundColor(.white)
                    })
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                }
            }
        }.background(LinearGradient(
                        gradient: Gradient(colors: [data.arrayIconObj.count > 0 ? Color(data.backgroundColor.color.0) : Color("323232"), data.arrayIconObj.count > 0 ? Color(data.backgroundColor.color.1) : Color("323232")]), startPoint: .top, endPoint: .bottom))
        
    }
}

struct ButtonView: View {
    var title: String
    var scheme: String
    var appIcon: Data
    var textIconColor: ColorLauncherType
    var size: CGSize
    var launcherType: LauncherType

    @Environment(\.widgetFamily) var widgetFamily

    var body: some View {
        let spacing: CGFloat = 10
        let totalWidth: CGFloat = size.width - 5*spacing
        let widthPerItem: CGFloat = totalWidth/4.0

        let isWidgetMedium: Bool = widgetFamily == .systemMedium
        let heightPerItem: CGFloat = isWidgetMedium
            ? (size.height - 3*spacing)/2
            : (size.height - 5*spacing)/4

        let heightImage = 50*size.width/360
        
        let firstColor = textIconColor == ColorLauncherType.none ? .black : textIconColor.color.0
        let secondColor = textIconColor == ColorLauncherType.none ? .black : textIconColor.color.1

        let urlString: String = scheme
        
        HStack {
            Link(destination: URL(string: urlString)!, label: {
                VStack(alignment: .center, spacing: 4, content: {
                    Image(uiImage: UIImage(data: appIcon) ?? UIImage())
                        .resizable()
                        .scaledToFit()
                        .frame(width: heightImage, height: heightImage, alignment: .center)
                        .foregroundColor(.clear)
                        .cornerRadius(10.0)
                    Text(title)
                        .font(Font.custom("Nunito-SemiBold", size: 10.5))
                        .gradientForeground(colors: [Color(firstColor), Color(secondColor)])
                        .lineLimit(1)
                        .multilineTextAlignment(.center)
                })
                .frame(width: widthPerItem, height: heightPerItem, alignment: .center)
            })
        }
    }
}

extension View {
    public func gradientForeground(colors: [Color]) -> some View {
        self.overlay(LinearGradient(gradient: .init(colors: colors),
                                    startPoint: .top,
                                    endPoint: .bottom))
            .mask(self)
    }
}

extension String {

    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

