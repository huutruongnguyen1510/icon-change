////
////  AdmobNewNativeView.swift
////  SuperCleaner
////
////  Created by Thanh Cong on 12/11/19.
////  Copyright © 2019 Thanh Cong. All rights reserved.
////
//
//import UIKit
//import GoogleMobileAds
//
//class AdmobNewNativeView: UIView {
//    
//    @IBOutlet weak var nativeAdView: GADUnifiedNativeAdView!
//
//    static func createNative() -> AdmobNewNativeView{
//        let nibObjects = Bundle.main.loadNibNamed("AdmobNewNativeView", owner: nil, options: nil)
//        let adView = nibObjects?.first as? AdmobNewNativeView
//        return adView!
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        nativeAdView.backgroundColor = UIColor.white
//        backgroundColor = UIColor.clear
//    }
//    
//    func setupHeader(nativeAd: GADUnifiedNativeAd) {
//        nativeAdView.nativeAd = nativeAd
//        
//        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
//        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
//        
//        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
//        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
//        
//        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
//        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
//        
//        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
//        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
//        nativeAdView.callToActionView?.isUserInteractionEnabled = false
//    }
//}
