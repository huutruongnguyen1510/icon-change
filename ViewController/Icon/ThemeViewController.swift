//
//  ThemeViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/4/21.
//

import UIKit
import FSPagerView
import CHIPageControl
import RxSwift
import RxCocoa
import Toast_Swift

class ThemeViewController: UIViewController {
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var pageControl: CHIPageControlAji!
    @IBOutlet private weak var buttonView: UIButton!
    @IBOutlet private weak var lockImageView: UIImageView!
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet private weak var pagerView: FSPagerView!{
        didSet {
            let nib = UINib(nibName: "ThemeSplashCell", bundle: nil)
            self.pagerView.register(nib,forCellWithReuseIdentifier: "ThemeSplashCell")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = 2
        setupButton()
        // Do any additional setup after loading the view.
    }
    
    // MARK: Variables
    enum StatusView: String, CaseIterable {
        case installIcon = "Install Icon"
        case saveWallPaper = "Save Wallpaper"
    }
    
    private var imageIsSaving: Bool = false
    var style = ToastStyle()
    var theme: ThemeType = .theme1
    private var statusArray: [StatusView] = StatusView.allCases
    private var statusSelected: StatusView = .installIcon {
        didSet {
            buttonView.setTitle(statusSelected.rawValue, for: .normal)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func setupButton() {
        backButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            })
        .disposed(by: rx.disposeBag)
        
        buttonView.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                guard let self = self else { return }
                switch self.statusSelected {
                case .installIcon:
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "InstallIconViewController") as! InstallIconViewController
                    vc.theme = self.theme
                    self.navigationController?.pushViewController(vc, animated: true)
                    //AdmobManager.shared.logEvent()
                case .saveWallPaper:
                    guard !self.imageIsSaving, let image = self.theme.wallpaper else { return }
                    self.imageIsSaving = true
                    UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                }
            })
            .disposed(by: rx.disposeBag)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            self.view.makeToast("This is a piece of toast", duration: 3.0, position: .bottom, style: style)
//            Toast(text: "Saved to the album", delay: 0, duration: 3).show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                self.imageIsSaving = false
            })
        }
    }
}

// MARK: FSPagerViewDataSource
extension ThemeViewController: FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return statusArray.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "ThemeSplashCell", at: index) as! ThemeSplashCell
        cell.bind(type: theme, statusView: statusArray[index])
        return cell
    }
}

// MARK: FSPagerViewDelegate
extension ThemeViewController: FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        pageControl.set(progress: targetIndex, animated: true)
        statusSelected = statusArray[targetIndex]
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        pageControl.set(progress: pagerView.currentIndex, animated: true)
        statusSelected = statusArray[pagerView.currentIndex]
    }
}
