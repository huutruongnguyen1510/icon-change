//
//  ViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 9/29/21.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var collectionViewIcon:UICollectionView!
    @IBOutlet weak var collectionViewTheme:UICollectionView!
    @IBOutlet weak var collectionViewToolbar:UICollectionView!
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet weak var lbName:UILabel!
    @IBAction func helpAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IconTutorialViewController") as! IconTutorialViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
        //dismiss(animated: true)
    }
    @IBAction func editAction() {
        if statusViewMyIcon == .done {
            statusViewMyIcon = .edit
        }
        else {
            statusViewMyIcon = .done
        }
        collectionViewIcon.reloadData()
    }
    
    private let iconMaterials: [ThemeType] = ThemeType.allCases
    var checkTab = 0
    var isChooseIcon = true
    private var arrIcon: [IconObj] = []
    private var statusViewMyIcon: StatusViewMyIcon = .edit {
        didSet {
            editButton.do {
                $0.setTitle(statusViewMyIcon.rawValue, for: .normal)
                $0.setTitleColor(statusViewMyIcon == .edit ? .purple : .lightCoral, for: .normal)
            }
        }
    }
    
    enum StatusViewMyIcon: String {
        case edit = "Edit"
        case done = "Done"
    }
    
    private func deleteIcon(index: Int) {
        arrIcon[index].deleteIcon()
        arrIcon.remove(at: index)
        collectionViewIcon.performBatchUpdates({
            collectionViewIcon.deleteItems(at: [IndexPath(item: index, section: 0)])
        }, completion: { _ in
            self.collectionViewIcon.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        arrIcon.removeAll()
        arrIcon = iconManager.getAllIcon()
        statusViewMyIcon = .edit
        collectionViewIcon.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewIcon.register(UINib(nibName: "MyIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "MyIconCLVCell")
        collectionViewIcon.register(UINib(nibName: "PlusCLVCell", bundle: nil), forCellWithReuseIdentifier: "PlusCLVCell")
        collectionViewTheme.register(UINib(nibName: "ThemeCLVCell", bundle: nil), forCellWithReuseIdentifier: "ThemeCLVCell")
        collectionViewToolbar.register(UINib(nibName: "ToolbarCLVCell", bundle: nil), forCellWithReuseIdentifier: "ToolbarCLVCell")
        collectionViewTheme.backgroundColor = .clear
        collectionViewIcon.backgroundColor = .clear
        collectionViewToolbar.backgroundColor = .clear
        collectionViewToolbar.isScrollEnabled = false
        collectionViewToolbar.layer.borderWidth = 3
        collectionViewToolbar.layer.borderColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
        collectionViewToolbar.layer.cornerRadius = 10
        if isChooseIcon {
            collectionViewTheme.isHidden = true
        }
        else {
            collectionViewIcon.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

}

extension HomeViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewToolbar {
            checkTab = indexPath.item
            collectionViewToolbar.reloadData()
            if indexPath.item == 0 {
                collectionViewTheme.isHidden = true
                collectionViewIcon.isHidden = false
                imgView.isHidden = false
                lbName.text = "My icons"
            }
            else {
                collectionViewTheme.isHidden = false
                collectionViewIcon.isHidden = true
                imgView.isHidden = true
                lbName.text = "Themes"
            }
        }
        else if collectionView == collectionViewIcon {
            if indexPath.item == arrIcon.count {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "IconViewController") as! IconViewController

                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                if statusViewMyIcon == .edit {
                    print("appRelate urlScheme: ", arrIcon[indexPath.item].appRelateScheme)
                    if let url = URL(string: arrIcon[indexPath.item].appRelateScheme) {
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url)
                        }
                    }
                }
                else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "IconViewController") as! IconViewController

                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                    vc.iconObj = arrIcon[indexPath.item]
                }
            }
            //self.present(vc, animated: true)
        }
        else if collectionView == collectionViewTheme {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ThemeViewController") as! ThemeViewController
            vc.theme = iconMaterials[indexPath.item]
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
            //self.present(vc, animated: true)
        }
    }
}

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewToolbar {
            return 2
        }
        else if collectionView == collectionViewIcon {
            return arrIcon.count + 1
        }
        else {
            return iconMaterials.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewToolbar {
            let cell = collectionViewToolbar.dequeueReusableCell(withReuseIdentifier: "ToolbarCLVCell", for: indexPath) as! ToolbarCLVCell
            cell.backgroundColor = .clear
    //        cell.imgView.image = #imageLiteral(resourceName: "quiz-bg").imageResize(sizeChange: CGSize(width: cell.frame.width, height: cell.frame.height))
            cell.layer.cornerRadius = 10
            cell.contentView.backgroundColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
            cell.lbName.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            if checkTab == indexPath.item {
                cell.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.lbName.textColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
            }
            if indexPath.item == 0 {
                cell.lbName.text = "My Icons"
            }
            else {
                cell.lbName.text = "Themes"
            }
            return cell
        }
        else if collectionView == collectionViewIcon {
            if indexPath.item == arrIcon.count {
                let cell = collectionViewIcon.dequeueReusableCell(withReuseIdentifier: "PlusCLVCell", for: indexPath) as! PlusCLVCell
                cell.layer.cornerRadius = 10
                return cell
            }
            else {
                let cell = collectionViewIcon.dequeueReusableCell(withReuseIdentifier: "MyIconCLVCell", for: indexPath) as! MyIconCLVCell
                cell.labelName.text = arrIcon[indexPath.item].appName
                //cell.imgView.layer.cornerRadius = 10
                cell.editView.layer.cornerRadius = 10
                let imageData = arrIcon[indexPath.item].appIcon.photo
                let image = UIImage(data: imageData)
                if image == nil {

                }
                else {
                    cell.imgView.image = image!.scaledToSize(CGSize(width: 60, height: 60))
                }
                if statusViewMyIcon == .edit {
                    cell.closeButton.isHidden = true
                    cell.editView.isHidden = true
                    cell.editImgView.isHidden = true
                    return cell
                }
    //            cell.editButton.rx.tap
    //                .subscribe(onNext: { [unowned self] in
    //
    ////                    self.handleEdit?(self.iconObj)
    //                })
    //                .disposed(by: rx.disposeBag)
//                cell.editButton.rx.tap
//                    .subscribe(onNext: { [unowned self] in
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let vc = storyboard.instantiateViewController(withIdentifier: "IconViewController") as! IconViewController
//                        vc.modalPresentationStyle = .fullScreen
//                        self.navigationController?.pushViewController(vc, animated: true)
//                        vc.iconObj = arrIcon[indexPath.item]
//                        //self.handleEdit?(self.iconObj)
//                    })
//                    .disposed(by: rx.disposeBag)
                cell.closeButton.rx.tap
                    .subscribe(onNext: { [unowned self] in
                        let alert = UIAlertController(title: "Are you sure want to delete?", message: nil, preferredStyle: .alert).then {
                            $0.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                            $0.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                                self.deleteIcon(index: indexPath.item)
                            }))
                        }
                        if let popoverController = alert.popoverPresentationController {
                            popoverController.sourceView = self.view //to set the source of your alert
                            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                        }
                        self.present(alert, animated: true, completion: nil)
    //                    self.handleDelete?()
                    })
                    .disposed(by: rx.disposeBag)
                cell.closeButton.isHidden = false
                cell.editView.isHidden = false
                cell.editImgView.isHidden = false
        //        cell.imgView.image = #imageLiteral(resourceName: "quiz-bg").imageResize(sizeChange: CGSize(width: cell.frame.width, height: cell.frame.height))
                return cell
                //cell.backgroundColor = .clear
            }
        }
        else {
            let cell = collectionViewTheme.dequeueReusableCell(withReuseIdentifier: "ThemeCLVCell", for: indexPath) as! ThemeCLVCell
            cell.layer.cornerRadius = 10
            cell.backgroundColor = .clear
            let image = iconMaterials[indexPath.item].imageTheme
            cell.imgView.image = image?.scaledToSize(CGSize(width: cell.frame.width, height: cell.frame.height))
    //        cell.imgView.image = #imageLiteral(resourceName: "quiz-bg").imageResize(sizeChange: CGSize(width: cell.frame.width, height: cell.frame.height))
            return cell
        }
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewToolbar {
            return CGSize(width: 100, height: 47)
        }
        else if collectionView == collectionViewIcon {
            return CGSize(width: 60, height: 100)
        }
        else {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: collectionViewTheme.frame.width/3 - 15 , height: UIScreen.main.bounds.height/3.5)
            }
            return CGSize(width: collectionViewTheme.frame.width/3 - 15, height: UIScreen.main.bounds.height/3)
        }
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            return CGSize(width: UIScreen.main.bounds.width/2 - 60 , height: UIScreen.main.bounds.width/2 - 80)
//        }
//        return CGSize(width: UIScreen.main.bounds.width - 100 , height: UIScreen.main.bounds.width - 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewIcon {
            return 15
        }
        else {
            return 15
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewToolbar {
            return 0
        }
        else if collectionView == collectionViewIcon {
            return 15
        }
        else {
            return 15
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}
