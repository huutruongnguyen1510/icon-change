//
//  InstallIconViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/8/21.
//

import UIKit
import RxSwift
import RxCocoa

class InstallIconViewController: UIViewController {
    // MARK: IBOutlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Variables
    var theme: ThemeType = .theme1
    var relateAppsRespectively: [RelateAppModel?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
        bindViewModel()
        
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.register(InstallIconCell.self)
    }
    
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupButton() {
        backButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            })
        .disposed(by: rx.disposeBag)
        
        tableView.do {
            $0.delegate = self
            $0.dataSource = self
            $0.register(InstallIconCell.self)
        }
    }

    enum AppOrder: Int, CaseIterable {
        case Facetime = 0
        case Calendar
        case Photo
        case Mail
        case Maps
        case Reminders
        case Notes
        case Stock
        case News
        case Books
        case AppStore
        case Podcasts
        case AppleTV
        case Health
        case Home
        case Wallet
        case Settings
        case Facebook
        case Instagram
        case Messenger
        case Twitter
        case Tiktok
        
        var appName: String {
            switch self {
            case .Facetime:
                return "FaceTime (Video)"
            case .Calendar:
                return "Calendar"
            case .Photo:
                return "Photos"
            case .Mail:
                return "Mail"
            case .Maps:
                return "Maps"
            case .Reminders:
                return "Reminders"
            case .Notes:
                return "Notes"
            case .Stock:
                return "Stocks"
            case .News:
                return "News"
            case .Books:
                return "iBooks"
            case .AppStore:
                return "App Store"
            case .Podcasts:
                return "Podcasts (Browse)"
            case .AppleTV:
                return "Apple TV"
            case .Health:
                return "Health"
            case .Home:
                return "Home"
            case .Wallet:
                return "Wallet"
            case .Settings:
                return "Settings"
            case .Facebook:
                return "Facebook"
            case .Instagram:
                return "Instagram"
            case .Messenger:
                return "Messenger"
            case .Twitter:
                return "Twitter"
            case .Tiktok:
                return "Tik Tok"
            }
        }
    }
    
    func bindViewModel() {
        let relateApps = setupRelateApps()
        for i in theme.iconMaterials.indices {
            if let appOrder = AppOrder.allCases.first(where: { $0.rawValue == i }),
               let relateApp = relateApps.first(where: { $0.appName == appOrder.appName }) {
                relateAppsRespectively.append(RelateAppModel(appName: relateApp.appName,
                                                             scheme: relateApp.scheme))
            } else {
                relateAppsRespectively.append(nil)
            }
        }
        
        tableView.reloadData()
    }
    
    private func setupRelateApps() -> [RelateAppModel] {
        do {
            if let file = Bundle.main.url(forResource: "data", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print(object)
                    return []
                } else if let object = json as? [Any] {
                    // json is an array
                    //                    print(object)
                    do {
                        let json = try JSONSerialization.data(withJSONObject: object)
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let decodeRelateApps = try decoder.decode([RelateAppModel].self, from: json)
                        
                        return decodeRelateApps.filter({ (relateApp) -> Bool in
                            if let url = URL(string: relateApp.scheme),
                               UIApplication.shared.canOpenURL(url),
                               UIImage().getUncachedImage(named: relateApp.appName) != nil {
                                return true
                            }
                            return false
                        })
                    } catch {
                        print(error)
                        return []
                    }
                } else {
                    print("JSON is invalid")
                    return []
                }
            } else {
                print("no file")
                return []
            }
        } catch {
            print(error.localizedDescription)
            return []
        }
    }
}

// MARK: -
extension InstallIconViewController {
    
}

// MARK: - TableView DataSource
extension InstallIconViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theme.iconMaterials.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeue(InstallIconCell.self).with {
            $0.bind(iconTheme: theme.iconMaterials[indexPath.row], appRelated: relateAppsRespectively[indexPath.row])
            $0.handleInstall = { [unowned self] in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CreateThemeViewController") as! CreateThemeViewController
                vc.iconApp = self.theme.iconMaterials[indexPath.row]
                vc.nameApp = AppOrder.allCases[indexPath.row].appName
                vc.relateAppsRespectively = self.relateAppsRespectively[indexPath.row]
                let appOrders = AppOrder.allCases
                vc.nameApp = appOrders[indexPath.row].appName
                self.navigationController?.pushViewController(vc, animated: true)
                //AdmobManager.shared.logEvent()
            }
//            $0.handleSelectAppRelate = { [unowned self] in
//                let vc = RelatedAppViewController()
//                vc.iconImage = self.theme.iconMaterials[indexPath.row]
//                vc.handleSelectRelatedApp = { relatedApp in
//                    self.relateAppsRespectively[indexPath.row] = relatedApp
//                    tableView.reloadData()
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
//                //AdmobManager.shared.logEvent()
//            }
        }
    }
}

// MARK: - TableView Delegate
extension InstallIconViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        settingTrigger.onNext(settingTypes[indexPath.row])
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (isIPad ? 180 : 169)*heightRatio
    }
}

