//
//  PreviewViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/12/21.
//

import UIKit
import RxSwift
import RxCocoa

class PreviewViewController: BaseViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var iconAppImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackground()
        // Do any additional setup after loading the view.
    }
    
    // MARK: Variables
    var iconApp: UIImage?
    var appName: String?

    override func setupUI() {
        backButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            })
            .disposed(by: rx.disposeBag)
        
        //previewImage.image = isIPad ? #imageLiteral(resourceName: "preview_iPad") : #imageLiteral(resourceName: "preview_iPhone")
        iconAppImageView.image = iconApp
        appNameLabel.text = appName
    }

    func createBackground(){
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = #imageLiteral(resourceName: "Home")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
