//
//  CreateThemeViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/11/21.
//

import UIKit
import RxSwift
import RxCocoa
import Swifter

final class CreateThemeViewController: BaseViewController {
    // MARK: IBOutlets
//    @IBOutlet private weak var shadowContainerView: UIView!
//    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var backButton: UIButton!
//    @IBOutlet private weak var shadowIconThemeView: UIView!
    @IBOutlet private weak var iconThemeImageView: UIImageView!
//    @IBOutlet private weak var containerAppRelateView: UIView!
    @IBOutlet private weak var appRelateImageView: UIImageView!
    @IBOutlet private weak var createButton: UIButton!
    @IBOutlet private weak var labelAppName: UILabel!
    
    // MARK: Variables
    var iconApp: UIImage?
    var server: HttpServer?
    var relateAppsRespectively: RelateAppModel?
    var nameApp = ""
    
    override func setupUI() {
        backButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            })
        .disposed(by: rx.disposeBag)
        labelAppName.text = nameApp
//        shadowContainerView.shadowView(color: .black, alpha: 0.05, x: 0, y: 5, blur: 5)
//
//        containerView.cornerRadius = 30*heightRatio
//
//        shadowIconThemeView.shadowView(alpha: 0.15, x: 2, y: 2, blur: 4)
//
        iconThemeImageView.image = iconApp
//
//        containerAppRelateView.do {
//            $0.cornerRadius = 19*heightRatio
//            $0.border(color: UIColor.hex("C9C9C9"), width: 0.5*heightRatio)
//        }
        
        if let appName = relateAppsRespectively?.appName {
            appRelateImageView.image = UIImage().getUncachedImage(named: appName)
        }
//
//        containerTextFieldView.do {
//            $0.cornerRadius = 16*heightRatio
//            $0.backgroundColor = .hex("F6F6F6")
//        }
//
//        enterIconNameTextField.do {
//            //$0.font = .bold(size: 12)
//            $0.placeholder = "Enter icon name"
//        }
        
//        createButton.do {
//            //$0.titleLabel?.font = .extraBold(size: 16)
//            $0.cornerRadius = 19*heightRatio
//            self.createButton.backgroundColor = .hex("E3E3E3")
//            self.createButton.setTitleColor(.hex("BBBBBB"), for: .normal)
//            self.createButton.isUserInteractionEnabled = false
//        }
    }

    override func bindViewModel() {
//        enterIconNameTextField.rx.controlEvent(.editingChanged)
//            .map { [unowned self] in self.enterIconNameTextField.text ?? "" }
//            .subscribe(onNext: { [unowned self] in
//                self.createButton.backgroundColor = $0.isEmpty ? .hex("E3E3E3") : .lightCoral
//                self.createButton.setTitleColor($0.isEmpty ? .hex("BBBBBB") : .white, for: .normal)
//                self.createButton.isUserInteractionEnabled = !$0.isEmpty
//            })
//            .disposed(by: rx.disposeBag)
        
        createButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                if let iconApp = self.iconApp,
                    let nameApp = self.labelAppName.text,
                    let relateAppsRespectively = self.relateAppsRespectively {
                    self.showShortcutScreen(forDeepLink: relateAppsRespectively.scheme, iconApp: iconApp, nameApp: nameApp)
                    let iconObj = IconObj()
                    iconObj.id = Common.generateID()
                    iconObj.createDate = Date().timeIntervalSince1970.rounded()
                    iconObj.appIcon = ImageData(photo: iconApp)
                    iconObj.appName = nameApp
                    iconObj.appRelateName = relateAppsRespectively.appName
                    iconObj.appRelateScheme = relateAppsRespectively.scheme
                    iconObj.createIconType = .photoIcon
                    iconObj.photoIcon = ImageData(photo: iconApp)
                    iconObj.saveIcon(true)
                    self.navigationController?.popToRootViewController(animated: true)
                }
            })
            .disposed(by: rx.disposeBag)
    }
    
    func showShortcutScreen(forDeepLink deepLink: String, iconApp: UIImage, nameApp: String) {
        server = HttpServer()
        
        let portId = Int.random(in: 1000..<9999)
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let str = String((0..<1).map{ _ in letters.randomElement()! })
        
        guard let deepLinkUrl = URL(string: deepLink) else {
            return
        }
        guard let shortcutUrl = URL(string: "http://localhost:\(portId)/\(str)") else {
            return
        }
        guard let iconData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }
        guard let pSplashData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }
        guard let lSplashData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }

        let iconBase64 = iconData.base64EncodedString()
        let pSplashBase64 = pSplashData.base64EncodedString()
        let lSplashBase64 = lSplashData.base64EncodedString()
        let html = Common.htmlFor(title: nameApp,
                           urlToRedirect: deepLinkUrl,
                           iconBase64: iconBase64,
                           pSplashBase64: pSplashBase64,
                           lSplashBase64: lSplashBase64)
        guard let base64 = html.data(using: .utf8)?.base64EncodedString() else {
            return
        }
        server?["/\(str)"] = { request in
            return .movedPermanently("data:text/html;base64,\(base64)")
        }
        try? server?.start(in_port_t(portId))
        print(shortcutUrl)
        UIApplication.shared.open(shortcutUrl)
    }
}
