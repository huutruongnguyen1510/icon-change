//
//  ColorViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/2/21.
//

import FlexColorPicker
import RealmSwift

class ColorViewController: DefaultColorPickerViewController {
    var pickedColor = #colorLiteral(red: 0.6813090444, green: 0.253660053, blue: 1, alpha: 1)
    var number = 0
    let realm = try! Realm()
    var didSendDataColor: ((UIColor) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedColor = pickedColor
        print(pickedColor)
        self.delegate = self
        //let color = UIColor(named: "#ffe700ff")
    }

    private func saveData(color: String) {
        let data = ColorModel()
        data.id = ColorModel().IncrementaID()
        data.color = color
        try! realm.write {
            realm.add(data)
        }
    }
    
    @IBAction func donePressed(_ sender: Any) {
        saveData(color: pickedColor.hexString)
        didSendDataColor!(pickedColor)
        dismiss(animated: true, completion: nil)
    }
}

extension ColorViewController: ColorPickerDelegate {
    func colorPicker(_: ColorPickerController, selectedColor: UIColor, usingControl: ColorControl) {
        pickedColor = selectedColor
    }

    func colorPicker(_: ColorPickerController, confirmedColor: UIColor, usingControl: ColorControl) {
        dismiss(animated: true, completion: nil)
    }
}
