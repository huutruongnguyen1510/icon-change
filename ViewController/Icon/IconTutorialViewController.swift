//
//  IconTutorialViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/19/21.
//

import UIKit
import CHIPageControl
import FSPagerView

class IconTutorialViewController: UIViewController {
    @IBOutlet private weak var backButton: UIButton!
    @IBAction func backAction() {
        self.dismiss(animated: true)
    }
    @IBOutlet private weak var pagerView: FSPagerView!{
        didSet {
            let nib = UINib(nibName: "IconTutorialCLVCell", bundle: nil)
            self.pagerView.register(nib,forCellWithReuseIdentifier: "IconTutorialCLVCell")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pagerView.isScrollEnabled = true
        iconTutorials = IconTutorialData.shared.menu
        // Do any additional setup after loading the view.
    }
    
    var iconTutorials: [TutorialModel] = [TutorialModel]()
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

// MARK: FSPagerViewDataSource
extension IconTutorialViewController: FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return iconTutorials.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "IconTutorialCLVCell", at: index) as! IconTutorialCLVCell
        cell.imgView.image = iconTutorials[index].image
        cell.content.text = iconTutorials[index].content
        cell.content.textColor = .black
        return cell
    }
}

// MARK: FSPagerViewDelegate
extension IconTutorialViewController: FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        let cell = pagerView.cellForItem(at: targetIndex) as! IconTutorialCLVCell
        cell.pageControl.set(progress: targetIndex, animated: true)
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        let cell = pagerView.cellForItem(at: pagerView.currentIndex) as! IconTutorialCLVCell
        cell.pageControl.set(progress: pagerView.currentIndex, animated: true)
    }
}
