//
//  RelatedAppViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/6/21.
//

import UIKit
import RxSwift
import RxCocoa

final class RelatedAppViewController: BaseViewController {
    // MARK: IBOutlets
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var containerSearchView: UIView!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var tableView: UITableView!
//    @IBAction func back() {
//        dismiss(animated: true)
//    }
    // MARK: Variables
    var iconImage: UIImage?
    var handleSelectRelatedApp: ((RelateAppModel) -> Void)?
    private var offSetY: CGFloat = 0
    private var relatedApps: [RelateAppModel] = []
    
    private var relatedAppsSearch: [RelateAppModel] = []

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //containerSearchView.circle()
        
//        offSetY = magicViewEdgeTableView.frame.maxY - tableView.frame.minY
//        tableView.contentInset.top = offSetY
//        tableView.setContentOffset(CGPoint(x: 0, y: -offSetY), animated: false)
    }
    
    func textFieldDesignWithLeftPadding(textField: UITextField, image: UIImage, padding:Int){
        let viewPadding = UIView(frame: CGRect(x: 0, y: 0, width: padding , height: Int(textField.frame.size.height)))
        let imageView = UIImageView (frame:CGRect(x: 0, y: 0, width: 21 , height: 21))
        imageView.center = viewPadding.center
        imageView.image  = image
        viewPadding.addSubview(imageView)
        textField.leftView = viewPadding
        textField.leftViewMode = .always
   }
    
    override func setupUI() {
        backButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.navigationController?.popViewController(animated: true)
            })
            .disposed(by: rx.disposeBag)
        
        searchTextField.do {
            $0.font = .boldSystemFont(ofSize: 14)
        }
        
        tableView.do {
            $0.register(UINib(nibName: "RelatedAppHeader", bundle: nil),
                        forHeaderFooterViewReuseIdentifier: "RelatedAppHeader")
            $0.register(RelatedAppCell.self)
            $0.dataSource = self
            $0.delegate = self
        }
        searchTextField.layer.borderWidth = 0.4
        searchTextField.cornerRadius = 15
        textFieldDesignWithLeftPadding(textField: searchTextField, image: #imageLiteral(resourceName: "search"), padding: 45)
        iconImageView.image = iconImage
        iconImageView.layer.borderWidth = 0.4
        iconImageView.layer.cornerRadius = 15
        
        searchTextField.rx.text
            .orEmpty
            .subscribe(onNext: { [unowned self] text in
                if text.isEmpty {
                    self.relatedAppsSearch = self.relatedApps
                } else {
                    self.relatedAppsSearch = self.relatedApps.filter { (app) -> Bool in
                        return app.appName.lowercased().contains(text.lowercased())
                    }
                }
                self.tableView.reloadData()
            })
            .disposed(by: rx.disposeBag)
        
    }
    
    override func bindViewModel() {
        setupRelateApps()
    }
//    override func viewWillAppear(_ animated: Bool) {
//
//    }
    
    private func setupRelateApps() {
        do {
            if let file = Bundle.main.url(forResource: "data", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
//                    print(object)
                    do {
                        let json = try JSONSerialization.data(withJSONObject: object)
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let decodeRelateApps = try decoder.decode([RelateAppModel].self, from: json)
                        //self.relatedApps = decodeRelateApps
                        self.relatedApps = decodeRelateApps.filter({ (relateApp) -> Bool in
                            if let url = URL(string: relateApp.scheme),
                                UIApplication.shared.canOpenURL(url),
                                UIImage().getUncachedImage(named: relateApp.appName) != nil {
                                return true
                            }
                            return false
                        })
                        self.relatedAppsSearch = self.relatedApps
                        tableView.reloadData()
                    } catch {
                        print(error)
                    }
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK: - tableView DataSource
extension RelatedAppViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        relatedAppsSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.dequeue(RelatedAppCell.self).then {
            $0.bind(type: relatedAppsSearch[indexPath.row])
        }
    }
}

// MARK: - tableView Delegate
extension RelatedAppViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        handleSelectRelatedApp?(relatedAppsSearch[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return isIPad ? 80 : 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        tableView.dequeueReusableHeaderFooterView(withIdentifier: RelatedAppHeader.className)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isIPad ? 45 : 35
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

