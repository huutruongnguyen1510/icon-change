//
//  WidgetTutorialViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/19/21.
//

import UIKit
import CHIPageControl
import FSPagerView

class WidgetTutorialViewController: UIViewController {
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var pageControl: CHIPageControlAji!
    @IBAction func backAction() {
        self.dismiss(animated: true)
    }
    @IBOutlet private weak var pagerView: FSPagerView!{
        didSet {
            let nib = UINib(nibName: "WidgetTutorialCLVCell", bundle: nil)
            self.pagerView.register(nib,forCellWithReuseIdentifier: "WidgetTutorialCLVCell")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = 4
        widgetTutorials = WidgetTutorialData.shared.menu
        // Do any additional setup after loading the view.
    }
    
    // MARK: Variables
    enum StatusView: String, CaseIterable {
        case installIcon = "Install Icon"
        case saveWallPaper = "Save Wallpaper"
    }
    
    var widgetTutorials: [TutorialModel] = [TutorialModel]()
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

// MARK: FSPagerViewDataSource
extension WidgetTutorialViewController: FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return widgetTutorials.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "WidgetTutorialCLVCell", at: index) as! WidgetTutorialCLVCell
        cell.imgView.image = widgetTutorials[index].image
        cell.content.text = widgetTutorials[index].content
        cell.content.textColor = .black
        return cell
    }
}

// MARK: FSPagerViewDelegate
extension WidgetTutorialViewController: FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        pageControl.set(progress: targetIndex, animated: true)
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        pageControl.set(progress: pagerView.currentIndex, animated: true)
    }
}
