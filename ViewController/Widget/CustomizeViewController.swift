//
//  CustomizeViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/12/21.
//

import UIKit
import Then
import Swifter

class CustomizeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var collectionViewWidget: UICollectionView!
    @IBOutlet weak var collectionViewToolEdit: UICollectionView!
    @IBOutlet private weak var previewButton: UIButton!
    @IBOutlet private weak var createWidgetButton: UIButton!
    @IBOutlet private weak var containerIconView: UIView!
//    @IBOutlet weak var iconButton: UIButton!
//    @IBOutlet weak var lbNameApp: UILabel!
//    @IBOutlet private weak var pleaseEnterLabel: UILabel!
    @IBOutlet private weak var enterTextIconTextField: UITextField!
    @IBOutlet private weak var enterWidgetNameTextField: UITextField!
    @IBAction func helpAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WidgetTutorialViewController") as! WidgetTutorialViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
//    @IBOutlet private weak var iconBorderImageView: UIImageView!
//    @IBOutlet private weak var enterPhotoIconImageView: UIImageView!
//    @IBOutlet private weak var selectAppButton: UIButton!
//    @IBOutlet private weak var pleaseEnterImageView: UIImageView!
    @IBOutlet weak var widgetImageView: UIImageView!
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: true)
    }
    
    @IBAction func previewAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PreviewViewController") as! PreviewViewController
        vc.appName = self.nameApp
        vc.iconApp = self.containerIconView.screenshot
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func selectAppAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RelatedAppViewController") as! RelatedAppViewController
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true)
        vc.handleSelectRelatedApp = { [unowned self] in
            self.appRelate = $0
            //self.enableButtonAction()
        }
    }
    
    @IBAction func createWidgetAction() {
        self.writeContent()
    }
    
    private func writeContent() {
        //guard isViewDidAppear else { return }
        if !isUpdate {
            widgetObj.id = Common.generateID()
            //widgetObj.isSelected = false
        }
        //widgetObj.isSelected = true
        widgetObj.widgetName = enterWidgetNameTextField.text ?? ""
        widgetObj.widgetType = self.familyWidgetType
        widgetObj.createDate = Date().timeIntervalSince1970.rounded()
        widgetObj.backgroundImage = ImageData(photo: widgetImageView.image ?? UIImage())
        
        if isUpdate {
            widgetObj.updateWidget()
        }
        else {
            widgetObj.saveWidget(true)
        }
        AppSettings.writeContents(widget: widgetObj)
        handleIconIsAdded?()
        self.navigationController?.popViewController(animated: true)
    }
    var backgroundWidget: UIImage = UIImage()
    var handleIconIsAdded: (() -> ())?
    var isUpdate = true
    private var camera: CameraHelper? = nil
    var widgetObj: WidgetObj = WidgetObj() //{
//        didSet {
//            if let widgetObj = WidgetObj {
//                nameApp = iconObj.appName
//                appRelate = RelateAppModel(appName: iconObj.appRelateName, scheme: iconObj.appRelateScheme)
//                fontSize = Float(iconObj.fontSize)
//                fontType = iconObj.fontType
//                iconBorder = iconObj.iconBorder
//                textColor = iconObj.textColor
////                self.pleaseEnterLabel.isHidden = true
////                self.pleaseEnterImageView.isHidden = true
////                enableButtonAction()
//            }
//        }
//    }
    private var backgroundImageSelected: BackgroundLauncherImageType? {
        didSet {
            widgetObj.backgroundImageSelected = backgroundImageSelected
            if !backgroundImageSelected.isNil {
                widgetImageView.image = widgetObj.widgetType == .Medium ? backgroundImageSelected?.mediumImage : backgroundImageSelected?.largeImage
                widgetObj.backgroundImage = ImageData(photo: widgetImageView.image ?? UIImage())
            } else {
                widgetImageView.image = nil
                widgetObj.backgroundImage = nil
            }
            enableSaveButton()
        }
    }
    
//    private var statusViewMyIcon: StatusViewMyIcon = .edit {
//        didSet {
//            editButton.setImage(statusViewMyIcon == .edit ? #imageLiteral(resourceName: "edit-2") : #imageLiteral(resourceName: "edit-2-selected"), for: .normal)
//            containerToolEditView.do {
//                $0.alpha = statusViewMyIcon == .edit ? 1 : 0.5
//                $0.isUserInteractionEnabled = statusViewMyIcon == .edit
//            }
//            collectionViewWidget.reloadData()
//        }
//    }
    
    private var colorLauncherTypes: [ColorLauncherType] = ColorLauncherType.allCases
    
    private var backgroundColorSelected: ColorLauncherType = .none {
        didSet {
            widgetObj.backgroundColor = backgroundColorSelected
//            containerWidgetView.gradient(firstColor: backgroundColorSelected.color.0,
//                                          secondColor: backgroundColorSelected.color.1,
//                                          startPoint: CGPoint(x: 0, y: 0),
//                                          endPoint: CGPoint(x: 0, y: 1))
            enableSaveButton()
        }
    }
    
    private var textColorSelected: ColorLauncherType = .none {
        didSet {
            widgetObj.textIconColor = textColorSelected
            collectionViewWidget.reloadData()
            enableSaveButton()
        }
    }
    
//    private var statusViewMyIcon: StatusViewMyIcon = .edit {
//        didSet {
//            editButton.setImage(statusViewMyIcon == .edit ? #imageLiteral(resourceName: "edit-2") : #imageLiteral(resourceName: "edit-2-selected"), for: .normal)
//            containerToolEditView.do {
//                $0.alpha = statusViewMyIcon == .edit ? 1 : 0.5
//                $0.isUserInteractionEnabled = statusViewMyIcon == .edit
//            }
//            collectionViewWidget.reloadData()
//        }
//    }
    
    var nameApp = ""
    var server: HttpServer?
    var textColor = ColorType.black
    var fontType = FontType.Nunito
    var fontName = FontType.Nunito.fontName
    var fontSize: Float = 40.0
    var iconBorder: BorderType?
    var checkTab = 0
    var isChooseFont = true
    var listSavedColor: [ColorModel] = [ColorModel]()
    var familyWidgetType: WidgetType = .Medium
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewWidget.layer.cornerRadius = 15
        if familyWidgetType == .Large {
            containerIconView.heightAnchor.constraint(equalTo: containerIconView.widthAnchor, multiplier: 1.0/1.0).isActive = true
        }
        else {
            containerIconView.heightAnchor.constraint(equalTo: containerIconView.widthAnchor, multiplier: 8.0/15.0).isActive = true
        }
        containerIconView.layer.cornerRadius = 15
        collectionViewWidget.layer.cornerRadius = 15
        //collectionViewToolEdit.roundCorners([.topLeft, .topRight], radius: 30)
        collectionViewToolEdit.register(UINib(nibName: "SizeColorFontCLVCell", bundle: nil), forCellWithReuseIdentifier: "SizeColorFontCLVCell")
        collectionViewToolEdit.register(UINib(nibName: "FontCLVCell", bundle: nil), forCellWithReuseIdentifier: "FontCLVCell")
        collectionViewToolEdit.register(UINib(nibName: "SizeIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "SizeIconCLVCell")
        collectionViewToolEdit.register(UINib(nibName: "IconCreateCLVCell", bundle: nil), forCellWithReuseIdentifier: "IconCreateCLVCell")
        collectionViewToolEdit.register(UINib(nibName: "BackgroundIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "BackgroundIconCLVCell")
        collectionViewWidget.register(UINib(nibName: "PlusCLVCell", bundle: nil), forCellWithReuseIdentifier: "PlusCLVCell")
        collectionViewWidget.register(UINib(nibName: "MyIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "MyIconCLVCell")
        collectionViewWidget.backgroundColor = .clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadFontColor(notification:)), name: Notification.Name("LOAD_FONT_COLOR"), object: nil)
        setupData()
        // Do any additional setup after loading the view.
    }
    
    func setupData() {
        enterWidgetNameTextField.text = widgetObj.widgetName//"\(widgetObj.widgetType.rawValue) - \()"
        backgroundImageSelected = widgetObj.backgroundImageSelected
        textColorSelected = widgetObj.textIconColor
        backgroundColorSelected = widgetObj.backgroundColor
        widgetImageView.image = backgroundWidget.scaledToSize(CGSize(width: containerIconView.frame.width, height: containerIconView.frame.height))
    }
    
//    private var iconObj: IconObj? {
//        didSet {
//            if let iconObj = iconObj {
////                createIconType = iconObj.createIconType
////                iconNameTextField.text = iconObj.appName
////                appRelate = RelateAppModel(appName: iconObj.appRelateName, scheme: iconObj.appRelateScheme)
////                fontSize = iconObj.fontSize
////                fontType = iconObj.fontType
////                iconBorder = iconObj.iconBorder
////                textColor = iconObj.textColor
//                enterTextIconTextField.text = iconObj.textIcon
//                enterPhotoIconImageView.image = iconObj.photoIcon
//                self.pleaseEnterLabel.isHidden = true
//                enableButtonAction()
//            }
//        }
//    }
    
    private var appRelate: RelateAppModel? //{
//        didSet {
//            if let appRelateName = appRelate?.appName {
//                selectAppButton.setImage(UIImage().getUncachedImage(named: appRelateName), for: .normal)
//            }
//            //containerAppRelateView.isHidden = appRelate.isNil
//        }
//    }
    
//    private func enableButtonAction() {
//        let haveTextIcon = !(enterTextIconTextField.text?.isEmpty ?? false)
//        let havePhotoIcon = !enterPhotoIconImageView.image.isNil
//        let haveAppRelated = appRelate != nil
//        let isEnable = (haveTextIcon || havePhotoIcon) && haveAppRelated
//        [previewButton, createWidgetButton].forEach {
//            $0?.alpha = isEnable ? 1 : 0.5
//            $0?.isUserInteractionEnabled = isEnable
//        }
//    }
    
    @objc func loadFontColor(notification not: Notification) {
        let item = not.object
        //enterTextIconTextField.textColor = item as? UIColor
        let image = UIImage(color: (item as? UIColor)!)
        widgetImageView.image = image?.scaledToSize(CGSize(width: widgetImageView.frame.width, height: widgetImageView.frame.height))
    }
    
    @objc private func editTapped() {
        //statusViewMyIcon = statusViewMyIcon == .edit ? .done : .edit
    }
    
    private func disableSaveButton() {
        createWidgetButton.do {
            $0.isUserInteractionEnabled = false
            $0.alpha = 0.4
        }
    }
    
    private func enableSaveButton() {
//        guard isViewDidAppear else { return }
        createWidgetButton.do {
            $0.isUserInteractionEnabled = true
            $0.alpha = 1
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CustomizeViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("editing")
    }
    //override var textInputMode: UITextInputMode?
}

extension CustomizeViewController: UICollectionViewDelegate{
    func presentAlert(_ alert: UIViewController, animated: Bool, completion: (() -> Void)?) {
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view //to set the source of your alert
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
            popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
        }
        present(alert, animated: animated, completion: completion)
    }
    
    private func actionAddPhoto() {
        self.view.endEditing(true)
        if camera == nil {
            camera = CameraHelper(delegate: self)
        }
        let alert = UIAlertController(title: "Select from:", message: nil, preferredStyle: .actionSheet).then {
            $0.addAction(UIAlertAction(title: "Direct camera shooting", style: .default, handler: { [unowned self]  (_) in
                self.camera?.getCameraOn(self, canEdit: false)
            }))
            $0.addAction(UIAlertAction(title: "Select a photo from an album", style: .default, handler: { [unowned self]  (_) in
                self.camera?.getPhotoLibraryOn(self, canEdit: false)
            }))
            
//            $0.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { [unowned self]  (_) in
//                backgroundImageSelected = nil
//                widgetImageView.image = nil
//                collectionViewToolEdit.reloadData()
//
//                widgetObj.backgroundImage = ImageData(photo: widgetImageView.image ?? UIImage())
//                enableSaveButton()
//            }))
            
            $0.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        }
        presentAlert(alert, animated: true, completion: nil)
    }
    
    private func saveIconForWidget(icon: IconObj) {
        if icon.id.isEmpty { // Save icon
            let saveIcon = icon.with {
                $0.id = Common.generateID()
                $0.createDate = Date().timeIntervalSince1970.rounded()
            }
            self.widgetObj.arrayIcon.append(saveIcon)
        } else { // Update icon
            if let indexIcon = self.widgetObj.arrayIcon.firstIndex(where: { $0.id == icon.id }) {
                self.widgetObj.arrayIcon[indexIcon] = icon
            }
        }
        self.collectionViewWidget.reloadData()
        enableSaveButton()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewWidget {
            if indexPath.item >= widgetObj.arrayIcon.count {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "IconWidgetViewController") as! IconWidgetViewController
                vc.handleSaveIconForWidget = { [unowned self] icon in
                    saveIconForWidget(icon: icon)
                }
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                print("appRelate urlScheme: ", widgetObj.arrayIcon[indexPath.item].appRelateScheme)
                if let url = URL(string: widgetObj.arrayIcon[indexPath.item].appRelateScheme) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
                }
            }
        }
        else {
            if indexPath.item == 0 {
                self.actionAddPhoto()
            }
        }
    }
        
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionViewWidget {
            return 1
        }
        return 4
    }
}

extension CustomizeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewToolEdit {
            return 1
        }
        return widgetObj.arrayIcon.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewWidget {
            if indexPath.item == widgetObj.arrayIcon.count {
                let cell = collectionViewWidget.dequeueReusableCell(withReuseIdentifier: "PlusCLVCell", for: indexPath) as! PlusCLVCell
                cell.layer.cornerRadius = 10
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyIconCLVCell", for: indexPath) as! MyIconCLVCell
            let imageData = widgetObj.arrayIcon[indexPath.item].appIcon.photo
            let image = UIImage(data: imageData)
            if image == nil {

            }
            else {
                cell.imgView.image = image!.scaledToSize(CGSize(width: 60, height: 60))
            }
            cell.labelName.text = widgetObj.arrayIcon[indexPath.item].appName
            cell.closeButton.isHidden = true
            cell.editView.isHidden = true
            cell.editImgView.isHidden = true
            //cell.editButton.isHidden = true
            cell.editImgView.isHidden = true
            return cell
        }
        if indexPath.section % 2 == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCreateCLVCell", for: indexPath) as! IconCreateCLVCell
            cell.lbName.textColor = .gray
            if indexPath.section == 0 {
                cell.lbName.text = "Color"
            }
            else {
                cell.lbName.text = "Images"
            }
            return cell
        }
        else if indexPath.section == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SizeColorFontCLVCell", for: indexPath) as! SizeColorFontCLVCell
            cell.isWidget = true
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BackgroundIconCLVCell", for: indexPath) as! BackgroundIconCLVCell
        if indexPath.item == 0 {
            cell.layer.cornerRadius = 10
            cell.imgView.image = #imageLiteral(resourceName: "plus-widget")
            cell.backgroundColor = #colorLiteral(red: 0.8861995339, green: 0.8863241673, blue: 0.8861603141, alpha: 1)
            return cell
        }
        cell.backgroundColor = .clear
        return cell
    }
}

extension CustomizeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewToolEdit {
            if indexPath.section % 2 == 0 {
                return CGSize(width: collectionView.frame.width - 40, height: 30)
            }
            else if indexPath.section == 1 {
                return CGSize(width: collectionView.frame.width - 40, height: 60)
            }
            else if indexPath.section == 3 {
                return CGSize(width: 60, height: 60)
            }
            else {
                if checkTab == 1 {
                    return CGSize(width: collectionView.frame.width, height: 40)
                }
                return CGSize(width: collectionView.frame.width, height: 60)
            }
        }
        else {
            return CGSize(width: 60, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionViewWidget {
            return UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        }
        else {
            if section % 2 == 0 {
                return UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
            }
            else if section == 3 {
                return UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
            }
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewWidget {
            return 10
        }
        return 0
    }
}


extension CustomizeViewController {
    func showShortcutScreen(forDeepLink deepLink: String, iconApp: UIImage, nameApp: String) {
        server = HttpServer()
        
        let portId = Int.random(in: 1000..<9999)
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let str = String((0..<1).map{ _ in letters.randomElement()! })
        
        guard let deepLinkUrl = URL(string: deepLink) else {
            return
        }
        guard let shortcutUrl = URL(string: "http://localhost:\(portId)/\(str)") else {
            return
        }
        guard let iconData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }
        guard let pSplashData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }
        guard let lSplashData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }

        let iconBase64 = iconData.base64EncodedString()
        let pSplashBase64 = pSplashData.base64EncodedString()
        let lSplashBase64 = lSplashData.base64EncodedString()
        let html = Common.htmlFor(title: nameApp,
                           urlToRedirect: deepLinkUrl,
                           iconBase64: iconBase64,
                           pSplashBase64: pSplashBase64,
                           lSplashBase64: lSplashBase64)
        guard let base64 = html.data(using: .utf8)?.base64EncodedString() else {
            return
        }
        server?["/\(str)"] = { request in
            return .movedPermanently("data:text/html;base64,\(base64)")
        }
        try? server?.start(in_port_t(portId))
        print(shortcutUrl)
        UIApplication.shared.open(shortcutUrl)
    }
}

extension CustomizeViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            backgroundImageSelected = nil
            widgetImageView.image = pickedImage.scaledToSize(CGSize(width: widgetImageView.frame.width, height: widgetImageView.frame.height))
            backgroundColorSelected = .none
            collectionViewWidget.reloadData()
            
            enableSaveButton()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
