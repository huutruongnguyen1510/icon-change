//
//  IconWidgetViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/20/21.
//

import UIKit
import RealmSwift
import Then
import Swifter

class IconWidgetViewController: UIViewController {
    @IBOutlet weak var collectionViewIcon:UICollectionView!
    @IBOutlet weak var collectionViewToolbar:UICollectionView!
    @IBOutlet weak var collectionViewEmpty:UICollectionView!
    @IBOutlet private weak var containerIconView: UIView!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var lbNameApp: UILabel!
    @IBOutlet private weak var pleaseEnterLabel: UILabel!
    @IBOutlet private weak var enterTextIconTextField: UITextField!
    @IBOutlet private weak var iconBorderImageView: UIImageView!
    @IBOutlet private weak var enterPhotoIconImageView: UIImageView!
    @IBOutlet private weak var selectAppButton: UIButton!
    //@IBOutlet private weak var previewButton: UIButton!
    @IBOutlet private weak var createIconButton: UIButton!
    @IBOutlet private weak var pleaseEnterImageView: UIImageView!
    
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: true)
    }
    
    @IBAction func selectAppAction() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RelatedAppViewController") as! RelatedAppViewController
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true)
        vc.handleSelectRelatedApp = { [unowned self] in
            self.appRelate = $0
            self.iconObj.appRelateScheme = $0.scheme
            self.enableButtonAction()
        }
    }
    
    @IBAction func createIconAction() {
        let icon = IconObj().with {
            $0.id = iconObj.id
            $0.color = iconObj.color
            $0.createDate = iconObj.createDate
            $0.appIcon = ImageData(photo: containerIconView.screenshot!)
            $0.appName = nameApp
            $0.appRelateName = iconObj.appRelateName
            $0.appRelateScheme = iconObj.appRelateScheme
            $0.iconBorder = iconBorder
            $0.photoIcon = ImageData(photo: enterPhotoIconImageView.image ?? UIImage())
        }
        handleSaveIconForWidget?(icon)
        navigationController?.popViewController(animated: true)
    }
    
    var handleSaveIconForWidget: ((IconObj) -> ())?
    var iconObj: IconObj = IconObj() //{
//        didSet {
//            if let iconObj = iconObj {
//                nameApp = iconObj.appName
//                appRelate = RelateAppModel(appName: iconObj.appRelateName, scheme: iconObj.appRelateScheme)
//                fontSize = Float(iconObj.fontSize)
//                fontType = iconObj.fontType
//                iconBorder = iconObj.iconBorder
//                textColor = iconObj.textColor
////                self.pleaseEnterLabel.isHidden = true
////                self.pleaseEnterImageView.isHidden = true
////                enableButtonAction()
//            }
//        }
 //   }
    var nameApp = ""
    var server: HttpServer?
    var textColor = ColorType.black
    var color: UIColor = UIColor()
    var fontType = FontType.Poppin
    var fontName = FontType.Poppin.fontName
    var fontSize: Float = 40.0
    var iconBorder: BorderType = .none
    var checkTab = 0
    var isChooseFont = true
    var listSavedColor: [ColorModel] = [ColorModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        nameApp = appRelate?.appName ?? ""
        if nameApp != "" {
            let image = UIImage().getUncachedImage(named: nameApp)
            selectAppButton.setImage(image?.scaledToSize(CGSize(width: 35, height: 35)), for: .normal)
        }
        enableButtonAction()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterTextIconTextField.rx.controlEvent(.editingDidBegin)
            .subscribe(onNext: { [unowned self] in
                self.pleaseEnterImageView.isHidden = true
                self.pleaseEnterLabel.isHidden = true
            })
            .disposed(by: rx.disposeBag)
        
        enterTextIconTextField.rx.controlEvent(.editingDidEnd)
            .map { [unowned self] in self.enterTextIconTextField.text }
            .compactMap { $0 }
            .subscribe(onNext: { [unowned self] in
                self.pleaseEnterImageView.isHidden = !$0.isEmpty
                self.pleaseEnterLabel.isHidden = !$0.isEmpty
                self.enableButtonAction()
            })
            .disposed(by: rx.disposeBag)
        
        collectionViewIcon.register(UINib(nibName: "SizeColorFontCLVCell", bundle: nil), forCellWithReuseIdentifier: "SizeColorFontCLVCell")
        collectionViewIcon.register(UINib(nibName: "FontCLVCell", bundle: nil), forCellWithReuseIdentifier: "FontCLVCell")
        collectionViewIcon.register(UINib(nibName: "SizeIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "SizeIconCLVCell")
        collectionViewIcon.register(UINib(nibName: "IconCreateCLVCell", bundle: nil), forCellWithReuseIdentifier: "IconCreateCLVCell")
        collectionViewIcon.register(UINib(nibName: "BorderStyleCLVCell", bundle: nil), forCellWithReuseIdentifier: "BorderStyleCLVCell")
        collectionViewToolbar.register(UINib(nibName: "ToolbarCLVCell", bundle: nil), forCellWithReuseIdentifier: "ToolbarCLVCell")
        collectionViewToolbar.layer.borderWidth = 3
        collectionViewToolbar.layer.borderColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
        collectionViewToolbar.layer.cornerRadius = 10
//        collectionViewEmpty.roundCorners([.topLeft, .topRight], radius: 40)
//        collectionViewIcon.roundCorners([.topLeft, .topRight], radius: 40)
        NotificationCenter.default.addObserver(self, selector: #selector(loadBorder(notification:)), name: Notification.Name("LOAD_BORDER"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadFontSize(notification:)), name: Notification.Name("LOAD_FONT_SIZE"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadFontColor(notification:)), name: Notification.Name("LOAD_FONT_COLOR"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadFontType(notification:)), name: Notification.Name("LOAD_FONT_TYPE"), object: nil)
        setupSavedIcon()
        enableButtonAction()
        setupIconObj()
        // Do any additional setup after loading the view.
    }
    
    func setupSavedIcon() {
        iconBorder = iconObj.iconBorder
        lbNameApp.text = iconObj.appName
        enterTextIconTextField.text = iconObj.textIcon
        enterPhotoIconImageView.image = UIImage(data: iconObj.photoIcon.photo)
        iconBorderImageView.image = iconObj.iconBorder.image
        nameApp = iconObj.appName
        textColor = iconObj.textColor
        fontType = iconObj.fontType
        fontName = iconObj.fontType.fontName
        fontSize = Float(iconObj.fontSize)
    }
//    private var iconObj: IconObj? {
//        didSet {
//            if let iconObj = iconObj {
////                createIconType = iconObj.createIconType
////                iconNameTextField.text = iconObj.appName
////                appRelate = RelateAppModel(appName: iconObj.appRelateName, scheme: iconObj.appRelateScheme)
////                fontSize = iconObj.fontSize
////                fontType = iconObj.fontType
////                iconBorder = iconObj.iconBorder
////                textColor = iconObj.textColor
//                enterTextIconTextField.text = iconObj.textIcon
//                enterPhotoIconImageView.image = iconObj.photoIcon
//                self.pleaseEnterLabel.isHidden = true
//                enableButtonAction()
//            }
//        }
//    }
    
    private var appRelate: RelateAppModel? //{
//        didSet {
//            if let appRelateName = appRelate?.appName {
//                selectAppButton.setImage(UIImage().getUncachedImage(named: appRelateName), for: .normal)
//            }
//            //containerAppRelateView.isHidden = appRelate.isNil
//        }
//    }
    
    private func enableButtonAction() {
        let haveTextIcon = !(enterTextIconTextField.text?.isEmpty ?? false)
        let havePhotoIcon = !enterPhotoIconImageView.image.isNil
        let haveAppRelated = appRelate?.appName != ""
        let isEnable = (haveTextIcon || havePhotoIcon) && haveAppRelated
        [createIconButton].forEach {
            $0?.alpha = isEnable ? 1 : 0.5
            $0?.isUserInteractionEnabled = isEnable
        }
    }
    
    @objc func loadFontType(notification not: Notification) {
        let item = not.object
        fontName = item as! String
        enterTextIconTextField.font = UIFont(name: fontName, size: CGFloat(fontSize))
        //enterTextIconTextField.font
    }
    
    @objc func loadFontColor(notification not: Notification) {
        let item = not.object
        color = item as? UIColor ?? UIColor.black
        enterTextIconTextField.textColor = color
    }
    
    @objc func loadFontSize(notification not: Notification) {
        let item = not.object
        fontSize = item as! Float
        enterTextIconTextField.font = UIFont(name: fontName, size: CGFloat(fontSize))
    }
    
    @objc func loadBorder(notification not: Notification) {
        let item = not.object
        let allBorderType = BorderType.allCases
        iconBorder = allBorderType[item as! Int]
        let image = iconBorder.image
        iconBorderImageView.image = image!.scaledToSize(CGSize(width: iconBorderImageView.frame.width, height: iconBorderImageView.frame.height))
    }
}

extension IconWidgetViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewToolbar {
            checkTab = indexPath.item
            collectionView.reloadData()
            if indexPath.item == 2 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "BackgroundViewController") as! BackgroundViewController
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
                vc.didSendDataBackground = { [weak self] background in
                        guard let self = self else { return }
                    self.enterPhotoIconImageView.image = background.scaledToSize(CGSize(width: self.enterPhotoIconImageView.frame.width, height: self.enterPhotoIconImageView.frame.height))
                }
                //self.present(vc, animated: true)
            }
            else {
                collectionViewIcon.reloadData()
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionViewIcon {
            return 6
        }
        return 1
    }
}

extension IconWidgetViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewToolbar {
            return 3
        }
        if collectionView == collectionViewIcon {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewToolbar {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToolbarCLVCell", for: indexPath) as! ToolbarCLVCell
            cell.backgroundColor = .clear
            cell.layer.cornerRadius = 10
            cell.contentView.backgroundColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
            cell.lbName.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            if checkTab == indexPath.item {
                cell.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.lbName.textColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
            }
            if indexPath.item == 0 {
                cell.lbName.text = "Border"
            }
            else if indexPath.item == 1 {
                cell.lbName.text = "Font"
            }
            else {
                cell.lbName.text = "Background"
            }
            return cell
        }
        else {
            if indexPath.section % 2 == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCreateCLVCell", for: indexPath) as! IconCreateCLVCell
                if indexPath.section == 0 {
                    cell.lbName.text = "Size"
                }
                else if indexPath.section == 2 {
                    cell.lbName.text = "Color"
                }
                else {
                    if checkTab == 1 {
                        cell.lbName.text = "Font style"
                    }
                    else {
                        cell.lbName.text = "Border"
                    }
                }
                return cell
            }
            else if indexPath.section == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SizeIconCLVCell", for: indexPath) as! SizeIconCLVCell

                return cell
            }
            else if indexPath.section == 5 {
                if checkTab == 0 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BorderStyleCLVCell", for: indexPath) as! BorderStyleCLVCell

                    return cell
                }
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FontCLVCell", for: indexPath) as! FontCLVCell

                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SizeColorFontCLVCell", for: indexPath) as! SizeColorFontCLVCell
            cell.layer.cornerRadius = 10
            cell.backgroundColor = .clear
    //        cell.imgView.image = #imageLiteral(resourceName: "quiz-bg").imageResize(sizeChange: CGSize(width: cell.frame.width, height: cell.frame.height))

            return cell
        }
    }
}

extension IconWidgetViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewToolbar {
            if indexPath.item == 2 {
                return CGSize(width: 110, height: collectionViewToolbar.frame.height)
            }
            else {
                return CGSize(width: 95, height: collectionViewToolbar.frame.height - 6)
            }
        }
        else {
            if indexPath.section % 2 == 0 {
                return CGSize(width: collectionViewIcon.frame.width - 50, height: 30)
            }
            else if indexPath.section == 1 {
                return CGSize(width: collectionViewIcon.frame.width - 50, height: 50)
            }
            else if indexPath.section == 3 {
                return CGSize(width: collectionViewIcon.frame.width - 50, height: 60)
            }
            else {
                if checkTab == 1 {
                    return CGSize(width: collectionViewIcon.frame.width - 50, height: 40)
                }
                return CGSize(width: collectionViewIcon.frame.width - 50, height: 60)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionViewIcon {
            if section == 0 {
                return UIEdgeInsets(top: 90, left: 0, bottom: 0, right: 0)
            }
            else if section == 2 || section == 4 {
                return UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
            }
            return .zero
        }
        else {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension IconWidgetViewController {
    private func setupIconObj() {
        appRelate = RelateAppModel(appName: iconObj.appRelateName, scheme: iconObj.appRelateScheme)
        fontSize = Float(iconObj.fontSize)
        fontType = iconObj.fontType
        iconBorder = iconObj.iconBorder
        textColor = iconObj.textColor
        enterTextIconTextField.text = iconObj.textIcon
        enterPhotoIconImageView.image = UIImage(data: iconObj.photoIcon.photo)
        if iconObj.textIcon != "" {
            self.pleaseEnterLabel.isHidden = true
            self.pleaseEnterImageView.isHidden = true
        }
        enableButtonAction()
    }
    
    func showShortcutScreen(forDeepLink deepLink: String, iconApp: UIImage, nameApp: String) {
        server = HttpServer()
        
        let portId = Int.random(in: 1000..<9999)
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let str = String((0..<1).map{ _ in letters.randomElement()! })
        
        guard let deepLinkUrl = URL(string: deepLink) else {
            return
        }
        guard let shortcutUrl = URL(string: "http://localhost:\(portId)/\(str)") else {
            return
        }
        guard let iconData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }
        guard let pSplashData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }
        guard let lSplashData = iconApp.jpegData(compressionQuality: 0) else {
            return
        }

        let iconBase64 = iconData.base64EncodedString()
        let pSplashBase64 = pSplashData.base64EncodedString()
        let lSplashBase64 = lSplashData.base64EncodedString()
        let html = Common.htmlFor(title: nameApp,
                           urlToRedirect: deepLinkUrl,
                           iconBase64: iconBase64,
                           pSplashBase64: pSplashBase64,
                           lSplashBase64: lSplashBase64)
        guard let base64 = html.data(using: .utf8)?.base64EncodedString() else {
            return
        }
        server?["/\(str)"] = { request in
            return .movedPermanently("data:text/html;base64,\(base64)")
        }
        try? server?.start(in_port_t(portId))
        print(shortcutUrl)
        UIApplication.shared.open(shortcutUrl)
    }
}

