//
//  CreateWidgetViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/12/21.
//

import UIKit

class CreateWidgetViewController: UIViewController {
    @IBOutlet weak var collectionViewWidget: UICollectionView!
    @IBOutlet weak var collectionViewToolbar: UICollectionView!
    @IBOutlet weak var editButton: UIButton!
    
    @IBAction func editAction() {
        if statusViewMyIcon == .done {
            statusViewMyIcon = .edit
            editButton.setImage(#imageLiteral(resourceName: "edit (1)"), for: .normal)
        }
        else {
            statusViewMyIcon = .done
            editButton.setImage(#imageLiteral(resourceName: "enable-edit"), for: .normal)
        }
        collectionViewWidget.reloadData()
    }
    
    private var statusViewMyIcon: StatusViewMyIcon = .edit
    
    @IBAction func backAction() {
        self.navigationController?.popViewController(animated: true)
//        dismiss(animated: true)
    }
    
    // MARK: - Variables
    private var widgets: [WidgetObj] = []
    private var familyWidgetType: WidgetType = .Medium {
        didSet {
//            familyWidgetSelectedLabel.text = familyWidgetType.rawValue
            widgets.removeAll()
            widgets = widgetManager.getAllWidget().filter { $0.widgetType == familyWidgetType }
            reloadCollectionView()
//            if collectionViewWidget.numberOfItems(inSection: 0) > 0 {
//                collectionViewWidget.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
//            }
        }
    }
    
//    private var indexShowMoreOption: Int? {
//        didSet {
//            reloadCollectionView()
//        }
//    }
//    
    var checkTab = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        widgets.removeAll()
        widgets = widgetManager.getAllWidget().filter { $0.widgetType == familyWidgetType }
        reloadCollectionView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewToolbar.register(UINib(nibName: "ToolbarCLVCell", bundle: nil), forCellWithReuseIdentifier: "ToolbarCLVCell")
        collectionViewWidget.register(UINib(nibName: "WidgetCLVCell", bundle: nil), forCellWithReuseIdentifier: "WidgetCLVCell")
        collectionViewWidget.backgroundColor = .clear
        collectionViewToolbar.layer.borderWidth = 3
        collectionViewToolbar.layer.borderColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
        collectionViewToolbar.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    private func selectWidget(index: Int?) {
//        indexShowMoreOption = nil
//        hidePopupWidgetFamiliesIfNeeded()
        if let index = index {
            for i in self.widgets.indices {
                self.widgets[i].isSelected = i == index
                self.widgets[i].updateWidget()
            }
            reloadCollectionView()
//            collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredVertically, animated: true)
            AppSettings.writeContents(widget: widgets[index])
        } else {
            reloadCollectionView()
            AppSettings.writeContents(widget: WidgetObj().with { $0.isSelected = true })
        }
    }
    
    private func deleteWidget(index: Int) {
        if widgets[index].isSelected {
            widgets[index].deleteWidget()
            widgets.remove(at: index)
            
            if widgets.count > 0 {
                var newIndex: Int = index
                while newIndex >= widgets.count {
                    newIndex -= 1
                }
                selectWidget(index: newIndex)
            } else {
                selectWidget(index: nil)
            }
            
        } else {
            widgets[index].deleteWidget()
            widgets.remove(at: index)
            collectionViewWidget.performBatchUpdates({
                collectionViewWidget.deleteItems(at: [IndexPath(item: index, section: 0)])
            }, completion: { _ in
                self.reloadCollectionView()
            })
        }
    }
    
    
    private func reloadCollectionView() {
        collectionViewWidget.reloadData()
        if collectionViewWidget.numberOfItems() > 0 {
            collectionViewWidget.backgroundView = nil
        } else {
            collectionViewWidget.backgroundView = Common.viewNoData(title: "There no data!\n Select 'Add Folder' to create your first Widget.")
        }
    }
    
}

extension CreateWidgetViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewToolbar {
            checkTab = indexPath.item
            if indexPath.item == 0 {
                familyWidgetType = .Medium
            }
            else {
                familyWidgetType = .Large
            }
            collectionViewToolbar.reloadData()
        }
        else if collectionView == collectionViewWidget {
            if statusViewMyIcon == .edit {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CustomizeViewController") as! CustomizeViewController
                vc.familyWidgetType = familyWidgetType
                if indexPath.item == 0{
                    vc.isUpdate = false
                }
                else {
                    vc.widgetObj = widgets[indexPath.item - 1]
                    let image = UIImage(data: widgets[indexPath.item - 1].backgroundImage?.photo ?? Data())
                    vc.backgroundWidget = image ?? UIImage()
                    vc.handleIconIsAdded = { [unowned self] in
                        selectWidget(index: indexPath.item - 1)
//                        for item in 0...widgets.count - 1 {
//                            widgets[item].isSelected = false
//                            if item == indexPath.item - 1 {
//                                widgets[item].isSelected = true
//                            }
//                        }
                    }
                }
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                if indexPath.item != 0 {
                    self.deleteWidget(index: indexPath.item - 1)
                }
            }
        }
    }
}

extension CreateWidgetViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewToolbar {
            return 2
        }
        else {
            return widgets.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewToolbar {
            let cell = collectionViewToolbar.dequeueReusableCell(withReuseIdentifier: "ToolbarCLVCell", for: indexPath) as! ToolbarCLVCell
            cell.backgroundColor = .clear
    //        cell.imgView.image = #imageLiteral(resourceName: "quiz-bg").imageResize(sizeChange: CGSize(width: cell.frame.width, height: cell.frame.height))
            cell.layer.cornerRadius = 10
            cell.contentView.backgroundColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
            cell.lbName.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            if checkTab == indexPath.item {
                cell.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                cell.lbName.textColor = #colorLiteral(red: 0.4581112266, green: 0.2310201526, blue: 1, alpha: 1)
            }
            if indexPath.item == 0 {
                cell.lbName.text = "Medium"
            }
            else {
                cell.lbName.text = "Large"
            }
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WidgetCLVCell", for: indexPath) as! WidgetCLVCell
            cell.layer.cornerRadius = 15
            cell.contentView.backgroundColor = .white
            if indexPath.item == 0 {
                cell.imgView.image = #imageLiteral(resourceName: "createIcon_pleaseEnterImage")
                cell.labelName.text = ""
                cell.blackView.isHidden = true
                cell.trashImgView.isHidden = true
                cell.chooseButton.isHidden = true
                cell.chooseImgView.isHidden = true
                return cell
            }
            else {
                let imageData = widgets[indexPath.item - 1].backgroundImage?.photo
                if imageData == nil {
                    
                }
                else {
                    let image = UIImage(data: imageData!)
                    if image == nil {

                    }
                    else {
                        cell.imgView.image = image!.scaledToSize(CGSize(width: cell.frame.width, height: cell.frame.height))
                    }
                }
                cell.chooseButton.isHidden = false
                cell.chooseImgView.isHidden = true
                cell.labelName.text = widgets[indexPath.item - 1].widgetName
                if statusViewMyIcon == .edit {
                    cell.blackView.isHidden = true
                    cell.trashImgView.isHidden = true
                    cell.chooseButton.rx.tap
                        .subscribe(onNext: { [unowned self] in
                            self.selectWidget(index: indexPath.item - 1)
//                            for item in 0...widgets.count - 1 {
//                                widgets[item].isSelected = false
//                                if item == indexPath.item - 1 {
//                                    widgets[item].isSelected = true
//                                }
//                            }
                        })
                        .disposed(by: rx.disposeBag)

                    if widgets[indexPath.item - 1].isSelected {
                        cell.chooseImgView.isHidden = false
                    }
                    return cell
                }
                cell.blackView.isHidden = false
                cell.trashImgView.isHidden = false
                //cell.backgroundColor = .clear
//                cell.handleUseIt = { [unowned self] in
//                    self.selectWidget(index: indexPath.row - 1)
//                }
                return cell
            }
        }
    }
}

extension CreateWidgetViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewToolbar {
            return CGSize(width: 100, height: 47)
        }
        else {
            if familyWidgetType == .Large {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    return CGSize(width: UIScreen.main.bounds.width/2 , height: UIScreen.main.bounds.width/2)
                }
                return CGSize(width: UIScreen.main.bounds.width/2 , height: UIScreen.main.bounds.width/2)
            }
            else {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    return CGSize(width: UIScreen.main.bounds.width/2 , height: UIScreen.main.bounds.width/3.5)
                }
                return CGSize(width: UIScreen.main.bounds.width/2 , height: UIScreen.main.bounds.width/3.5)
            }

        }
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            return CGSize(width: UIScreen.main.bounds.width/2 - 60 , height: UIScreen.main.bounds.width/2 - 80)
//        }
//        return CGSize(width: UIScreen.main.bounds.width - 100 , height: UIScreen.main.bounds.width - 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewToolbar {
            return 0
        }
        else {
            return 15
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}
