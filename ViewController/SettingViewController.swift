//
//  SettingViewController.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/18/21.
//

import UIKit

class SettingViewController: UIViewController {
    @IBOutlet weak var collectionViewMain: UICollectionView!
    var listSetting: [SettingModel] = [SettingModel]()
    var checkTab = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        listSetting = SettingData.shared.menu
        collectionViewMain.backgroundColor = .clear
        collectionViewMain.register(UINib(nibName: "ContentSettingCLVCell", bundle: nil), forCellWithReuseIdentifier: "ContentSettingCLVCell")
        collectionViewMain.register(UINib(nibName: "SettingCLVCell", bundle: nil), forCellWithReuseIdentifier: "SettingCLVCell")
        collectionViewMain.register(UINib(nibName: "IconCreateCLVCell", bundle: nil), forCellWithReuseIdentifier: "IconCreateCLVCell")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(){
        if checkTab == 0 {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            checkTab = 0
            collectionViewMain.reloadData()
        }
    }
    
    func addBottomBorder(view: UIView, with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: view.frame.size.height - borderWidth, width: view.frame.size.width, height: borderWidth)
        view.addSubview(border)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if checkTab == 0 {
            if indexPath.section == 1 {
                checkTab = indexPath.item + 1
                collectionView.reloadData()
            }
            else if indexPath.section == 3 {
                checkTab = indexPath.item + 5
                collectionView.reloadData()
            }
        }
        else if checkTab == 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if indexPath.item == 0 {
                let vc = storyboard.instantiateViewController(withIdentifier: "IconTutorialViewController") as! IconTutorialViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }
            else {
                let vc = storyboard.instantiateViewController(withIdentifier: "WidgetTutorialViewController") as! WidgetTutorialViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true)
            }
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        if checkTab == 0 {
            return 4
        }
        else if checkTab == 1 {
            return 2
        }
        return 2
    }
}
extension SettingViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if checkTab == 0 {
            if section % 2 == 0 {
                return 1
            }
            else if section == 3 {
                return 3
            }
            return 4
        }
        else if checkTab == 1 {
            if section == 0 {
                return 1
            }
            return 2
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if checkTab == 0 {
            if indexPath.section % 2 == 0 {
                let cell = collectionViewMain.dequeueReusableCell(withReuseIdentifier: "IconCreateCLVCell", for: indexPath) as! IconCreateCLVCell
                cell.contentView.backgroundColor = #colorLiteral(red: 0.9729534984, green: 0.976138413, blue: 0.9969729781, alpha: 1)
                if indexPath.section == 0 {
                    cell.lbName.text = "SUPPORT US"
                }
                else {
                    cell.lbName.text = "OTHERS"
                }
                return cell
            }
            let cell = collectionViewMain.dequeueReusableCell(withReuseIdentifier: "SettingCLVCell", for: indexPath) as! SettingCLVCell
            addBottomBorder(view: cell, with: .gray, andWidth: 0.5)
            if indexPath.section == 1 {
                cell.lbName.text = listSetting[indexPath.item].text
            }
            else {
                cell.lbName.text = listSetting[indexPath.item + 4].text
            }
            return cell
        }
        else if checkTab == 1 {
            if indexPath.section == 0 {
                let cell = collectionViewMain.dequeueReusableCell(withReuseIdentifier: "IconCreateCLVCell", for: indexPath) as! IconCreateCLVCell
                cell.lbName.text = listSetting[0].text.uppercased()
                return cell
            }
            let cell = collectionViewMain.dequeueReusableCell(withReuseIdentifier: "SettingCLVCell", for: indexPath) as! SettingCLVCell
            if indexPath.item == 0 {
                cell.lbName.text = "Icon Tutorial"
            }
            else {
                cell.lbName.text = "Widget Tutorial"
            }
            return cell
        }
        else {
            if indexPath.section == 0 {
                let cell = collectionViewMain.dequeueReusableCell(withReuseIdentifier: "IconCreateCLVCell", for: indexPath) as! IconCreateCLVCell
                cell.lbName.text = listSetting[checkTab - 1].text.uppercased()
                return cell
            }
            let cell = collectionViewMain.dequeueReusableCell(withReuseIdentifier: "ContentSettingCLVCell", for: indexPath) as! ContentSettingCLVCell
            cell.content.text = listSetting[checkTab - 1].content
            return cell
        }
    }
}

extension SettingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if checkTab > 1 {
            if indexPath.section == 1 {
                return CGSize(width: collectionView.frame.width, height: UIScreen.main.bounds.height)
            }
            return CGSize(width: collectionView.frame.width, height: 50)
        }
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if checkTab <= 1 {
            if section % 2 == 1 {
                return 0.5
            }
            return .zero
        }
        return .zero
    }
    
}




