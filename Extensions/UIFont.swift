//
//  UIFont.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/30/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

extension UIFont {
    static func regular(size: CGFloat) -> UIFont {
        // swiftlint:disable:next force_unwrapping
        let font = UIFont(name: "Nunito-Regular", size: size)!
        return Common.getFontForDeviceWithFontDefault(fontDefault: font)
    }
    
    static func extraBold(size: CGFloat) -> UIFont {
        // swiftlint:disable:next force_unwrapping
        let font = UIFont(name: "Nunito-ExtraBold", size: size)!
        return Common.getFontForDeviceWithFontDefault(fontDefault: font)
    }
    
    static func semiBold(size: CGFloat) -> UIFont {
        // swiftlint:disable:next force_unwrapping
        let font = UIFont(name: "Nunito-SemiBold", size: size)!
        return Common.getFontForDeviceWithFontDefault(fontDefault: font)
    }
    
    static func bold(size: CGFloat) -> UIFont {
        // swiftlint:disable:next force_unwrapping
        let font = UIFont(name: "Nunito-Bold", size: size)!
        return Common.getFontForDeviceWithFontDefault(fontDefault: font)
    }
}
