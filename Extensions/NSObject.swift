//
//  NSObject.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import Foundation

extension NSObject {
    static var className: String {
        return String(describing: self)
    }
    
    static var classNameHaveIpad: String {
        return className + (isIPad ? "_iPad" : "")
    }

    var className: String {
        return type(of: self).className
    }
}
