//
//  ComposeType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum FontType: String, CaseIterable, Codable {
    case ArchitectsDaughter = "ArchitectsDaughter-Regular"
    case AvertaStd = "AvertaStdCY-Regular"
    case BebasNeue = "BebasNeue-Regular"
    case HLTAparo = "HLT Aparo"
    case HLTFitigraf = "HLT Fitigraf_s"
    case iCielKoni = "iCiel Koni Black"
    case Jua = "Jua-Regular"
    case Kerokero = "KERO KERO"
    case Nunito = "Nunito-SemiBold"
    case Pacifico = "Pacifico-Regular"
    case Roboto = "Roboto-Regular"
    case Peddana = "segoeprb"
    case SVN_AdamGorry = "SVN-Adam Gorry"
    case UVN_Anh_Hai = "UVNAnhHai_B"
    case UTM_Flamenco = "UTM Flamenco"
    case Poppin = "Poppins-SemiBoldItalic"
    
    var fontName: String {
        switch self {
        case .Nunito:
            return "Nunito-SemiBold"
        case .AvertaStd:
            return "AvertaStdCY-Regular"
        case .iCielKoni:
            return "iCiel Koni Black"
        case .ArchitectsDaughter:
            return "ArchitectsDaughter-Regular"
        case .BebasNeue:
            return "BebasNeue-Regular"
        case .HLTAparo:
            return "HLTAparo"
        case .HLTFitigraf:
            return "HLTFitigraf-S"
        case .Jua:
            return "Jua-Regular"
        case .Kerokero:
            return "Kerokero"
        case .Pacifico:
            return "Pacifico-Regular"
        case .Peddana:
            return "segoeprb"
        case .Roboto:
            return "Roboto-Regular"
        case .Poppin:
            return "Poppins-SemiBoldItalic"
        case .SVN_AdamGorry:
            return "SVN-Adam Gorry"
        case .UTM_Flamenco:
            return "UTM Flamenco"
        case .UVN_Anh_Hai:
            return "UVNAnhHai_B"
        }
    }
}
