//
//  ComposeType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum BorderType: String, CaseIterable, Codable {
    case none = "none"
    case border1 = "border1"
    case border2 = "border2"
    case border3 = "border3"
    case border4 = "border4"
    case border5 = "border5"
    case border6 = "border6"
    case border7 = "border7"
    case border8 = "border8"
    
    var image: UIImage? {
        switch self {
        case .none:
            return nil
        case .border1:
            return #imageLiteral(resourceName: "3ad2e964b84c0e899122b48bba99f2a6 1")
        case .border2:
            return #imageLiteral(resourceName: "81116a8276d03d38e0540f2b4d24ddca 1")
        case .border3:
            return #imageLiteral(resourceName: "17973833 1")
        case .border4:
            return #imageLiteral(resourceName: "17973837 1")
        case .border5:
            return #imageLiteral(resourceName: "17973838 copy 1")
        case .border6:
            return #imageLiteral(resourceName: "colored-drops-border-design-vector_7Jc0XM_SB_PM 1")
        case .border7:
            return #imageLiteral(resourceName: "download copy 1")
        case .border8:
            return #imageLiteral(resourceName: "e6f7c0bb21e2e7d4f5733830541bd391 1")
        }
    }
}
