//
//  HowToUseType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 8/7/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum HowToUseType: String, CaseIterable {
    case first
    case second
    case third
    case fourth
    case fifth
    case sixth
    
    var title: String? {
        switch self {
        case .first:
            return "Click + to create icon"
        case .second:
            return nil
        case .third:
            return "Click “ Create Icon ”"
        case .fourth:
            return nil
        case .fifth:
            return "Click Add to Home Screen"
        case .sixth:
            return "Enjoy Your New Icon Pack"
        }
    }
    
    var titleAttribute: NSAttributedString? {
        switch self {
        case .second:
            let myString = NSMutableAttributedString(string: "Click ")
            
            let imageOffsetY: CGFloat = -5.0
            let heightImage: CGFloat = isIPad ? 31 : 22
            let attachment = NSTextAttachment()
            attachment.bounds = CGRect(x: 0, y: imageOffsetY, width: heightImage, height: heightImage)
            attachment.image = #imageLiteral(resourceName: "createIcon_pleaseEnterText")
            
            myString.append(NSAttributedString(attachment: attachment))
            myString.append(NSAttributedString(string: " to add Text icon\nor "))
            
            let attachment2 = NSTextAttachment()
            attachment2.bounds = CGRect(x: 0, y: imageOffsetY, width: heightImage, height: heightImage)
            attachment2.image = #imageLiteral(resourceName: "createIcon_pleaseEnterImage")
            myString.append(NSAttributedString(attachment: attachment2))
            myString.append(NSAttributedString(string: " to add Image icon"))
            
            return myString
        case .fourth:
            let myString = NSMutableAttributedString(string: "Click ")
            
            let imageOffsetY: CGFloat = -5.0
            let heightImage: CGFloat = isIPad ? 45 : 30
            let attachment = NSTextAttachment()
            attachment.bounds = CGRect(x: 0, y: imageOffsetY, width: heightImage, height: heightImage)
            attachment.image = #imageLiteral(resourceName: "share")
            myString.append(NSAttributedString(attachment: attachment))
            myString.append(NSAttributedString(string: " button"))
            
            return myString
        default:
            return nil
        }
    }
    
    var image: UIImage {
        switch self {
        case .first:
            return #imageLiteral(resourceName: "howToUse1")
        case .second:
            return #imageLiteral(resourceName: "howToUse2")
        case .third:
            return #imageLiteral(resourceName: "howToUse3")
        case .fourth:
            return #imageLiteral(resourceName: "howToUse4")
        case .fifth:
            return #imageLiteral(resourceName: "howToUse5")
        case .sixth:
            return #imageLiteral(resourceName: "howToUse6")
        }
    }
}
