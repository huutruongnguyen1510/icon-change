//
//  ComposeType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum ColorType: String, CaseIterable, Codable {
    case none = "none"
    case black = "black"
    case color2 = "color2"
    case color3 = "color3"
    case color4 = "color4"
    case color5 = "color5"
    case color6 = "color6"
    case color7 = "color7"
    case color8 = "color8"
    case color9 = "color9"
    case color10 = "color10"
    case color11 = "color11"
    case color12 = "color12"
    case color13 = "color13"
    case color14 = "color14"
    case color15 = "color15"
    case color16 = "color16"
    case color17 = "color17"
    case color18 = "color18"
    case color19 = "color19"
    case color20 = "color20"
    case color21 = "color21"
    case color22 = "color22"
    case color23 = "color23"
    case color24 = "color24"
    case color25 = "color25"
    case color26 = "color26"
    case color27 = "color27"
    case color28 = "color28"
    case color29 = "color29"
    case color30 = "color30"
    
    var color: UIColor {
        switch self {
        case .none:
            return UIColor.none
        case .black:
            return .black
        case .color2:
            return .hex("009345")
        case .color3:
            return .hex("26D508")
        case .color4:
            return .hex("FDBF0F")
        case .color5:
            return .hex("FF910F")
        case .color6:
            return .hex("FF5B94")
        case .color7:
            return .hex("EE4646")
        case .color8:
            return .hex("B26A7F")
        case .color9:
            return .hex("FF9BBE")
        case .color10:
            return .hex("FFC683")
        case .color11:
            return .hex("EC9734")
        case .color12:
            return .hex("AD6818")
        case .color13:
            return .hex("7C4A0F")
        case .color14:
            return .hex("633B0A")
        case .color15:
            return .hex("007436")
        case .color16:
            return .hex("013419")
        case .color17:
            return .hex("01130A")
        case .color18:
            return .hex("232323")
        case .color19:
            return .hex("454545")
        case .color20:
            return .hex("787878")
        case .color21:
            return .hex("8F8F8F")
        case .color22:
            return .hex("B4B4B4")
        case .color23:
            return .hex("CBCBCB")
        case .color24:
            return .hex("D9D9D9")
        case .color25:
            return .hex("640074")
        case .color26:
            return .hex("8400B2")
        case .color27:
            return .hex("A100D9")
        case .color28:
            return .hex("B000ED")
        case .color29:
            return .hex("D14BFF")
        case .color30:
            return .hex("FC3AE8")
        }
    }
}
