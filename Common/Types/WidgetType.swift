//
//  WidgetType.swift
//  IconHomeScreen
//
//  Created by nguyen.viet.luy on 10/30/20.
//

import Foundation

enum WidgetType: String, Codable {
    case Medium
    case Large
}
