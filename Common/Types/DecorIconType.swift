//
//  ComposeType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum DecorIconType: String, CaseIterable {
    case imageShape = "Image Shape"
    case font = "Font"
    case fontColor = "Font Color"
    case fontSize = "Font Size"
    case iconBorder = "Icon Border"
}
