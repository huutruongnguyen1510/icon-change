//
//  SettingType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum SettingType: CaseIterable {
    case howToUse
    case privatePolicy
    case share
    case support
    case about
    case rateApp
    
    var title: String {
        switch self {
        case .howToUse:
            return "How to use ?"
        case .privatePolicy:
            return "Private Policy"
        case .share:
            return "Share this App"
        case .support:
            return "Support"
        case .about:
            return "About"
        case .rateApp:
            return "Rate App"
        }
    }
    
    var content: String {
        switch self {
        case .about:
            return "We is a mobile development team that creates apps for App Store with many active users per month worldwide. We always aims at redefining the way people interact with their mobile devices. The unique blend of passion and skills enables to deliver unrivaled products to the end users. We stay focused and create products that drive value to our users and make their life easier. If you have any questions, do not hesitate to contact us at ducnguyen.gsc@gmail.com"
        case .privatePolicy:
            return "By using Nguyen Duc Team's APP (the “APP”), you are agreeing to our privacy policy, which applies to the use of the Site and Nguyen Duc Team applications. Nguyen Duc reserves the right at any time, without further notice, to change and modify this privacy policy. You should check this privacy policy periodically.\n\nWe here at Eyewind respect your privacy. Here is a little explanation of when and why we might collect your data, and what we do with it . If you have any questions about this Privacy Policy, please contact us！\nI. Collection and Use of Personal Information\n\nFor a better experience, while using our Service, I may require you to provide us with certain personally identifiable information, including but not limited to Internet access. The information that I request will be retained on your device and is not collected by me in any way.\n\nLog Data\n\nI want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.\n\nII. Information Sharing\n\nUnless the Law forces us, we will not share any personal information with any unaffiliated third party for any purpose…ever.\nIII. Security\n\nWhile we take all necessary measures to protect your personal information, no method of transmission over the Internet or method of electronic storage is fully secure, therefore we cannot guarantee complete security.\nIn order to provide products or services you have requested;We require that these parties agree to process such information based on our instructions and in compliance with this policy;There are other instances in which we may divulge your personal information, such as when required by law, regulation, or litigation.\nIf SmartApp becomes involved in a merger, acquisition, or any form of sale of some or all of its assets, we will provide notice before personal information is transferred and becomes subject to a different privacy policy.\n\nContact Us\n\nIf you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me:ducnguyen.gsc@gmail.com"
        default:
            return ""
        }
    }
}
