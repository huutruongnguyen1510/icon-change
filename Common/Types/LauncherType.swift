//
//  LauncherType.swift
//  LauncherWidgets
//
//  Created by nguyen.viet.luy on 17/11/2020.
//

import Foundation

enum LauncherType: String, Codable {
    case Contact
    case App
}
