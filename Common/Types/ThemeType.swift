//
//  CreateIconType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum ThemeType: String, CaseIterable {
    case theme1 = "Theme 1"
    case theme2 = "Theme 2"
    case theme3 = "Theme 3"
    case theme4 = "Theme 4"
    case theme5 = "Theme 5"
    case theme6 = "Theme 6"
    case theme7 = "Theme 7"
    case theme8 = "Theme 8"
    case theme9 = "Theme 9"

    var imageTheme: UIImage? {
        switch self {
        case .theme1:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 1") : #imageLiteral(resourceName: "Themes 1")
        case .theme2:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 2") : #imageLiteral(resourceName: "Themes 2")
        case .theme3:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 3") : #imageLiteral(resourceName: "Themes 3")
        case .theme4:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 4") : #imageLiteral(resourceName: "Themes 4")
        case .theme5:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 5") : #imageLiteral(resourceName: "Themes 5")
        case .theme6:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 6") : #imageLiteral(resourceName: "Themes 6")
        case .theme7:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 7") : #imageLiteral(resourceName: "Themes 7")
        case .theme8:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 8") : #imageLiteral(resourceName: "Themes 8")
        case .theme9:
            return isIPad ? #imageLiteral(resourceName: "Themes iPad 9") : #imageLiteral(resourceName: "Themes 9")
        }

    }

    var wallpaper: UIImage? {
        switch self {
        case .theme1:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 1") : #imageLiteral(resourceName: "wallpaper 1")
        case .theme2:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 2") : #imageLiteral(resourceName: "wallpaper 2")
        case .theme3:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 3") : #imageLiteral(resourceName: "wallpaper 3")
        case .theme4:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 4") : #imageLiteral(resourceName: "wallpaper 4")
        case .theme5:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 5") : #imageLiteral(resourceName: "wallpaper 5")
        case .theme6:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 6") : #imageLiteral(resourceName: "wallpaper 6")
        case .theme7:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 7") : #imageLiteral(resourceName: "wallpaper 7")
        case .theme8:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 8") : #imageLiteral(resourceName: "wallpaper 8")
        case .theme9:
            return isIPad ? #imageLiteral(resourceName: "wallpaper iPad 9") : #imageLiteral(resourceName: "wallpaper 9")
        }
    }

    var iconMaterials: [UIImage] {
        switch self {
        case .theme1:
            return [#imageLiteral(resourceName: "theme1_icon1"),
                    #imageLiteral(resourceName: "theme1_icon2"),
                    #imageLiteral(resourceName: "theme1_icon3"),
                    #imageLiteral(resourceName: "theme1_icon4"),
                    #imageLiteral(resourceName: "theme1_icon5"),
                    #imageLiteral(resourceName: "theme1_icon6"),
                    #imageLiteral(resourceName: "theme1_icon7"),
                    #imageLiteral(resourceName: "theme1_icon8"),
                    #imageLiteral(resourceName: "theme1_icon9"),
                    #imageLiteral(resourceName: "theme1_icon10"),
                    #imageLiteral(resourceName: "theme1_icon11"),
                    #imageLiteral(resourceName: "theme1_icon12"),
                    #imageLiteral(resourceName: "theme1_icon13"),
                    #imageLiteral(resourceName: "theme1_icon14"),
                    #imageLiteral(resourceName: "theme1_icon15"),
                    #imageLiteral(resourceName: "theme1_icon16"),
                    #imageLiteral(resourceName: "theme1_icon17"),
                    #imageLiteral(resourceName: "theme1_icon18"),
                    #imageLiteral(resourceName: "theme1_icon19"),
                    #imageLiteral(resourceName: "theme1_icon20"),
                    #imageLiteral(resourceName: "theme1_icon21"),
                    #imageLiteral(resourceName: "theme1_icon22"),
                    #imageLiteral(resourceName: "theme1_icon23"),
                    #imageLiteral(resourceName: "theme1_icon24"),
                    #imageLiteral(resourceName: "theme1_icon25"),
                    #imageLiteral(resourceName: "theme1_icon26"),
                    #imageLiteral(resourceName: "theme1_icon27"),
                    #imageLiteral(resourceName: "theme1_icon28"),
                    #imageLiteral(resourceName: "theme1_icon29"),
                    #imageLiteral(resourceName: "theme1_icon30"),
                    #imageLiteral(resourceName: "theme1_icon31"),
                    #imageLiteral(resourceName: "theme1_icon32"),
                    #imageLiteral(resourceName: "theme1_icon33"),
                    #imageLiteral(resourceName: "theme1_icon34"),
                    #imageLiteral(resourceName: "theme1_icon35"),
                    #imageLiteral(resourceName: "theme1_icon36"),
                    #imageLiteral(resourceName: "theme1_icon37"),
                    #imageLiteral(resourceName: "theme1_icon38"),
                    #imageLiteral(resourceName: "theme1_icon39"),
                    #imageLiteral(resourceName: "theme1_icon40"),
                    #imageLiteral(resourceName: "theme1_icon41"),
                    #imageLiteral(resourceName: "theme1_icon42"),
                    #imageLiteral(resourceName: "theme1_icon43"),
                    #imageLiteral(resourceName: "theme1_icon44"),
                    #imageLiteral(resourceName: "theme1_icon45")]
        case .theme2:
            return [#imageLiteral(resourceName: "theme2_icon1"),
                    #imageLiteral(resourceName: "theme2_icon2"),
                    #imageLiteral(resourceName: "theme2_icon3"),
                    #imageLiteral(resourceName: "theme2_icon4"),
                    #imageLiteral(resourceName: "theme2_icon5"),
                    #imageLiteral(resourceName: "theme2_icon6"),
                    #imageLiteral(resourceName: "theme2_icon7"),
                    #imageLiteral(resourceName: "theme2_icon8"),
                    #imageLiteral(resourceName: "theme2_icon9"),
                    #imageLiteral(resourceName: "theme2_icon10"),
                    #imageLiteral(resourceName: "theme2_icon11"),
                    #imageLiteral(resourceName: "theme2_icon12"),
                    #imageLiteral(resourceName: "theme2_icon13"),
                    #imageLiteral(resourceName: "theme2_icon14"),
                    #imageLiteral(resourceName: "theme2_icon15"),
                    #imageLiteral(resourceName: "theme2_icon16"),
                    #imageLiteral(resourceName: "theme2_icon17"),
                    #imageLiteral(resourceName: "theme2_icon18"),
                    #imageLiteral(resourceName: "theme2_icon19"),
                    #imageLiteral(resourceName: "theme2_icon20"),
                    #imageLiteral(resourceName: "theme2_icon21"),
                    #imageLiteral(resourceName: "theme2_icon22"),
                    #imageLiteral(resourceName: "theme2_icon23"),
                    #imageLiteral(resourceName: "theme2_icon24"),
                    #imageLiteral(resourceName: "theme2_icon25"),
                    #imageLiteral(resourceName: "theme2_icon26"),
                    #imageLiteral(resourceName: "theme2_icon27"),
                    #imageLiteral(resourceName: "theme2_icon28"),
                    #imageLiteral(resourceName: "theme2_icon29"),
                    #imageLiteral(resourceName: "theme2_icon30"),
                    #imageLiteral(resourceName: "theme2_icon31"),
                    #imageLiteral(resourceName: "theme2_icon32"),
                    #imageLiteral(resourceName: "theme2_icon33"),
                    #imageLiteral(resourceName: "theme2_icon34"),
                    #imageLiteral(resourceName: "theme2_icon35"),
                    #imageLiteral(resourceName: "theme2_icon36"),
                    #imageLiteral(resourceName: "theme2_icon37"),
                    #imageLiteral(resourceName: "theme2_icon38"),
                    #imageLiteral(resourceName: "theme2_icon39"),
                    #imageLiteral(resourceName: "theme2_icon40"),
                    #imageLiteral(resourceName: "theme2_icon41")]
        case .theme3:
            return [#imageLiteral(resourceName: "theme3_icon1"),
                    #imageLiteral(resourceName: "theme3_icon2"),
                    #imageLiteral(resourceName: "theme3_icon3"),
                    #imageLiteral(resourceName: "theme3_icon4"),
                    #imageLiteral(resourceName: "theme3_icon5"),
                    #imageLiteral(resourceName: "theme3_icon6"),
                    #imageLiteral(resourceName: "theme3_icon7"),
                    #imageLiteral(resourceName: "theme3_icon8"),
                    #imageLiteral(resourceName: "theme3_icon9"),
                    #imageLiteral(resourceName: "theme3_icon10"),
                    #imageLiteral(resourceName: "theme3_icon11"),
                    #imageLiteral(resourceName: "theme3_icon12"),
                    #imageLiteral(resourceName: "theme3_icon13"),
                    #imageLiteral(resourceName: "theme3_icon14"),
                    #imageLiteral(resourceName: "theme3_icon15"),
                    #imageLiteral(resourceName: "theme3_icon16"),
                    #imageLiteral(resourceName: "theme3_icon17"),
                    #imageLiteral(resourceName: "theme3_icon18"),
                    #imageLiteral(resourceName: "theme3_icon19"),
                    #imageLiteral(resourceName: "theme3_icon20"),
                    #imageLiteral(resourceName: "theme3_icon21"),
                    #imageLiteral(resourceName: "theme3_icon22"),
                    #imageLiteral(resourceName: "theme3_icon23"),
                    #imageLiteral(resourceName: "theme3_icon24"),
                    #imageLiteral(resourceName: "theme3_icon25"),
                    #imageLiteral(resourceName: "theme3_icon26"),
                    #imageLiteral(resourceName: "theme3_icon27"),
                    #imageLiteral(resourceName: "theme3_icon28"),
                    #imageLiteral(resourceName: "theme3_icon29"),
                    #imageLiteral(resourceName: "theme3_icon30"),
                    #imageLiteral(resourceName: "theme3_icon31"),
                    #imageLiteral(resourceName: "theme3_icon32"),
                    #imageLiteral(resourceName: "theme3_icon33"),
                    #imageLiteral(resourceName: "theme3_icon34"),
                    #imageLiteral(resourceName: "theme3_icon35"),
                    #imageLiteral(resourceName: "theme3_icon36"),
                    #imageLiteral(resourceName: "theme3_icon37"),
                    #imageLiteral(resourceName: "theme3_icon38"),
                    #imageLiteral(resourceName: "theme3_icon39"),
                    #imageLiteral(resourceName: "theme3_icon40")]
        case .theme4:
            return [#imageLiteral(resourceName: "theme4_icon1"),
                    #imageLiteral(resourceName: "theme4_icon2"),
                    #imageLiteral(resourceName: "theme4_icon3"),
                    #imageLiteral(resourceName: "theme4_icon4"),
                    #imageLiteral(resourceName: "theme4_icon5"),
                    #imageLiteral(resourceName: "theme4_icon6"),
                    #imageLiteral(resourceName: "theme4_icon7"),
                    #imageLiteral(resourceName: "theme4_icon8"),
                    #imageLiteral(resourceName: "theme4_icon9"),
                    #imageLiteral(resourceName: "theme4_icon10"),
                    #imageLiteral(resourceName: "theme4_icon11"),
                    #imageLiteral(resourceName: "theme4_icon12"),
                    #imageLiteral(resourceName: "theme4_icon13"),
                    #imageLiteral(resourceName: "theme4_icon14"),
                    #imageLiteral(resourceName: "theme4_icon15"),
                    #imageLiteral(resourceName: "theme4_icon16"),
                    #imageLiteral(resourceName: "theme4_icon17"),
                    #imageLiteral(resourceName: "theme4_icon18"),
                    #imageLiteral(resourceName: "theme4_icon19"),
                    #imageLiteral(resourceName: "theme4_icon20"),
                    #imageLiteral(resourceName: "theme4_icon21"),
                    #imageLiteral(resourceName: "theme4_icon22"),
                    #imageLiteral(resourceName: "theme4_icon23"),
                    #imageLiteral(resourceName: "theme4_icon24"),
                    #imageLiteral(resourceName: "theme4_icon25"),
                    #imageLiteral(resourceName: "theme4_icon26"),
                    #imageLiteral(resourceName: "theme4_icon27"),
                    #imageLiteral(resourceName: "theme4_icon28"),
                    #imageLiteral(resourceName: "theme4_icon29"),
                    #imageLiteral(resourceName: "theme4_icon30"),
                    #imageLiteral(resourceName: "theme4_icon31"),
                    #imageLiteral(resourceName: "theme4_icon32"),
                    #imageLiteral(resourceName: "theme4_icon33"),
                    #imageLiteral(resourceName: "theme4_icon34"),
                    #imageLiteral(resourceName: "theme4_icon35"),
                    #imageLiteral(resourceName: "theme4_icon36"),
                    #imageLiteral(resourceName: "theme4_icon37"),
                    #imageLiteral(resourceName: "theme4_icon38"),
                    #imageLiteral(resourceName: "theme4_icon39"),
                    #imageLiteral(resourceName: "theme4_icon40"),
                    #imageLiteral(resourceName: "theme4_icon41"),
                    #imageLiteral(resourceName: "theme4_icon42")]
        case .theme5:
            return [#imageLiteral(resourceName: "theme5_icon1"),
                    #imageLiteral(resourceName: "theme5_icon2"),
                    #imageLiteral(resourceName: "theme5_icon3"),
                    #imageLiteral(resourceName: "theme5_icon4"),
                    #imageLiteral(resourceName: "theme5_icon5"),
                    #imageLiteral(resourceName: "theme5_icon6"),
                    #imageLiteral(resourceName: "theme5_icon7"),
                    #imageLiteral(resourceName: "theme5_icon8"),
                    #imageLiteral(resourceName: "theme5_icon9"),
                    #imageLiteral(resourceName: "theme5_icon10"),
                    #imageLiteral(resourceName: "theme5_icon11"),
                    #imageLiteral(resourceName: "theme5_icon12"),
                    #imageLiteral(resourceName: "theme5_icon13"),
                    #imageLiteral(resourceName: "theme5_icon14"),
                    #imageLiteral(resourceName: "theme5_icon15"),
                    #imageLiteral(resourceName: "theme5_icon16"),
                    #imageLiteral(resourceName: "theme5_icon17"),
                    #imageLiteral(resourceName: "theme5_icon18"),
                    #imageLiteral(resourceName: "theme5_icon19"),
                    #imageLiteral(resourceName: "theme5_icon20"),
                    #imageLiteral(resourceName: "theme5_icon21"),
                    #imageLiteral(resourceName: "theme5_icon22"),
                    #imageLiteral(resourceName: "theme5_icon23"),
                    #imageLiteral(resourceName: "theme5_icon24"),
                    #imageLiteral(resourceName: "theme5_icon25"),
                    #imageLiteral(resourceName: "theme5_icon26"),
                    #imageLiteral(resourceName: "theme5_icon27"),
                    #imageLiteral(resourceName: "theme5_icon28"),
                    #imageLiteral(resourceName: "theme5_icon29"),
                    #imageLiteral(resourceName: "theme5_icon30"),
                    #imageLiteral(resourceName: "theme5_icon31"),
                    #imageLiteral(resourceName: "theme5_icon32"),
                    #imageLiteral(resourceName: "theme5_icon33"),
                    #imageLiteral(resourceName: "theme5_icon34"),
                    #imageLiteral(resourceName: "theme5_icon35"),
                    #imageLiteral(resourceName: "theme5_icon36"),
                    #imageLiteral(resourceName: "theme5_icon37"),
                    #imageLiteral(resourceName: "theme5_icon38"),
                    #imageLiteral(resourceName: "theme5_icon39"),
                    #imageLiteral(resourceName: "theme5_icon40"),
                    #imageLiteral(resourceName: "theme5_icon41"),
                    #imageLiteral(resourceName: "theme5_icon42"),
                    #imageLiteral(resourceName: "theme5_icon43"),
                    #imageLiteral(resourceName: "theme5_icon44"),
                    #imageLiteral(resourceName: "theme5_icon45")]
        case .theme6:
            return [#imageLiteral(resourceName: "theme6_icon1"),
                    #imageLiteral(resourceName: "theme6_icon2"),
                    #imageLiteral(resourceName: "theme6_icon3"),
                    #imageLiteral(resourceName: "theme6_icon4"),
                    #imageLiteral(resourceName: "theme6_icon5"),
                    #imageLiteral(resourceName: "theme6_icon6"),
                    #imageLiteral(resourceName: "theme6_icon7"),
                    #imageLiteral(resourceName: "theme6_icon8"),
                    #imageLiteral(resourceName: "theme6_icon9"),
                    #imageLiteral(resourceName: "theme6_icon10"),
                    #imageLiteral(resourceName: "theme6_icon11"),
                    #imageLiteral(resourceName: "theme6_icon12"),
                    #imageLiteral(resourceName: "theme6_icon14"),
                    #imageLiteral(resourceName: "theme6_icon15"),
                    #imageLiteral(resourceName: "theme6_icon16"),
                    #imageLiteral(resourceName: "theme6_icon17"),
                    #imageLiteral(resourceName: "theme6_icon18"),
                    #imageLiteral(resourceName: "theme6_icon19"),
                    #imageLiteral(resourceName: "theme6_icon20"),
                    #imageLiteral(resourceName: "theme6_icon21"),
                    #imageLiteral(resourceName: "theme6_icon22"),
                    #imageLiteral(resourceName: "theme6_icon23"),
                    #imageLiteral(resourceName: "theme6_icon24"),
                    #imageLiteral(resourceName: "theme6_icon25"),
                    #imageLiteral(resourceName: "theme6_icon26"),
                    #imageLiteral(resourceName: "theme6_icon27"),
                    #imageLiteral(resourceName: "theme6_icon28"),
                    #imageLiteral(resourceName: "theme6_icon29")]
        case .theme7:
            return [#imageLiteral(resourceName: "theme7_icon1"),
                    #imageLiteral(resourceName: "theme7_icon2"),
                    #imageLiteral(resourceName: "theme7_icon3"),
                    #imageLiteral(resourceName: "theme7_icon4"),
                    #imageLiteral(resourceName: "theme7_icon5"),
                    #imageLiteral(resourceName: "theme7_icon6"),
                    #imageLiteral(resourceName: "theme7_icon7"),
                    #imageLiteral(resourceName: "theme7_icon8"),
                    #imageLiteral(resourceName: "theme7_icon9"),
                    #imageLiteral(resourceName: "theme7_icon10"),
                    #imageLiteral(resourceName: "theme7_icon11"),
                    #imageLiteral(resourceName: "theme7_icon12"),
                    #imageLiteral(resourceName: "theme7_icon13"),
                    #imageLiteral(resourceName: "theme7_icon14"),
                    #imageLiteral(resourceName: "theme7_icon15"),
                    #imageLiteral(resourceName: "theme7_icon16"),
                    #imageLiteral(resourceName: "theme7_icon17"),
                    #imageLiteral(resourceName: "theme7_icon18"),
                    #imageLiteral(resourceName: "theme7_icon19"),
                    #imageLiteral(resourceName: "theme7_icon20"),
                    #imageLiteral(resourceName: "theme7_icon21"),
                    #imageLiteral(resourceName: "theme7_icon22"),
                    #imageLiteral(resourceName: "theme7_icon23"),
                    #imageLiteral(resourceName: "theme7_icon24"),
                    #imageLiteral(resourceName: "theme7_icon25"),
                    #imageLiteral(resourceName: "theme7_icon26"),
                    #imageLiteral(resourceName: "theme7_icon27"),
                    #imageLiteral(resourceName: "theme7_icon28"),
                    #imageLiteral(resourceName: "theme7_icon29"),
                    #imageLiteral(resourceName: "theme7_icon30"),
                    #imageLiteral(resourceName: "theme7_icon31"),
                    #imageLiteral(resourceName: "theme7_icon32"),
                    #imageLiteral(resourceName: "theme7_icon33"),
                    #imageLiteral(resourceName: "theme7_icon34"),
                    #imageLiteral(resourceName: "theme7_icon35"),
                    #imageLiteral(resourceName: "theme7_icon36"),]
        case .theme8:
            return [#imageLiteral(resourceName: "theme8_icon1"),
                    #imageLiteral(resourceName: "theme8_icon2"),
                    #imageLiteral(resourceName: "theme8_icon3"),
                    #imageLiteral(resourceName: "theme8_icon4"),
                    #imageLiteral(resourceName: "theme8_icon5"),
                    #imageLiteral(resourceName: "theme8_icon6"),
                    #imageLiteral(resourceName: "theme8_icon7"),
                    #imageLiteral(resourceName: "theme8_icon8"),
                    #imageLiteral(resourceName: "theme8_icon9"),
                    #imageLiteral(resourceName: "theme8_icon10"),
                    #imageLiteral(resourceName: "theme8_icon11"),
                    #imageLiteral(resourceName: "theme8_icon12"),
                    #imageLiteral(resourceName: "theme8_icon13"),
                    #imageLiteral(resourceName: "theme8_icon14"),
                    #imageLiteral(resourceName: "theme8_icon15"),
                    #imageLiteral(resourceName: "theme8_icon16"),
                    #imageLiteral(resourceName: "theme8_icon17"),
                    #imageLiteral(resourceName: "theme8_icon18"),
                    #imageLiteral(resourceName: "theme8_icon19"),
                    #imageLiteral(resourceName: "theme8_icon20"),
                    #imageLiteral(resourceName: "theme8_icon21"),
                    #imageLiteral(resourceName: "theme8_icon22"),
                    #imageLiteral(resourceName: "theme8_icon23"),
                    #imageLiteral(resourceName: "theme8_icon24"),
                    #imageLiteral(resourceName: "theme8_icon25"),
                    #imageLiteral(resourceName: "theme8_icon26"),
                    #imageLiteral(resourceName: "theme8_icon27"),
                    #imageLiteral(resourceName: "theme8_icon28"),
                    #imageLiteral(resourceName: "theme8_icon29"),
                    #imageLiteral(resourceName: "theme8_icon30"),
                    #imageLiteral(resourceName: "theme8_icon31"),
                    #imageLiteral(resourceName: "theme8_icon32"),
                    #imageLiteral(resourceName: "theme8_icon33"),
                    #imageLiteral(resourceName: "theme8_icon34"),
                    #imageLiteral(resourceName: "theme8_icon35"),
                    #imageLiteral(resourceName: "theme8_icon36"),
                    #imageLiteral(resourceName: "theme8_icon37"),
                    #imageLiteral(resourceName: "theme8_icon38"),
                    #imageLiteral(resourceName: "theme8_icon39")]
        case .theme9:
            return [#imageLiteral(resourceName: "theme9_icon1"),
                    #imageLiteral(resourceName: "theme9_icon2"),
                    #imageLiteral(resourceName: "theme9_icon3"),
                    #imageLiteral(resourceName: "theme9_icon4"),
                    #imageLiteral(resourceName: "theme9_icon5"),
                    #imageLiteral(resourceName: "theme9_icon6"),
                    #imageLiteral(resourceName: "theme9_icon7"),
                    #imageLiteral(resourceName: "theme9_icon8"),
                    #imageLiteral(resourceName: "theme9_icon9"),
                    #imageLiteral(resourceName: "theme9_icon10"),
                    #imageLiteral(resourceName: "theme9_icon11"),
                    #imageLiteral(resourceName: "theme9_icon12"),
                    #imageLiteral(resourceName: "theme9_icon13"),
                    #imageLiteral(resourceName: "theme9_icon14"),
                    #imageLiteral(resourceName: "theme9_icon15"),
                    #imageLiteral(resourceName: "theme9_icon16"),
                    #imageLiteral(resourceName: "theme9_icon17"),
                    #imageLiteral(resourceName: "theme9_icon18"),
                    #imageLiteral(resourceName: "theme9_icon19"),
                    #imageLiteral(resourceName: "theme9_icon20"),
                    #imageLiteral(resourceName: "theme9_icon21"),
                    #imageLiteral(resourceName: "theme9_icon22"),
                    #imageLiteral(resourceName: "theme9_icon23"),
                    #imageLiteral(resourceName: "theme9_icon24"),
                    #imageLiteral(resourceName: "theme9_icon25"),
                    #imageLiteral(resourceName: "theme9_icon26"),
                    #imageLiteral(resourceName: "theme9_icon27"),
                    #imageLiteral(resourceName: "theme9_icon28"),
                    #imageLiteral(resourceName: "theme9_icon29"),
                    #imageLiteral(resourceName: "theme9_icon30"),
                    #imageLiteral(resourceName: "theme9_icon31"),
                    #imageLiteral(resourceName: "theme9_icon32"),
                    #imageLiteral(resourceName: "theme9_icon33")]
        }

    }

    var numberOfIcon: Int {
        return self.iconMaterials.count
    }
}
