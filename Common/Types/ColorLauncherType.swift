//
//  ComposeType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum ColorLauncherType: String, CaseIterable, Codable {
    case none = "none"
    case color1 = "color1"
    case color2 = "color2"
    case color3 = "color3"
    case color4 = "color4"
    case color5 = "color5"
    case color6 = "color6"
    case color7 = "color7"
    case color8 = "color8"
    case color9 = "color9"
    case color10 = "color10"
    case color11 = "color11"
    case color12 = "color12"
    case color13 = "color13"
    case color14 = "color14"
    case color15 = "color15"
    case color16 = "color16"
    case color17 = "color17"
    case color18 = "color18"
    case color19 = "color19"
    case color20 = "color20"
    case color21 = "color21"
    case color22 = "color22"
    case color23 = "color23"
    case color24 = "color24"
    case color25 = "color25"
    case color26 = "color26"
    case color27 = "color27"
    case color28 = "color28"
    case color29 = "color29"
    case color30 = "color30"
    case color31 = "color31"
    case color32 = "color32"
    case color33 = "color33"
    case color34 = "color34"
    case color35 = "color35"
    case color36 = "color36"
    case color37 = "color37"
    case color38 = "color38"
    case color39 = "color39"
    case color40 = "color40"
    case color41 = "color41"
    case color42 = "color42"
    case color43 = "color43"
    case color44 = "color44"
    case color45 = "color45"
    case color46 = "color46"
    case color47 = "color47"
    
    var color: (UIColor, UIColor) {
        switch self {
        case .none:
            return (.white, .white)
        case .color1:
            return (.black, .black)
        case .color2:
            return (.hex("F8D449"), .hex("F2A73C"))
        case .color3:
            return (.hex("AB5AF0"), .hex("461096"))
        case .color4:
            return (.hex("F6D591"), .hex("E4937B"))
        case .color5:
            return (.hex("4C3F85"), .hex("57ACCA"))
        case .color6:
            return (.hex("782DC6"), .hex("CD597B"))
        case .color7:
            return (.hex("787878"), .hex("0A0A0A"))
        case .color8:
            return (.hex("62C561"), .hex("387E31"))
        case .color9:
            return (.hex("5B59DB"), .hex("0C0B73"))
        case .color10:
            return (.hex("355D69"), .hex("66AF80"))
        case .color11:
            return (.hex("65688C"), .hex("582D66"))
        case .color12:
            return (.hex("E3649A"), .hex("EC7675"))
        case .color13:
            return (.hex("E55343"), .hex("B94538"))
        case .color14:
            return (.hex("7DCAF9"), .hex("3260E7"))
        case .color15:
            return (.hex("D02F53"), .hex("A02075"))
        case .color16:
            return (.hex("74D39B"), .hex("62B1AD"))
        case .color17:
            return (.hex("712E6F"), .hex("A23660"))
        case .color18:
            return (.hex("C04EBE"), .hex("7489ED"))
        case .color19:
            return (.hex("F1A03E"), .hex("ED7741"))
        case .color20:
            return (.hex("367AF0"), .hex("1130B4"))
        case .color21:
            return (.hex("EC993A"), .hex("912214"))
        case .color22:
            return (.hex("63D5C5"), .hex("5E8DD9"))
        case .color23:
            return (.hex("D24F58"), .hex("1516A9"))
        case .color24:
            return (.hex("FBEA8C"), .hex("FBEA8C"))
        case .color25:
            return (.hex("F8D74A"), .hex("F8D74A"))
        case .color26:
            return (.hex("B57528"), .hex("B57528"))
        case .color27:
            return (.hex("9493F8"), .hex("9493F8"))
        case .color28:
            return (.hex("5D5CDE"), .hex("5D5CDE"))
        case .color29:
            return (.hex("0200B7"), .hex("0200B7"))
        case .color30:
            return (.hex("000000"), .hex("000000"))
        case .color31:
            return (.hex("A1FCA8"), .hex("A1FCA8"))
        case .color32:
            return (.hex("B12718"), .hex("B12718"))
        case .color33:
            return (.hex("55BC48"), .hex("55BC48"))
        case .color34:
            return (.hex("D2A3F9"), .hex("D2A3F9"))
        case .color35:
            return (.hex("B25FF7"), .hex("B25FF7"))
        case .color36:
            return (.hex("6B11B7"), .hex("6B11B7"))
        case .color37:
            return (.hex("EF8C82"), .hex("EF8C82"))
        case .color38:
            return (.hex("EB5545"), .hex("EB5545"))
        case .color39:
            return (.hex("67CE67"), .hex("67CE67"))
        case .color40:
            return (.hex("BBE6FC"), .hex("BBE6FC"))
        case .color41:
            return (.hex("81CFFA"), .hex("81CFFA"))
        case .color42:
            return (.hex("3C89BE"), .hex("3C89BE"))
        case .color43:
            return (.hex("F7CD89"), .hex("F7CD89"))
        case .color44:
            return (.hex("F1A33E"), .hex("F1A33E"))
        case .color45:
            return (.hex("BBA134"), .hex("BBA134"))
        case .color46:
            return (.hex("3A82F7"), .hex("3A82F7"))
        case .color47:
            return (.hex("275DB8"), .hex("275DB8"))
        }
    }
}

