//
//  CreateIconType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import Foundation

enum CreateIconType: String, Codable {
    case textIcon = "textIcon"
    case photoIcon = "photoIcon"
}
