//
//  ComposeType.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 7/21/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

enum ImageShapeType: String, CaseIterable, Codable {
    case none = "none"
    case imageShape1 = "imageShape1"
    case imageShape2 = "imageShape2"
    case imageShape3 = "imageShape3"
    case imageShape4 = "imageShape4"
    case imageShape5 = "imageShape5"
    case imageShape6 = "imageShape6"
    case imageShape7 = "imageShape7"
    case imageShape8 = "imageShape8"
    case imageShape9 = "imageShape9"
    
    var image: UIImage? {
        switch self {
        case .none:
            return nil
        case .imageShape1:
            return #imageLiteral(resourceName: "imageShape1")
        case .imageShape2:
            return #imageLiteral(resourceName: "imageShape2")
        case .imageShape3:
            return #imageLiteral(resourceName: "imageShape3")
        case .imageShape4:
            return #imageLiteral(resourceName: "imageShape4")
        case .imageShape5:
            return #imageLiteral(resourceName: "imageShape5")
        case .imageShape6:
            return #imageLiteral(resourceName: "imageShape6")
        case .imageShape7:
            return #imageLiteral(resourceName: "imageShape7")
        case .imageShape8:
            return #imageLiteral(resourceName: "imageShape8")
        case .imageShape9:
            return #imageLiteral(resourceName: "imageShape9")
        }
    }
}
