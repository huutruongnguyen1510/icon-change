//
//  WidgetType.swift
//  IconHomeScreen
//
//  Created by nguyen.viet.luy on 10/30/20.
//

import UIKit

enum BackgroundLauncherImageType: String, CaseIterable, Codable {
    case type1
    case type2
    case type3
    case type4
    case type5
    case type6
    case type7
    case type8
    case type9
    case type10
    case type11
    case type12
    case type13
    case type14
    case type15
    case type16
    case type17
    
    var mediumImage: UIImage {
        switch self {
        case .type1:
            return #imageLiteral(resourceName: "wgbg_medium_1")
        case .type2:
            return #imageLiteral(resourceName: "wgbg_medium_2")
        case .type3:
            return #imageLiteral(resourceName: "wgbg_medium_3")
        case .type4:
            return #imageLiteral(resourceName: "wgbg_medium_4")
        case .type5:
            return #imageLiteral(resourceName: "wgbg_medium_5")
        case .type6:
            return #imageLiteral(resourceName: "wgbg_medium_6")
        case .type7:
            return #imageLiteral(resourceName: "wgbg_medium_7")
        case .type8:
            return #imageLiteral(resourceName: "wgbg_medium_8")
        case .type9:
            return #imageLiteral(resourceName: "wgbg_medium_9")
        case .type10:
            return #imageLiteral(resourceName: "wgbg_medium_10")
        case .type11:
            return #imageLiteral(resourceName: "wgbg_medium_11")
        case .type12:
            return #imageLiteral(resourceName: "wgbg_medium_12")
        case .type13:
            return #imageLiteral(resourceName: "wgbg_medium_13")
        case .type14:
            return #imageLiteral(resourceName: "wgbg_medium_14")
        case .type15:
            return #imageLiteral(resourceName: "wgbg_medium_15")
        case .type16:
            return #imageLiteral(resourceName: "wgbg_medium_16")
        case .type17:
            return #imageLiteral(resourceName: "wgbg_medium_17")
        }
    }
    
    var largeImage: UIImage {
        switch self {
        case .type1:
            return #imageLiteral(resourceName: "wgbg_large_1")
        case .type2:
            return #imageLiteral(resourceName: "wgbg_large_2")
        case .type3:
            return #imageLiteral(resourceName: "wgbg_large_3")
        case .type4:
            return #imageLiteral(resourceName: "wgbg_large_4")
        case .type5:
            return #imageLiteral(resourceName: "wgbg_large_5")
        case .type6:
            return #imageLiteral(resourceName: "wgbg_large_6")
        case .type7:
            return #imageLiteral(resourceName: "wgbg_large_7")
        case .type8:
            return #imageLiteral(resourceName: "wgbg_large_8")
        case .type9:
            return #imageLiteral(resourceName: "wgbg_large_9")
        case .type10:
            return #imageLiteral(resourceName: "wgbg_large_10")
        case .type11:
            return #imageLiteral(resourceName: "wgbg_large_11")
        case .type12:
            return #imageLiteral(resourceName: "wgbg_large_12")
        case .type13:
            return #imageLiteral(resourceName: "wgbg_large_13")
        case .type14:
            return #imageLiteral(resourceName: "wgbg_large_14")
        case .type15:
            return #imageLiteral(resourceName: "wgbg_large_15")
        case .type16:
            return #imageLiteral(resourceName: "wgbg_large_16")
        case .type17:
            return #imageLiteral(resourceName: "wgbg_large_17")
        }
    }
}
