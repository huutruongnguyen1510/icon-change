//
//  PathContentWidgetType.swift
//  IconHomeScreen
//
//  Created by nguyen.viet.luy on 10/30/20.
//

import Foundation

enum PathContentWidgetType {
    static let medium = "contentsMedium.json"
    static let large = "contentsLarge.json"
}
