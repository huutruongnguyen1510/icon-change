//
//  StatusViewMyIcon.swift
//  IconHomeScreen
//
//  Created by nguyen.viet.luy on 10/26/20.
//

enum StatusViewMyIcon: String {
    case edit = "Edit"
    case done = "Done"
}
