//
//  Utilities.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit
import CoreLocation

func openSetting(completion: (() -> Void) = {}) {
    if let settingUrl = URL(string: UIApplication.openSettingsURLString) {
        if UIApplication.shared.canOpenURL(settingUrl) {
            UIApplication.shared.open(settingUrl)
            completion()
        }
    }
}

func openGoogleMaps(coordinate: CLLocationCoordinate2D) {
    let application = UIApplication.shared
    if let appURL = OpenMapType.app(coordinate).url, application.canOpenURL(appURL) {
        application.open(appURL)
    } else if let browserURL = OpenMapType.browser(coordinate).url {
        application.open(browserURL)
    }
}
