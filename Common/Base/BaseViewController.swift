//
//  BaseViewController.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import LinkPresentation

class BaseViewController: UIViewController {

    // MARK: - Public Properties
    internal var bag = DisposeBag()

    internal var isShowBack = true {
        didSet {
            configBackButton()
        }
    }

    internal var hasSafeArea: Bool {
        guard #available(iOS 11.0, *),
        let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top,
            topPadding > 24 else { return false }
        return true
    }
    
    internal var sharingMessage: String?
    
    @available(iOS 13.0, *)
    internal lazy var linkMetadata: LPLinkMetadata? = nil

    // MARK: - Private Properties
    private lazy var backButton = UIButton().with {
        $0.imageView?.contentMode = .scaleAspectFit
        $0.imageEdgeInsets = UIEdgeInsets(top: 10, left: -5, bottom: 10, right: 25)
        $0.setImage(#imageLiteral(resourceName: "ic_back.pdf"), for: .normal)
        $0.tintColor = .red
        $0.addTarget(self, action: #selector(self.backTapped), for: .touchUpInside)
    }
    
//    private var backgroundImage = UIImageView().with {
//        $0.image = isIPad ? #imageLiteral(resourceName: "backgroundImage_iPad") : #imageLiteral(resourceName: "backgroundImage")
//        $0.contentMode = .scaleToFill
//    }

    // MARK: - Override
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        defaultStyle()
        setupUI()
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //view.insertSubview(backgroundImage, at: 0)
        //backgroundImage.frame = view.bounds
    }

    // MARK: - Public Functions
    internal func setupUI() {}
    
    internal func bindViewModel() {}
    
    deinit {
        debugPrint("\(String(describing: type(of: self))) \(#function)")
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Private Functions
extension BaseViewController {
    private func defaultStyle() {
        view.backgroundColor = .white
        
    }
    
    private func configNavigationBar() {
        navigationController?.topViewController?.title = title
    }

    private func configBackButton() {
        if isShowBack {
            navigationItem.backBarButtonItem = UIBarButtonItem(
                title: nil,
                style: .plain,
                target: nil,
                action: nil)
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            let backBarButton = UIBarButtonItem(customView: backButton)
            navigationItem.leftBarButtonItems = [spaceButton, backBarButton]
        } else {
            navigationItem.leftBarButtonItem = nil
            navigationItem.setHidesBackButton(true, animated: true)
        }
    }

    @objc private func backTapped() {
        navigationController?.popViewController(animated: true)
    }

    private func handlePresentAlert(_ alertVC: UIViewController) {
        alertVC.modalTransitionStyle = .crossDissolve
        alertVC.modalPresentationStyle = .overCurrentContext
        navigationController?.present(alertVC, animated: true)
    }
}

// MARK: - Public Functions
extension BaseViewController {
    
}

 // MARK: - UIActivityItemSource
extension BaseViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }

    func activityViewController(_ activityViewController: UIActivityViewController,
                                itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return sharingMessage
    }

    @available(iOS 13.0, *)
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        return linkMetadata
    }
}
