//
//  BaseNavigationController.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    
//    // MARK: - Properties
//    private lazy var titleLabel = FontLabel().with {
//        $0.lineBreakMode = .byTruncatingTail
//        $0.font = UIFont.systemFont(ofSize: 17)
//        $0.adjustsFontSizeToFitWidth = true
//        $0.minimumScaleFactor = 0.8
//        $0.textAlignment = .center
//    }
    
    // MARK: - Override
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
}

// MARK: - Private Functions
extension BaseNavigationController {
    
    private func setupNavigationBar() {
//        titleLabel.text = title
        let attribute = [NSAttributedString.Key.foregroundColor: UIColor.white,
                         NSAttributedString.Key.font: UIFont.extraBold(size: 18)]
        navigationBar.do {
            $0.barTintColor = UIColor.white
            $0.isTranslucent = false
            $0.shadowImage = UIImage()
            $0.titleTextAttributes = attribute
            $0.backIndicatorImage = #imageLiteral(resourceName: "ic_back")
            $0.tintColor = .black
            $0.setBackgroundImage(#imageLiteral(resourceName: "ic_back"), for: .default)
        }
        delegate = self
        interactivePopGestureRecognizer?.delegate = self
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: true)
        viewController.removeBackButtonTitle()
    }
}

// MARK: - Navigation delegate
extension BaseNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 1 {
            interactivePopGestureRecognizer?.isEnabled = true
        } else {
            interactivePopGestureRecognizer?.isEnabled = false
        }
    }
}
