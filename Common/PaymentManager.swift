//
//  PaymentManager.swift
//  MasterCleaner
//
//  Created by Nhuom Tang on 7/15/19.
//  Copyright © 2019 Nhuom Tang. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class PaymentManager: NSObject {
    
    var isVerifyError = true
    
    static let shared = PaymentManager()
  
    func isPurchase()->Bool{
        if let time = UserDefaults.standard.value(forKey: "purchaseTime") as? TimeInterval{
            let timeInterval = Date().timeIntervalSince1970
            if timeInterval > time{
                return false
            }
            return true
        }
        return false
    }
    
    func savePurchase(time: TimeInterval){
        UserDefaults.standard.setValue(time, forKey: "purchaseTime")
    }
        
    func verifyPurchase(){
        var appleValidator = AppleReceiptValidator(service: .production, sharedSecret: PRODUCT_SHARED_SECRET)
        if iS_TEST{
            appleValidator = AppleReceiptValidator(service: .sandbox, sharedSecret: PRODUCT_SHARED_SECRET)
        }
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { [weak self] result in
            switch result {
            case .success(let receipt):
                self?.isVerifyError = false
                let productIds = Set.init(PRODUCT_IDS)
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                switch purchaseResult {
                case .purchased(let expiryDate, _):
                    let timeInterval = expiryDate.timeIntervalSince1970
                    PaymentManager.shared.savePurchase(time: timeInterval)
                case .expired(let expiryDate, _):
                    if let vc = UIApplication.shared.keyWindow?.rootViewController{
                        let dateFormatterPrint = DateFormatter()
                        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
                        let str = dateFormatterPrint.string(from: expiryDate)
//                        vc.showError(message: "Your purchase is expired since " + str)
                        let timeInterval = expiryDate.timeIntervalSince1970
                        PaymentManager.shared.savePurchase(time: timeInterval)
                    }
                case .notPurchased:
                    print("not purchased")
                }
            case .error(let error):
                print(error.localizedDescription)
                self?.isVerifyError = true
            }
        }
    }
}

