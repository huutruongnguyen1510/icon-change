//
//  SKStoreReviewHepler.swift
//  FontKeyboards
//
//  Created by nguyen.viet.luy on 8/5/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit
import StoreKit

class SKStoreReviewHepler: NSObject {
    static let shared = SKStoreReviewHepler()

    func showReview() {
        if #available(iOS 10.3, *) {
            print("Review Requested")
            SKStoreReviewController.requestReview()
        }
    }
}
