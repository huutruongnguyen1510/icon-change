//
//  Common.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

class Common {
    
    class func getFontForDeviceWithFontDefault(fontDefault:UIFont) -> UIFont {
        var font:UIFont = fontDefault
        var sizeScale: CGFloat = 1
        if DeviceType.IS_IPAD {
            sizeScale = 1
        } else if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_7 {
            sizeScale = 0.95
        } else if DeviceType.IS_IPHONE_5 {
            sizeScale = 0.9
        }
        font = UIFont(name: font.fontName, size: font.pointSize * sizeScale) ?? UIFont()
        return font
    }
    
    class func generateID() -> String {
        return UUID().uuidString
    }
    
    class func viewNoData(title: String) -> UIView{
        if let window = UIApplication.shared.keyWindow {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: window.bounds.size.width, height: window.bounds.size.height))
            noDataLabel.text          = title
            noDataLabel.textColor     = UIColor.gray
            noDataLabel.textAlignment = .center
            let font = UIFont.systemFont(ofSize: 16)
            noDataLabel.font = Common.getFontForDeviceWithFontDefault(fontDefault: font)
            return noDataLabel
        } else {
            return UIView()
        }
    }
    
    class func htmlFor(title: String, urlToRedirect: URL, iconBase64: String, pSplashBase64: String, lSplashBase64: String) -> String {
            return """
            <html>
                    <head>
                    <title>\(title)</title>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                    <meta name="apple-mobile-web-app-capable" content="yes">
                    <meta name="apple-mobile-web-app-status-bar-style" content="#ffffff">
                    <meta name="apple-mobile-web-app-title" content="\(title)">
                    <link rel="apple-touch-icon-precomposed" href="data:image/jpeg;base64,\(iconBase64)"/>
                    <link rel="apple-touch-startup-image" media="(orientation: landscape)" href="data:image/jpeg;base64,\(lSplashBase64)"/>
                    <link rel="apple-touch-startup-image" media="(orientation: portrait)" href="data:image/jpeg;base64,\(pSplashBase64)"/>
                    </head>
                    <body>
                    <a id="redirect" href=\(urlToRedirect.absoluteString)></a>
                <div class="container">
                    <div class="header">
                        <div class="center" style="margin-bottom: 10px;">
                            <img id="icon_image" src="data:image/png;base64, \(iconBase64)"/>
                        </div>
                        <div class="center text_bold">
                            \(title)
                        </div>
                    </div>
                    <div class="body">
                        <div class="center">
                            <p>Add Icon to desktop</p>
                        </div>
                        <div>
                            <p>1. Click the <img  class="click_button"src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABmJLR0QA/wD/AP+gvaeTAAAEPUlEQVR4nO2czW4URxDH/9WztnfiBQkeIOGWIBQOGIRipMSHJJATOeA8QfAJIUSMjcVlL8liCyIFKScnDxD5knCwiJUoziWRBXviQ+TExwMkwG6YNXinOOCREgTrnekeVzeun+RTT1f11k/T3WOpG1AURVEURVEUZatB0gMowt5JHh6oJF8S8BkAgPEDx/G5Zp0eCw8tN8EJ2DvJw4OVZBHA+y80/RlV4yMrdXokMa6iBCWgR/EzgpMQjIA+ip8RlIQgBOQofkYwErwXUKD4GUFI8FqARfEzvJfgrQAHxc/wWoKXAhwWP8NbCd4JKKH4GV5KMNID+C9jda4OVpIluC8+ALzX7SSLe+o8WELswngloJV0JgGMlpjiULyanCoxfm68EkDEH5afZTNy9I9XAsD4p/wc9HfpOXLglQAi8y0ALjFFSobmS4yfG68EXD1f/YUIxwC6CaDrMHQXoBsGOHb1q/hXh3Gt8W4buhH7zz7u+YZcO/9GUL/JqzdgK6IChFEBwlRcB9w3tbrbmO5xgD4GeBeA4f8/wY8AWo4omllpDN1ynb8IB6Zbe5iiBsAfALT9heZ/AboD5iU20XyzMXTbZW5nC9bIBA/QjmQOhBPoT2ybOR1tztau58njehEemW6/S2T+AFDr4/E1MC7V4nh6uU5refK8CidT0MgED9DOzmUQTqH/t6pGRBdc5LeBDF1Ef8UHgAoIp9udzuWxOjuZPZwIoB3JHMBHCvQcGx/nyMUYijA+zhGYxvL35E/aSTLrYgzWAvZNre5en3a2FoST+6c6b9uGsRZgqDuBgos5Ef22sEAuv3hzsbBAXRAvF+xeIZMetx2D/RRE9FHBnq007Z6xzm8Jp/wFgHahvqDDtvkdrAH8Zr7H8QCMH9M0Oph3B1QGzdna9YiigwB+AuNBvt78lm1+Fyv5tl6NIfxvZv175NOXtW2w7e352/tBv4SFUQHCqABhVIAwKkAYFSCMChBGBQijAoRRAcKoAGFUgDAqQBgVIIwKEEYFCKMChFEBwqgAYVSAMCpAGBUgjAoQRgUIowKEUQHCqABhVIAwKkAYFSCMChAmQAH08JVNuQ9YyBOgAP69R2PR815iBCcgomgGLz/T1YpMdG6zx2NLcAJWGkO3mNNRgJcAPHn+x0vM6SFfrj7Ig/O7IjaD9cN9h7ND3pJHXW0JUkBGyIXPCG4Ket1QAcKoAGFUgDAqQBgVIIyLbWgLPe5M2OiKsbBh66vwHbwBdN8+RqiYe9YRrMfA/LN1jGDhK7YRrAWwieYBOLlBMDCeIjXf2waxFtBsDN0G45JtnNBgxjfX5qp/2cZxsguqxfE0QIsuYoUBLW6L4xkXkZwIWK7TWq1aPQrG13i9p6M1ZlyoVatHXV3c6vw6sZGZ1XcMdz9/fqEd70L/l6L6Shugu8x8hdh852LaURRFURRFURRla/MMIIU584bY8ukAAAAASUVORK5CYII="> button  at the bottom of the page</p>
                        </div>
                        <div>
                            <p>2. Select |Add to Home Screen | from the pop-up menu</p>
                        </div>
                        <div class="content_images">
                            <img class="img_click" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbkAAABYCAYAAABoO4m7AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABPySURBVHgB7Z17jFzVfcd/szM7+559GhvvYpa4IGFVMiiN8R+l2QVMVCHFOERqU2F5iaqikBBD1SgopCUkBRk1Uo1QkFKpsVEi8Q/GOFJUFTvxRkQt67TFSK1txQ2M6rWNAe/L633M7sz0fM+ds757585jZ2fuvTPz/VjHs3v3zj3nnjtzvvf3OOeGpEKk0+ku9TKkyqAq21W5S5WuzO+EEELqk3imTKnyviqjqpwOhUJTUgFCUkYywrZfLHEbEkIIIaQ4RlU5pspbSvDiUibKInJK3IbUy3NCYSOEELJ+RlV5XondqKyTdYkcxY0QQkgFiavytBK7t6REShI5JW6D6uWQUNwIIYRUnsNiWXZxWSMNskaUwCHm9p5Q4AghhHjDiConlf6MyBop2pLLJJXANfmUEEIIIf5wUFl0Txe7c1EilxG4k2JNAyCEEEL85LQqe4pxXxYUuUz8DQI3KIQQQkgwiKsyXEjo8oocBY4QQkiAiatyd76J5IUST44KBY4QQkgwGRQrIaUr1w6RXH9Qb/pHCUgMbn5+QRYWEvo1kUhIMpmSVColhBBCvKGhoUEaGyMSiUSkqSkqra3N0tLSLAEAOoWkSNdkFFd3ZSZN85D4CERscnJGpqZmKGiEEBJAIHoQut7eLi1+PoNJ4wedG7NEzu84HATt6tUpLW6EEEKqg1is3W+xQ1zubmciipvIwYIbER+A5TYxMUXLjRBCqhBYdj09XVrwfGJUidywfcMqkVMC97BYySae88knE7TeCCGkBujqismGDT3iE8P2hZ2dIveheOymhNV26dLHOqmEEEJIbYDklIGBTTphxWNWWXMrtWeeKDAoHjM+/hEFjhBCaozFxYQ2YHxgKKNnGrvEPiceAxclOoIQQkjtAQMG47wPrOiZdldmMio/FA+ZmZmVK1c+FUIIIbVNX1+PdHfHxGO6sRKKseQeFg9ZWlrWWZSEEEJqH5+y5vUTc4zI7RYPwQlD6AghhNQ+Zv6zx3we/4Uya35NikdA3OLxcSGEEFJfbN26xetsy27U5un6lHRTEkJIfYIFPzxmCCI3JB7C6QKEEFKf+LDgxyBEbrt4BASOsThCCKlPEJvz2NDZDpHrEo+Ym6MVRwgh9Qwem+Yhd0HkBsUj8Cw4Qggh9YvHllwXnokwKB5BVyUhhJSXsbF35ZVXXpazZ8/ItWulx7x27NgpX/rSI6p8WSqJx8bOoKcP/qHIEUJI+XjllYNa4O655x4tULFYaauKzMzMyIkTx+WZZ74lFy9elCef3C+VIpn0dlI45smlxSPOn48LIYSQ9fPmm29oUXryyafKJkpGNH/609eVcO6USnH77YPiFRQ5QgipQh599CvK6hqXkyffkXKC44Kf/ex1qRReipznD/opFZjT8D2Pj5e2Wsrw8L26FMMzz/yN3HHHbfoD5Bc4V5T17lNN4Fxwd4pSS+dFSCU4depdeeCBB6Xc3HnnnXLu3BmpFTyNya2HF174gRw9+obs2/dVefbZv5UgAOFFgR+8VF94Lvbu/Yr09w/kvUsrZp9qAAHzJ554POumAucGN0ylA+GEVCuxWIeUG4xlGNdqhaqx5HDXAiB068kgKicnTrwt9913r34lpQHLHO4RfKkgaL/61Tty7Ngv5MCBf9B/R8zhtdd+IoSQ0sBNJL5j8JDUI1Uhcrg4uMtH9hAGw3q9WLUIBAw3LbDOEUAfGBhQ7pJt2npDTKCjI6YD4YSQ0sCYCSPBz/CLn1SFu/LNN4/oVwyCx48f1wVuy9z7v6HTYXFxMWDu2/dYzn2tONAR/QGAe2xk5LGC7bG/x7RvbGxMi7A9I8m+H1wADzywyxPXm7kROHVqTP9szgt9YceKfY1pgXHuD6sKglPsOeB9r712SN812vsdx8jH+PhF/Yo6nWAbUqPPnTu7cn0Mx4+/ra9xrnahHYcPH5Jdu3ZpoUTbwKuv/nhln7Wcm71/3M7NHOub39yvrVP7cdGXzr4nhHhD4EUOAwbuQiAgGOTwagZTt4ED7i0MSBjYBgb6tSsRxcTO7Bw+/BN58cUf6J+tYOu07N79kOuAawfH+u1v35XpacttirZcujSuB1SDScVFO7Ztu1Pvg3Zg289//gu9vRKgvxCrwwCLc8I5o164eZ2pxujXo0eP6FecE9qJOTL43QS1YWlhMO/vv9GXsLzsNxludeJ92Beikm+AR52//KUlWG4py6+++k9Z2+zX2N63EOyXXrLcnDgfnLP9DtZ+XXNdn7Nnz66K+eLcvv71xzOftxvnhmP/6Ec/Xmkz6sA2BOzNvqaPcdxKp2QTQtwJvMiZeNeePdYdtjXwHtKDoptlgoLtxtUFzIBmFzkMXhA4DHzY1wyAuCNHEkQ+du16UBczTwWDot0CwHbUB8vgpZd+mNWOb3/7W6ssinysNZvUJHCgTUaI8PvXvva4rn/HjnuyBlv8jv1NO1944fu6jzGYIzZmzg3H+eIXH9LnYBc5I3D2gdzEAZAwlC8VGceB1YO68B5YnPkyxsw1diYgmTZbk2JvXAvTF9hmzi/X9cF1QTuw3ZyHWUnCrR9w7Z1JPxBXez+YzwiOS5EjXmC8GAaTw4Ax03hOAG7KK5GdGTQCH5PDwGW5rawBAq+W+yk7GcG4NSEgdksJFozTOjPiCcvG/jcc326RlYKJIT377N9ltQNL5xhrqBAYTJHYkqs4gUDDkoC1axchnJ+xcNziWxAWezvNBx99YRcMHAftx0BurCPUaeKl9kEcNxrox0KxANx4QAQhLNgXIv3Zz27XguM2jcDcrDgzbNG3wJkEhOOiL+znZz4nzusDVyOARQZwg4GfnW5M41HAeTnbiL6094MRV1iIhHiB8WKYYr4TED/7dvxeDwTakrMPoHYwkGCww9/tAwouGgbAQu5G69hj+nXbtvLGSowAoF1u7TCDOWJKheJzOJfvfCf3dAlYCHbMhxZC5ASig/a4zX9xuk5NrAkuSiednatTlk3Wa3//LVkDfjod0q/Ynu9c0S64JdFvcCHjjtN8EWHBf/e7lpVp71sn6CtkZrptd4I2o06ImJulbD4bZnBwOzcR69zQn/b2uLmh1zO9pDn5qWyePyHh1OKq7cmGZvmg/c+EECf4PNq/C/i8Y6xAHNl+89vZWZmQSdAItMiZO24M2vZBxoiHtWbbjQEG1lGxLiFjSZU7NmaO6yYQwAx4xVhyaFs+cXCK3I263UUe2/GBx37lOm9jpcEVul7QPlhXKBBsMzcSdcDaK9S3hZJc7O3FK9ys+TD1wWuQaxpDpecThSSlBC57QdtwakHdRaTUDlUzC4h4iP27cPGi9TPGnmK+I7VGYEXOWjDUupN2DuYGtwG72EGnUokfhTDtq2T9hQS0nHWbY8HizJVgks9ahiXlNpnexFXhujTXuZzg+LmsZGdbcAecK3ZR+UHDs1X3CKlJAityJm6FmJmb+w1/R7wOAVaTMQhLAHfoxVgqyLwEGGSLcW8WC46Fuo3Ly4mJzZSzTnvdpg63QRnWUbnrNccLhWTNiRUm5gjBwQRwN/A3k/1ZqG+R5GGP3+ZqL46Ry+3p3BfEYp1MGiGkSgmsr8O4KuGuwwDjLCbRwO7GzDVZHELmTH4wIuBMxLDWyHQfRIvFJCW4tQPut0IDcangnExSjjPWBHcixL/c8/TM8XCz4bS20AYzP80Nk8gC8XVLMrFfN2NdQfTcEj5wDFj8Jmkkf5sfyczrc0teemOl7/L1Z6FzIyQomESpep2rGUhLzsyNy5W8ATDoYYDEfiYBxaSjI5Zz5sxZnU6OeV9IZnCC/c37kcmHfa2spCNFrwxg2mYGOxwP7quRka+uPJvJsqp26UHY7FeptTfNxGNMjUC8CT9jG0QbA3Wxk93XWifcfqgTafWmTpw3+h0/4wuWy7JGXyCjEu1FkgmuA8B1Myvd4IbGvB9ZoqgHc9e+8Y39GUtvTNdlTWJ/qmCbzfUxn5MbNyXWnEEz/cLZn3Bboj5zLfF5wbWthFVOSLnAmHTgwA+lXgmkyBnras+eR/LuhwFo7953VyYSm3R0zM8y2XkYHJEafuTIkSxLA1MNXnzx+3owNRYABi3MJSvmLh113n//g3oyMwQNc6kGBr68MvcOTzOwJy1ggDxwoLKTgjGAox/Qh/ZYJsQDH/RKxALXU6eJvcHSRMap3RLDl9M+3w+gbzGZ/okn/mplIr+9rmIEx1wf1Gm/9minsz77uWXX9zoFjpCAU7PPk8PdNgYtpMkWGtghfrAeMaiWIgKmLrckBBz7zBkrFuZ1ZhPOCfWXel5+1GmmQeTqTzvrvW7mGMVeHz/6syV5RQau/6u4JaCc79gr4fSitCUvSkN6SebCmyUR7hRSH+DRYeaGrZxg4Qis4JQrTl4O+NBUQogmn8hdaP1T6Z8/oQUOpENhudp0l0xG/1BtW1bvvSzR5DVZiPTIfHiTkNqiEk/xRujHCnWU72njbtSsyP3+9/8nqVRKCCHFkU/kFqRLmmVq1bZkqFk+avkT6V38T2lOXs1sDcls4y1yuWVI/9yS/EialidkuaFNrkduUeLIuXbVCGLCWGsX8WS42NcTH4aHAmEfE0ev5Pq6DQ0NsnXrFvEKT0UuHh+XpaVlIYQURz6RS1ydkGhvT9b2uVCftKY/zdp+qfU+aVsel87E71a2LYT71Pb7tTiS6gMCZ8WWj0g5cK7nWgmamqKyZctm8QpPRe7y5Y9ldnZOCCHFkU/krp//QNq2DuLWeNX2xOS0RLti1uRF+/4Nm6Q1dUXZcquPNdG0Xbs5SfUCsUNsudRFEyBqO3fu9CTW3NbWKps33yRe4Wl2ZXNzM0WOkDKRWl62BK2na5WgJWdnJanulsOtLav2b5z4nYRi7Vmi2KyE1BLRkJDqBG7Kasn0hSXnJZ4645ubvT05QqqftORa2iuk/i1NTkl6Obn6Ham03q7XtrSzvCSJqew7/Qa1H+WNeEVrq7eucU9FrqWlWQcdCSHrp7GnWyId7RKKhFdtDymrbll5TJbnFrLeszShRJHJX8QnGhsjWge8xHPF6eqqj8c7EFIeQpLLjdi0sU+VDVmxt0hnTIlfm4SjjVnHSiUSsjQ1LYT4gdcCBzwXue5uihwhlaSxs0OaN2+SUONqkYu0t0kDYnVRhg2IP/T2donXeC5ycFdS6AjxnobmJp2NGVZiR4jXxGLtEol4v5KkL2tX9vR0yfT0LCeGk7oEKfyhdFLCacTMkDQS1i7HtH5EakTSIVWC+4AQQtYMYnF+WHHAF5GDNYcT/uSTCSGk3mhfikt34r+lKTkpJnMSopYKRSUZapGlhjaZj2yUa42fES9IZ82cI6S8wLDxw4oDqDWuyqB4DBJQlpeXZXKyvE98JiTItC5flpsW/n1lvUkDbDhYdijR1KRemaR34T1JNNC1T6obhKfgqvSJOHwiU+ITfX09vmTbEOIXsaXzWQKXCwhfUwpfT9pZpDrBxG+M8z6iRe598REs7+L1DHhC/CKcTggh9QAMmIEB359+MQ2ROy0+gvgcFutkxiWpB2Ybb1VxN97UkdoG4zkELgCLf5w2MTnfgUkbjUZlYmKKTyogNct04+0yGxmQaHJGL76Mx940JyeUC3NRCKl2TFJhgBb9GMVTCJDXOSkBAQIHoZuZmRVCap1QOiWR9JyO1XUlzhYdrysn8+GNMt72BeECzWQ9wHpDFmXAlm7s1p9qJXQn1cuQBAgjdvPzC7TsSM2DJP625QvSt/Af0pi6Jl5CkSOlYhb3gOUWwHWJR0Oh0LCZuPBrCZjIYfLgxo19+mcI3dzcgiwuJvS0A4geJ5KTWgJz1WYjW2SppV02LJ6SluWPhVmVJEhAxMLhBh1WQlIJnioT8Oz4Y/jPWHKBclkSUtcsXhW58LbIzP9mPy6nEnR8RuSOR4WWHKkxblOWnJ5CgEdzYDLOqBBC/KepV2TLQ0p8BsUTopioS4EjNQVclXH8YHeiPi+EkGAQjYls/XORDZ+TihJSQ0DHbUJIjbGiZysip1RvVGjNERIcGhpFbvmCyKY/lopZWm1bRGJ/IITUEPGMnmmc6TC05ggJEqGwyM2fF7kJFl2ZhS7aqdyiSkQbfVtXkJBK8LT9l1XLQkP90un0qAQs05KQeiZ+/br886VWSU1tFFn4VMoCnibevVUGP7wif7lto5JPxuRITXBY6dhb9g1Zn2wlcoPq5T1V/Hn4DyFkFe9cviBDx16XVLr8UwruvXlATu7+CwmHKHKk6omrMmwSTgxZs/cyO9BtSQghpJp43ilwwHWKutrxoHp5WQghVY/6Pkt/W7vc13+r3NrBhdBJTfKy+pwfdvtDzke1qjc8pVyXKuItdwkhpGrZ3rtB/nr7DumMNsm1REL+/r/+Tc5NXhVCaoTT0Ktcfyy02NiwBOQpBYSQ0vjchpulSwkcom6xaFRZdFuEkBohrsqefDvkFbnMSigUOkKqmPHrM5LMJK3g//+ZoBVHaoK4uCSaOIlIAXAA5baE0B0Vui4JqTp+89FFpW4h+aObNsn7Vz+W31y+IIRUOXjY93DGEMtLQZEDGaW8W4kdElL2CyGkakAc7l8ufKALITUAkiK/V4zAgTVPjlFCN6JenlNlUAghhBBvgKg9n8n+L5qSZoBmJox/T5V9QgghhFSWUVUeKxR/c6OkR7miIlVGxMpqiQshhBBSfkbFir0NlyJwYF3PK8caYargOR3DwicYEEIIKQ+jckPcRmUdlHXBuowb82FVdgsXeSaEEFI8o6r8WpWDxSaVFEPFVmVVgocFnjHlYEiV7WIt+DwoTFghhJB6Ji5WEgmmAbyf+X20nMJm5/8BUzd7zk6+BNUAAAAASUVORK5CYII=" >
                        </div>
                    </div>
                </div>
            </body>
            <style>
                #icon_image{
                    width: 80px;
                    height: 80px;
                    border-radius: 10px;
                }
                .header {
                    padding: 30px;
                }
                .center{
                    text-align: center;
                }
                .container{
                    padding: 10px 5px;
                }
                .body {
                    padding: 5px 5px;
                    background-color: #9e9e9e0f;
                    border-radius: 10px;
                    font-weight: bold;
                    font-size: 16px;
                }
                .text_bold {
                    font-weight: bold;
                }
                .click_button {
                    width: 20px;
                    height: 20px;
                }.img_click {
                    width: 250px;
                }
                .content_images{
                    text-align: center;
                    padding: 10px;
                }
            </style>
            </html>
            <script type="text/javascript">
                if (window.navigator.standalone) {
                    var element = document.getElementById('redirect');
                    var event = document.createEvent('MouseEvents');
                    event.initEvent('click', true, true, document.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                    document.body.style.backgroundColor = '#FFFFFF';
                    setTimeout(function() { element.dispatchEvent(event); }, 25);
                }
            </script>
    """
    }
}
