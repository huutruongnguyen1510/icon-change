//
//  Constant.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftDate

struct Constant {
    
    enum Strings {
        static let languageCode = (NSLocale.current.languageCode ?? "en").lowercased()
        static let appName = "Font Keyboards"
        static let linkApp = "https://apps.apple.com/app/id" + appid
    }
    
    enum NotificationNames {
        static let refresh = NSNotification.Name("refresh")
    }
    
    enum Numbers {
        static var birthYearsRange: [Int] {
            let gapYear = 15
            let minDate = 1920
            let region = Region(calendar: Calendar(identifier: .gregorian),
                                zone: TimeZone.current,
                                locale: Locale(identifier: "en_US_POSIX"))
            let gregorianDate = Date().convertTo(region: region)
            return [Int](minDate...(gregorianDate.year - gapYear)).reversed()
        }
    }
    
    enum NotificationKeys {
        static let data = "data"
        static let type = "type"
        static let params = "params"
    }
}
