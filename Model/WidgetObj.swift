//
//  WidgetObj.swift
//  IconHomeScreen
//
//  Created by nguyen.viet.luy on 10/26/20.
//

import UIKit
import CoreData
//import WidgetKit

class WidgetObj: NSObject, Codable {
    var id: String = ""
    var widgetName: String = ""
    var createDate: Double = 0
    var isSelected: Bool = false
    var widgetType: WidgetType = .Medium
    var backgroundImage: ImageData?
    var backgroundImageSelected: BackgroundLauncherImageType?
    var textIconColor: ColorLauncherType = .none
    var backgroundColor: ColorLauncherType = .none
    var arrayIcon: [IconObj] = []

    override init() {
        super.init()
    }
    
    init(_ obj: Widget) {
        self.id = obj.id ?? ""
        self.widgetName = obj.widgetName ?? ""
        self.createDate = obj.createDate
        self.isSelected = obj.isSelected
        self.widgetType = WidgetType(rawValue: obj.widgetType ?? "") ?? .Medium
        if let data = obj.backgroundImage {
            if let backgroundImage = UIImage(data: data) {
                self.backgroundImage = ImageData(photo: backgroundImage)
            }
        }
        self.backgroundImageSelected = BackgroundLauncherImageType(rawValue: obj.backgroundImageSelected ?? "")
        self.textIconColor = ColorLauncherType(rawValue: obj.textIconColor ?? "") ?? .none
        self.backgroundColor = ColorLauncherType(rawValue: obj.backgroundColor ?? "") ?? .none
        do {
            let decoder = JSONDecoder()
            if let data = obj.arrayIcon {
                self.arrayIcon = try decoder.decode([IconObj].self, from: data)
            }
        } catch {
            print(error)
        }
    }
    
}

extension WidgetObj {
    func saveWidget(_ isMerge: Bool = false) {
        print("save Widget list, \(self.id), \(isMerge)")
        let minionManagedObjectContextWorker: NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        minionManagedObjectContextWorker.parent = mainContextInstance

        let widget = NSEntityDescription.insertNewObject(forEntityName: "Widget",
                                                       into: minionManagedObjectContextWorker) as! Widget
        widget.id = self.id
        widget.widgetName = self.widgetName
        widget.createDate = self.createDate
        widget.isSelected = self.isSelected
        widget.widgetType = self.widgetType.rawValue
        widget.backgroundImage = self.backgroundImage?.photo
        widget.backgroundImageSelected = self.backgroundImageSelected?.rawValue
        widget.textIconColor = self.textIconColor.rawValue
        widget.backgroundColor = self.backgroundColor.rawValue
        
        do {
            let encoder = JSONEncoder()
            widget.arrayIcon = try encoder.encode(self.arrayIcon)
        } catch let error {
            print(error)
        }

        persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        if isMerge {
            persistenceManager.mergeWithMainContext()
        }
    }

    func updateWidget() {
        if let widget = findWidget() {
            widget.id = self.id
            widget.widgetName = self.widgetName
            widget.createDate = self.createDate
            widget.isSelected = self.isSelected
            widget.widgetType = self.widgetType.rawValue
            widget.backgroundImageSelected = self.backgroundImageSelected?.rawValue
            widget.backgroundImage = self.backgroundImage?.photo
            widget.textIconColor = self.textIconColor.rawValue
            widget.backgroundColor = self.backgroundColor.rawValue
            
            do {
                let encoder = JSONEncoder()
                widget.arrayIcon = try encoder.encode(self.arrayIcon)
            } catch let error {
                print(error)
            }
            
            persistenceManager.mergeWithMainContext()
        }
    }

    func deleteWidget() {
        if let widget = findWidget() {
            mainContextInstance.delete(widget)
            persistenceManager.mergeWithMainContext()
        }
    }

    func findWidget() -> Widget? {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Widget")
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format:"%K == %@", "id", self.id as CVarArg)

        var fetchedResults: Array<Widget> = Array<Widget>()

        do {
            fetchedResults = try  mainContextInstance.fetch(fetchRequest) as! [Widget]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Widget>()
        }
        if fetchedResults.count == 1 {
            return fetchedResults[0]
        }
        return nil
    }
}
