//
//  TutorialModel.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/19/21.
//

import Foundation
import UIKit

struct TutorialModel {
    var image: UIImage!
    var content: String
}

class WidgetTutorialData {
    static let shared: WidgetTutorialData = WidgetTutorialData()
    var menu = [TutorialModel(image: #imageLiteral(resourceName: "widget_1"), content: "Long press the home screen to edit, then press the + button at the top left."),
                TutorialModel(image: #imageLiteral(resourceName: "widget_2"), content: "Scroll down and select Memo Widget."),
                TutorialModel(image: #imageLiteral(resourceName: "widget_3"), content: "Choose a widget with a desired shape and add it to the home screen."),
                TutorialModel(image: #imageLiteral(resourceName: "widget_4"), content: "if you long press the added widget, you can edit, delete or move it.")]
}

class IconTutorialData {
    static let shared: IconTutorialData = IconTutorialData()
    var menu = [TutorialModel(image: #imageLiteral(resourceName: "Icon_1"), content: "Step 1: Open Shortcuts"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_2"), content: "Step 2: Tap the plus button in the right top corner to create a new shortcut"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_3"), content: "Step 3: On the New Shortcut Screen, tap on " + String("New Action")),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_4"), content: "Step 4: Tap on " + String("Scripting")),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_5"), content: "Step 5: Tap on " + String("Open App")),
                TutorialModel(image: #imageLiteral(resourceName: "Icon-6"), content: "Step 6: Tap on " + String("Choose")),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_7"), content: "Step 7: Select the app you want to customize"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_8"), content: "Step 8: We choose the Camera app"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_9"), content: "Step 9: Tap on the Three Dots on the top right corner"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_10"), content: "Step 10: Tap on Add to Home Screen"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_11"), content: "Step 11: Set the name that will be displayed on the home screen"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_12"), content: "Step 12: Tap on the icon to change it"),
                TutorialModel(image: #imageLiteral(resourceName: "icon_13"), content: "Step 13: Set the name that will be displayed on the home screen"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_14"), content: "Step 14: Choose from your Photo Library the icon you want to set"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_15"), content: "Step 15: Tap Choose button to go to the next step"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_16"), content: "Step 16: Finally, tap on the Add button in the top right corner"),
                TutorialModel(image: #imageLiteral(resourceName: "Icon_17"), content: "Step 17: Done! Now you should fine the newly added icon on your home screen.")]
}
