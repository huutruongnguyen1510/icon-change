//
//  IconManager.swift
//  IconMarker
//
//  Created by Luy Nguyen on 10/14/20.
//  Copyright © 2019 Luy Nguyen. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import UserNotifications

final class IconManager {
    static let sharedInstance = IconManager()
    var arrSearch: [IconObj] = []
    
    init() {
    }
    
    func getAllIcon() -> [IconObj] {
        var fetchedResults: Array<Icon> = Array<Icon>()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Icon")
        do {
            fetchedResults = try mainContextInstance.fetch(fetchRequest) as? [Icon] ?? []
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Icon>()
        }
        var result: [IconObj] = []
        for fetch in fetchedResults {
            let obj: IconObj = IconObj(fetch)
            result.append(obj)
        }
        return result.sorted(by: { (obj1, obj2) -> Bool in
            obj1.createDate < obj2.createDate
        })
    }
}

let iconManager = IconManager.sharedInstance

