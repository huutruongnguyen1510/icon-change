//
//  RelateApp.swift
//  IconMarker
//
//  Created by nguyen.viet.luy on 10/16/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import Foundation

struct RelateAppModel: Codable {
    var appName: String
    var scheme: String
    
    init(appName: String, scheme: String) {
        self.appName = appName
        self.scheme = scheme
    }
}
