//
//  LauncherWidget.swift
//  LauncherWidget
//
//  Created by Luy Nguyen on 10/14/20.
//  Copyright © 2019 Luy Nguyen. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import UserNotifications

final class WidgetManager {
    static let sharedInstance = WidgetManager()
    var arrSearch: [WidgetObj] = []

    init() {
    }

    func getAllWidget() -> [WidgetObj] {
        var fetchedResults: Array<Widget> = Array<Widget>()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Widget")
        do {
            fetchedResults = try mainContextInstance.fetch(fetchRequest) as? [Widget] ?? []
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Widget>()
        }
        var result: [WidgetObj] = []
        for fetch in fetchedResults {
            let obj: WidgetObj = WidgetObj(fetch)
            result.append(obj)
        }
        return result.sorted(by: { (obj1, obj2) -> Bool in
            obj1.createDate < obj2.createDate
        })
    }
}

let widgetManager = WidgetManager.sharedInstance

