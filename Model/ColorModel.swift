//
//  ColorModel.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/2/21.
//

import Foundation
import RealmSwift

class ColorModel: Object {
    @objc dynamic var id = 0
    @objc dynamic var color: String = ""
    override class func primaryKey() -> String? {
        return "id"
    }

    func IncrementaID() -> Int{
        let realm = try! Realm()
        if let retNext = realm.objects(ColorModel.self).sorted(byKeyPath: "id").last?.id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
