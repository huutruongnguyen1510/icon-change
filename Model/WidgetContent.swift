//
//  WidgetContent.swift
//  IconHomeScreen
//
//  Created by nguyen.viet.luy on 10/28/20.
//

import Foundation
import UIKit
import WidgetKit

struct WidgetContent: Codable, TimelineEntry {
    var date = Date()
    var backgroundImage: Data? = nil
    var textIconColor: ColorLauncherType = .none
    var backgroundColor: ColorLauncherType = .none
    var arrayIconObj: [IconInWidgetModel]
    var size: CGSize = CGSize()
}

struct IconInWidgetModel: Codable {
    var appIcon: Data
    var appName: String
    var appRelateScheme: String
    var launcherType: LauncherType
}

// MARK: - Extension
extension FileManager {
  static func sharedContainerURL() -> URL {
    return FileManager.default.containerURL(
      forSecurityApplicationGroupIdentifier: "group.com.iconchanger.themer")!
  }
}
