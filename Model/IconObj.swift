//
//  IconObj.swift
//  IconMarker
//
//  Created by Luy Nguyen on 10/14/20.
//  Copyright © 2019 Luy Nguyen. All rights reserved.
//

import UIKit
import CoreData

public struct ImageData: Codable {

    public let photo: Data

    public init(photo: UIImage) {
        self.photo = photo.pngData() ?? Data()
    }
}

class IconObj: NSObject, Codable {
    var id: String = ""
    var appIcon: ImageData = ImageData(photo: UIImage())
    var appName: String = ""
    var appRelateName: String = ""
    var appRelateScheme: String = ""
    var color: String = ""
    var createDate: Double = 0
    var createIconType: CreateIconType = .textIcon
    var fontSize: CGFloat = 0
    var fontType: FontType = .Nunito
    var iconBorder: BorderType = .none
    var imageShape: ImageShapeType = .none
    var imageShapeTintColor: ColorLauncherType = .none
    var textColor: ColorType = .black
    var textIcon: String = ""
    var photoIcon: ImageData = ImageData(photo: UIImage())
    var launcherType: LauncherType = .App

    override init() {
        super.init()
    }
    
    init(_ obj: Icon) {
        self.id = obj.id ?? ""
        if let data = obj.appIcon {
            if let appIcon = UIImage(data: data) {
                self.appIcon = ImageData(photo: appIcon)
            }
        }
        
        self.appName = obj.appName ?? ""
        self.appRelateName = obj.appRelateName ?? ""
        self.appRelateScheme = obj.appRelateScheme ?? ""
        self.color = obj.color ?? ""
        self.createDate = obj.createDate
        self.createIconType = CreateIconType(rawValue: obj.createIconType ?? "") ?? .textIcon
        self.fontSize = CGFloat(obj.fontSize)
        self.fontType = FontType(rawValue: obj.fontType ?? "") ?? .Nunito
        self.iconBorder = BorderType(rawValue: obj.iconBorder ?? "") ?? .none
        self.imageShape = ImageShapeType(rawValue: obj.imageShape ?? "") ?? .none
//        self.imageShapeTintColor = ColorType(rawValue: obj.imageShapeTintColor ?? "") ?? .none
        self.textColor = ColorType(rawValue: obj.textColor ?? "") ?? .black
        self.textIcon = obj.textIcon ?? ""
        if let data = obj.photoIcon {
            if let photoIcon = UIImage(data: data) {
                self.photoIcon = ImageData(photo: photoIcon)
            }
        }
    }
}

extension IconObj {
    func saveIcon(_ isMerge: Bool = false) {
        print("save Icon list, \(self.id), \(isMerge)")
        let minionManagedObjectContextWorker: NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        minionManagedObjectContextWorker.parent = mainContextInstance
        
        let icon = NSEntityDescription.insertNewObject(forEntityName: "Icon",
                                                       into: minionManagedObjectContextWorker) as! Icon
        icon.id = self.id
        icon.appIcon = self.appIcon.photo
        icon.appName = self.appName
        icon.appRelateName = self.appRelateName
        icon.appRelateScheme = self.appRelateScheme
        icon.color = self.color
        icon.createDate = self.createDate
        icon.createIconType = self.createIconType.rawValue
        icon.fontSize = Double(self.fontSize)
        icon.fontType = self.fontType.rawValue
        icon.iconBorder = self.iconBorder.rawValue
        icon.imageShape = self.imageShape.rawValue
        icon.imageShapeTintColor = self.imageShapeTintColor.rawValue
        icon.textColor = self.textColor.rawValue
        icon.textIcon = self.textIcon
        icon.photoIcon = self.photoIcon.photo
        
        persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        if isMerge {
            persistenceManager.mergeWithMainContext()
        }
    }
    
    func updateIcon() {
        if let icon = findIcon() {
            icon.id = self.id
            icon.appIcon = self.appIcon.photo
            icon.appName = self.appName
            icon.appRelateName = self.appRelateName
            icon.appRelateScheme = self.appRelateScheme
            icon.createDate = self.createDate
            icon.createIconType = self.createIconType.rawValue
            icon.fontSize = Double(self.fontSize)
            icon.fontType = self.fontType.rawValue
            icon.iconBorder = self.iconBorder.rawValue
            icon.imageShape = self.imageShape.rawValue
            icon.imageShapeTintColor = self.imageShapeTintColor.rawValue
            icon.textColor = self.textColor.rawValue
            icon.textIcon = self.textIcon
            icon.photoIcon = self.photoIcon.photo
            persistenceManager.mergeWithMainContext()
        }
    }
    
    func deleteIcon() {
        if let icon = findIcon() {
            mainContextInstance.delete(icon)
            persistenceManager.mergeWithMainContext()
        }
    }
    
    func findIcon() -> Icon? {
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Icon")
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format:"%K == %@", "id", self.id as CVarArg)
        
        var fetchedResults: Array<Icon> = Array<Icon>()
        
        do {
            fetchedResults = try  mainContextInstance.fetch(fetchRequest) as! [Icon]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Icon>()
        }
        if fetchedResults.count == 1 {
            return fetchedResults[0]
        }
        return nil
    }
}
