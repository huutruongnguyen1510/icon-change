//
//  ThemeSplashCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/4/21.
//

import UIKit
import FSPagerView

class ThemeSplashCell: FSPagerViewCell {
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

}

extension ThemeSplashCell {
    func bind(type: ThemeType, statusView: ThemeViewController.StatusView) {
        imgView.image = statusView == .installIcon ? type.imageTheme : type.wallpaper
    }
}
