//
//  WidgetCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/14/21.
//

import UIKit

class WidgetCLVCell: UICollectionViewCell {
    @IBOutlet weak var trashImgView: UIImageView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var chooseImgView: UIImageView!
    var handleUseIt: (() -> Void)?
    override func awakeFromNib() {
        blackView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(0.5)
//        chooseButton.rx.tap
//            .subscribe(onNext: { [unowned self] in
//                self.handleUseIt?()
//            })
//            .disposed(by: rx.disposeBag)
        super.awakeFromNib()
        // Initialization code
    }

}
