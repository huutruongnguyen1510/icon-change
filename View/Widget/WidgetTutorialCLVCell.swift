//
//  WidgetTutorialCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/19/21.
//

import UIKit
import FSPagerView

class WidgetTutorialCLVCell: FSPagerViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var content: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
