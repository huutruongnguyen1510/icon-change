//
//  FontCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/1/21.
//

import UIKit

class FontCLVCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var chooseFont = -1
    var listFont = [UIFont(name: "ArchitectsDaughter-Regular.ttf", size: 18), UIFont(name: "AvertaStdCY-Regular", size: 18), UIFont(name: "BebasNeue-Regular", size: 18), UIFont(name: "BebasNeue-Regular", size: 18), UIFont(name: "HLT Aparo", size: 18), UIFont(name: "HLT Fitigraf_s", size: 18), UIFont(name: "iCiel Koni Black", size: 18), UIFont(name: "Jua-Regular", size: 18), UIFont(name: "KERO KERO", size: 18), UIFont(name: "Nunito-SemiBold", size: 18), UIFont(name: "Pacifico-Regular", size: 18), UIFont(name: "Roboto-Regular", size: 18), UIFont(name: "segoeprb", size: 18), UIFont(name: "SVN-Adam Gorry", size: 18), UIFont(name: "UTM Flamenco", size: 18), UIFont(name: "UVNAnhHai_B", size: 18), UIFont(name: "Poppins-SemiBoldItalic", size: 18)]
    private let fonts: [FontType] = FontType.allCases
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "FontIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "FontIconCLVCell")
        collectionView.backgroundColor = .clear
        // Initialization code
    }
}

extension FontCLVCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        chooseFont = indexPath.item
        collectionView.reloadData()
        NotificationCenter.default.post(name: Notification.Name("LOAD_FONT_TYPE"), object: fonts[indexPath.item].fontName)
    }
}

extension FontCLVCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fonts.count
//        return listFont.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FontIconCLVCell", for: indexPath) as! FontIconCLVCell
        cell.backgroundColor = .clear
        cell.layer.borderWidth = 0
        if chooseFont == -1 {
            
        }
        else {
            if chooseFont == indexPath.item {
                cell.layer.borderWidth = 4
                cell.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            }
        }
        cell.lbFont.font = UIFont(name: fonts[indexPath.item].fontName, size: 16)
        //cell.lbFont.font = listFont[indexPath.item]
        return cell
    }
}

extension FontCLVCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
