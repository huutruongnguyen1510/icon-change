//
//  SizeIconCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/1/21.
//

import UIKit

class SizeIconCLVCell: UICollectionViewCell {
    @IBOutlet weak var sizeControl: UISlider!
    @IBAction func adjustVolume(sender: AnyObject) {
        NotificationCenter.default.post(name: Notification.Name("LOAD_FONT_SIZE"), object: sizeControl.value)
    }
    override func awakeFromNib() {
        sizeControl.maximumValue = 50.0
        sizeControl.minimumValue = 0
        sizeControl.value = 40.0
        super.awakeFromNib()
        // Initialization code
    }
    
}
