//
//  ExpandBgCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/4/21.
//

import UIKit

class ExpandBgCLVCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    var chooseBackground = -1
    var listBackground: [UIImage] = [UIImage]()
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "BackgroundIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "BackgroundIconCLVCell")
        collectionView.backgroundColor = .clear
        // Initialization code
    }

}

extension ExpandBgCLVCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        chooseBackground = indexPath.item
        collectionView.reloadData()
        NotificationCenter.default.post(name: Notification.Name("LOAD_BACKGROUND"), object: listBackground[indexPath.item])
//        if collectionView == collectionViewToolbar {
//            checkTab = indexPath.item
//            collectionView.reloadData()
//        }
//        else {
//
//        }
    }
}

extension ExpandBgCLVCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listBackground.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BackgroundIconCLVCell", for: indexPath) as! BackgroundIconCLVCell
        cell.backgroundColor = .clear
        cell.layer.borderWidth = 0
        if chooseBackground == -1 {
            
        }
        else {
            if chooseBackground == indexPath.item {
                cell.layer.borderWidth = 4
                cell.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            }
        }
        cell.imgView.image = listBackground[indexPath.item].scaledToSize(CGSize(width: 35, height: 35))
        return cell
    }
}

extension ExpandBgCLVCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
