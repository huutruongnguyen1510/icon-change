//
//  BorderStyleCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/2/21.
//

import UIKit

class BorderStyleCLVCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var chooseBorder = -1
    var listBorder = BorderType.allCases
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "BorderIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "BorderIconCLVCell")
        collectionView.backgroundColor = .clear
        // Initialization code
    }

}

extension BorderStyleCLVCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        chooseBorder = indexPath.item
        collectionView.reloadData()
        NotificationCenter.default.post(name: Notification.Name("LOAD_BORDER"), object: indexPath.item + 1)
    }
}

extension BorderStyleCLVCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listBorder.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BorderIconCLVCell", for: indexPath) as! BorderIconCLVCell
        cell.backgroundColor = .clear
        cell.layer.borderWidth = 0
        if chooseBorder == -1 {
            
        }
        else {
            if chooseBorder == indexPath.item {
                cell.layer.borderWidth = 4
                cell.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            }
        }
        cell.imgView.image = listBorder[indexPath.item + 1].image
        return cell
    }
}

extension BorderStyleCLVCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
