//
//  MyIconCell.swift
//  IconMarker
//
//  Created by nguyen.viet.luy on 10/10/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

class MyIconCell: UICollectionViewCell {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var createIconButton: UIButton!
    @IBOutlet private weak var myIconContainerView: UIView!
    @IBOutlet private weak var myIconImageView: UIImageView!
    @IBOutlet private weak var editView: UIView!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var titleLabel: UILabel!
    
    enum StatusCell {
        case create
        case myIcon
    }
    var iconObj = IconObj()
    
    var handleCreate: (() -> ())?
    var handleMyIcon: ((IconObj) -> ())?
    var handleEdit: ((IconObj) -> ())?
    var handleDelete: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        [containerView, myIconContainerView].forEach {
            $0.cornerRadius = 19*heightRatio
        }
        
        createIconButton.do {
            $0.backgroundColor = .wePeep
            $0.setTitleColor(.lightCoral, for: .normal)
        }
        
        myIconContainerView.border(color: UIColor.hex("C9C9C9"), width: 0.5*heightRatio)
        
        createIconButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.handleCreate?()
            })
            .disposed(by: rx.disposeBag)
        
        myIconImageView.rx.tapGesture()
            .mapToVoid()
            .subscribe(onNext: { [unowned self] in
                self.handleMyIcon?(self.iconObj)
            })
            .disposed(by: rx.disposeBag)
        
        editButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.handleEdit?(self.iconObj)
            })
            .disposed(by: rx.disposeBag)
        
        deleteButton.rx.tap
            .subscribe(onNext: { [unowned self] in
                self.handleDelete?()
            })
            .disposed(by: rx.disposeBag)
    }

    func bind(iconObj: IconObj?, statusView: HomeViewController.StatusViewMyIcon, statusCell: StatusCell) {
        createIconButton.do {
            $0.backgroundColor = statusView == .done ? .hex("DEDEDE") : .wePeep
            $0.setTitle(statusView == .done ? "" : "+", for: .normal)
            $0.setImage(statusView == .done ? #imageLiteral(resourceName: "top_none") : nil, for: .normal)
            $0.tintColor = .hex("C7C7C7")
            $0.isUserInteractionEnabled = statusView != .done
            $0.isHidden = statusCell != .create
        }
        myIconContainerView.isHidden = statusCell != .myIcon
        editView.isHidden = statusView != .done || statusCell == .create
        deleteButton.isHidden = statusView != .done || statusCell == .create
        
        titleLabel.text = statusCell == .create ? "Create Icon" : iconObj?.appName
        
        if let iconObj = iconObj {
            self.iconObj = iconObj
            myIconImageView.image = UIImage(data: iconObj.appIcon.photo)
        }
    }
}
