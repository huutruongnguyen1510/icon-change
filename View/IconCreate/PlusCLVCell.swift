//
//  PlusCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/13/21.
//

import UIKit

class PlusCLVCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIView!
    override func awakeFromNib() {
        imgView.layer.cornerRadius = 10
        super.awakeFromNib()
        // Initialization code
    }

}
