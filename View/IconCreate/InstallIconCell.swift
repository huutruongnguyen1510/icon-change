//
//  InstallIconCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/8/21.
//

import UIKit

class InstallIconCell: BaseTableCell {
    @IBOutlet private weak var shadowContainerView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var shadowIconThemeView: UIView!
    @IBOutlet private weak var iconThemeImageView: UIImageView!
    @IBOutlet private weak var containerAppRelateView: UIView!
    @IBOutlet private weak var appRelateImageView: UIImageView!
    @IBOutlet private weak var plusLabel: UILabel!
    @IBOutlet private weak var installButton: UIButton!
    @IBOutlet private weak var installLabel: UILabel!
    @IBOutlet private weak var installImageView: UIImageView!
    
    var handleSelectAppRelate: (() -> ())?
    var handleInstall: (() -> ())?
    private var appRelated: RelateAppModel?
    
    override func setupUI() {
//        shadowContainerView.shadowView(color: .black, alpha: 0.05, x: 0, y: 5, blur: 5)
//        containerView.cornerRadius = 30*heightRatio
//
//        shadowIconThemeView.shadowView(alpha: 0.15, x: 2, y: 2, blur: 4)
//
//        [iconThemeImageView, containerAppRelateView].forEach {
//            $0?.cornerRadius = 19*heightRatio
//        }
//
//        containerAppRelateView.border(color: UIColor.hex("C9C9C9"), width: 0.5*heightRatio)
//
        appRelateImageView.rx.tapGesture()
            .subscribe(onNext: { [unowned self] _ in
                self.handleSelectAppRelate?()
            })
            .disposed(by: rx.disposeBag)
        
        installButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                if self.appRelated != nil {
                    self.handleInstall?()
                }
            })
            .disposed(by: rx.disposeBag)
    }
}

extension InstallIconCell {
    func bind(iconTheme: UIImage, appRelated: RelateAppModel?) {
        self.appRelated = appRelated
        iconThemeImageView.image = iconTheme
        if let appName = appRelated?.appName {
            appRelateImageView.image = UIImage().getUncachedImage(named: appName)
        } else {
            appRelateImageView.image = nil
        }
        
        plusLabel.isHidden = !appRelated.isNil
        installButton.alpha = appRelated.isNil ? 0.3 : 1
        installImageView.alpha = appRelated.isNil ? 0.3 : 1
        installLabel.alpha = appRelated.isNil ? 0.3 : 1
    }
}

