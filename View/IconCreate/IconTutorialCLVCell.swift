//
//  IconTutorialCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/19/21.
//

import UIKit
import CHIPageControl
import FSPagerView

class IconTutorialCLVCell: FSPagerViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var pageControl: CHIPageControlAji!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
