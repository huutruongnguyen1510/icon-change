//
//  MyIconCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 9/30/21.
//

import UIKit

class MyIconCLVCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var editImgView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    var handleEdit: ((IconObj) -> ())?
    var handleDelete: (() -> ())?
    var iconObj = IconObj()
    override func awakeFromNib() {
        editImgView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(0.5)
        super.awakeFromNib()
        // Initialization code
    }

}
