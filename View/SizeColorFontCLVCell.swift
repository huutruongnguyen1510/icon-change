//
//  SizeColorFontCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/1/21.
//

import UIKit
import RealmSwift

class SizeColorFontCLVCell: UICollectionViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    //var listColor: [UIColor] = [#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.5617598891, green: 0.3415982723, blue: 0.9792051911, alpha: 1), #colorLiteral(red: 0.982455194, green: 0.3872094154, blue: 0.388961643, alpha: 1), #colorLiteral(red: 0.2469733953, green: 0.880644381, blue: 0.5760262609, alpha: 1), #colorLiteral(red: 0.9307311177, green: 0.5871212482, blue: 0.2213520706, alpha: 1), #colorLiteral(red: 0.3022302389, green: 0.8893552423, blue: 0.8264402747, alpha: 1), #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)]
    var isWidget = false
    var listColor: [UIColor] = [UIColor]()
    var colors: [ColorType] = ColorType.allCases
    var listSavedColor: [UIColor] = [UIColor]()
    var listAllColor: [UIColor] = [UIColor]()
    let realm = try! Realm()
    var chooseColor = -1
    override func awakeFromNib() {
        super.awakeFromNib()
        let results = realm.objects(ColorModel.self)
        for item in results {
            listSavedColor.append(UIColor.hex(item.color))
            listSavedColor.reverse()
        }
        collectionView.register(UINib(nibName: "ColorIconCLVCell", bundle: nil), forCellWithReuseIdentifier: "ColorIconCLVCell")
        collectionView.backgroundColor = .clear
        // Initialization code
    }
}

extension SizeColorFontCLVCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        chooseColor = indexPath.item
        collectionView.reloadData()
        if indexPath.item == listAllColor.count {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ColorViewController") as! ColorViewController
            if let parent = self.parentViewController as? IconViewController {
                parent.present(vc, animated: true)
                vc.didSendDataColor = { [weak self] pickedColor in
                        guard let self = self else { return }
                    self.listSavedColor.insert(pickedColor, at: 0)
                    self.collectionView.reloadData()
                }
            }
            if let parent = self.parentViewController as? IconWidgetViewController {
                parent.present(vc, animated: true)
                vc.didSendDataColor = { [weak self] pickedColor in
                        guard let self = self else { return }
                    self.listSavedColor.insert(pickedColor, at: 0)
                    self.collectionView.reloadData()
                }
            }
            if let parent = self.parentViewController as? CreateWidgetViewController {
                parent.present(vc, animated: true)
                vc.didSendDataColor = { [weak self] pickedColor in
                        guard let self = self else { return }
                    self.listSavedColor.insert(pickedColor, at: 0)
                    self.collectionView.reloadData()
                }
            }
        }
        else {
            NotificationCenter.default.post(name: Notification.Name("LOAD_FONT_COLOR"), object: listAllColor[indexPath.item])
        }
//        if collectionView == collectionViewToolbar {
//            checkTab = indexPath.item
//            collectionView.reloadData()
//        }
//        else {
//
//        }
    }
}

extension SizeColorFontCLVCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        listColor.removeAll()
        for item in colors {
            listColor.append(item.color)
        }
        listAllColor = listSavedColor + listColor
        return listAllColor.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorIconCLVCell", for: indexPath) as! ColorIconCLVCell
        if isWidget {
            cell.circle()
        }
        cell.layer.cornerRadius = 10
        cell.backgroundColor = .clear
        cell.layer.borderWidth = 0
        cell.imgView.isHidden = true
        if chooseColor == -1 {
            
        }
        else {
            if chooseColor == indexPath.item {
                cell.layer.borderWidth = 4
                cell.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            }
        }
        if indexPath.item == listAllColor.count {
            cell.imgView.isHidden = false
            cell.imgView.image = #imageLiteral(resourceName: "ion_color-filter").scaledToSize(CGSize(width: 50, height: 50))
            cell.contentView.backgroundColor = .clear
            return cell
        }
        cell.contentView.backgroundColor = listAllColor[indexPath.item]
        return cell
    }
}

extension SizeColorFontCLVCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
