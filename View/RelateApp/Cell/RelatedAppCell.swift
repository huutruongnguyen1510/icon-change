//
//  RelatedAppCell.swift
//  IconMarker
//
//  Created by nguyen.viet.luy on 10/12/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

class RelatedAppCell: BaseTableCell {
    @IBOutlet private weak var iconContainerView: UIView!
    @IBOutlet private weak var iconAppImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconAppImageView.image = nil
    }
    
    override func setupUI() {
        isEnableShrink = true
        iconContainerView.cornerRadius = 10*heightRatio
        iconContainerView.border(color: UIColor.hex("C9C9C9"), width: 0.5*heightRatio)
        backgroundColor = .hex(isIPad ? "FEFEFE" : "F5F5F5")
    }
    
    func bind(type: RelateAppModel) {
        iconAppImageView.image = UIImage().getUncachedImage(named: type.appName)
        
        titleLabel.text = type.appName
    }
}
