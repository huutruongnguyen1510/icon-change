//
//  RelatedAppHeader.swift
//  IconMarker
//
//  Created by nguyen.viet.luy on 10/12/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

class RelatedAppHeader: UITableViewHeaderFooterView {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = UIView(frame: bounds)
    }

}
