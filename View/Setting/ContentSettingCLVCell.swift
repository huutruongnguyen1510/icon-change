//
//  ContentSettingCLVCell.swift
//  appiconchanger
//
//  Created by Huu Truong Nguyen on 10/19/21.
//

import UIKit

class ContentSettingCLVCell: UICollectionViewCell {
    @IBOutlet weak var content: UITextView!
    override func awakeFromNib() {
        content.isEditable = false
        super.awakeFromNib()
        // Initialization code
    }

}
