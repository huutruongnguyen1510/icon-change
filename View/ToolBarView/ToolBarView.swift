//
//  BaseShadowView.swift
//  IconMarker
//
//  Created by nguyen.viet.luy on 10/9/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import Foundation
import UIKit

final class ToolBarView: BaseView {
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var button1: UIButton!
    @IBOutlet private weak var button2: UIButton!
    
    var handleButton1: ((CreateIconType) -> ())?
    var handleButton2: ((CreateIconType) -> ())?
    
    var createIconType: CreateIconType = .textIcon {
        didSet {
            switch createIconType {
            case .textIcon:
                button1.do {
                    $0.backgroundColor = .lightCoral
                    $0.setTitleColor(.white, for: .normal)
                }
                button2.do {
                    $0.backgroundColor = .white
                    $0.setTitleColor(.nero, for: .normal)
                }
            case .photoIcon:
                button1.do {
                    $0.backgroundColor = .white
                    $0.setTitleColor(.nero, for: .normal)
                }
                button2.do {
                    $0.backgroundColor = .lightCoral
                    $0.setTitleColor(.white, for: .normal)
                }
            }
        }
    }
    
    @IBInspectable var title1: String = "" {
        didSet {
            button1.setTitle(title1, for: .normal)
        }
    }
    
    @IBInspectable var title2: String = "" {
        didSet {
            button2.setTitle(title2, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        containerView.circle()
    }
    
    private func configureView() {
        containerView.border(color: .lightCoral, width: 1*heightRatio)
        button1.titleLabel?.font = .extraBold(size: 12)
        button2.titleLabel?.font = .extraBold(size: 12)
    }
    
    @IBAction func actionButton1(_ sender: Any) {
        handleButton1?(.textIcon)
    }
    
    @IBAction func actionButton2(_ sender: Any) {
        handleButton2?(.photoIcon)
    }
}
