//
//  UIImageView.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

//import UIKit
//import RxCocoa
//import RxSwift
//import Kingfisher
//
//extension UIImageView {
//
//    func loadImage(placeholderImage: UIImage?,
//                   anotherUrl: String?,
//                   withBackgroundImageColor color: UIColor = .lightGray) {
//        guard let urlString = anotherUrl, let url = URL(string: urlString) else {
//            return
//        }
//        kf.setImage(
//            with: url,
//            placeholder: placeholderImage,
//            options: [
//                .processor(DownsamplingImageProcessor(size: self.bounds.size)),
//                .scaleFactor(UIScreen.main.scale),
//                .transition(.fade(0.3)),
//                .cacheOriginalImage
//        ]) { result in
//            switch result {
//            case .success:
//                break
//            case .failure:
//                break
//            }
//        }
//    }
//}
