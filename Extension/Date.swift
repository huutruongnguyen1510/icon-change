//
//  Date.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import Foundation
import SwiftDate

let calendar = Calendar(identifier: .gregorian)

extension Date {
    func toGMT(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

    func convertToTimeZone(from fromTimeZone: TimeZone, to toTimeZone: TimeZone) -> Date {
         let delta = TimeInterval(toTimeZone.secondsFromGMT(for: self) - fromTimeZone.secondsFromGMT(for: self))
         return addingTimeInterval(delta)
    }

    func compareDate(_ date: Date) -> Bool {
        return day == date.day && month == date.month && year == date.year
    }
}

//extension Date {
//    func toString(_ format: String) -> String {
//        let formatter = DateFormatter().then {
//            $0.dateFormat = format
//            $0.calendar = Calendar(identifier: .gregorian)
////            $0.locale = Locale(identifier: "ja_JP")
//            $0.timeZone = TimeZone(secondsFromGMT: TimeZone.current.secondsFromGMT())
//        }
//        return formatter.string(from: self)
//    }
//}

extension Date {
    func getComponent(_ component: Calendar.Component) -> Int {
        let calendar = Calendar.current
        return calendar.component(component, from: self)
    }
}

extension Date {
    func hasSame(_ component: Calendar.Component, as date: Date) -> Bool {
        return self.compare(toDate: date, granularity: component).rawValue == 0
    }
}

extension Date {
    static func dates(from fromDate: Date, to toDate: Date, component: Calendar.Component = .day) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}

extension Date {
    func isBetween(date date1: Date, andDate date2: Date) -> Bool {
        return date1.compare(self).rawValue * self.compare(date2).rawValue >= 0
    }
    
    var startOfMonth: Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)
        return calendar.date(from: components) ?? Date()
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth) ?? Date()
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay) ?? Date()
    }
}
