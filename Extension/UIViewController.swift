//
//  UIViewController.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

typealias ActionHandler = (_ action: UIAlertAction) -> ()
typealias AttributedActionTitle = (title: String, style: UIAlertAction.Style)

extension UIViewController {
    func topMostViewController() -> UIViewController? {
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController()
        }
        
        if let tab = self as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            if let visibleController = navigation.visibleViewController {
                return visibleController.topMostViewController()
            }
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController?.topMostViewController()
    }
    
    private func forcePresentAlertVC(_ alertVC: UIAlertController) {
        if let presentedViewController = presentedViewController {
            if !presentedViewController.isKind(of: UIAlertController.self) {
                presentedViewController.present(alertVC, animated: true, completion: nil)
            }
        } else {
            present(alertVC, animated: true, completion: nil)
        }
    }
    
    func showAlert(title: String?, message: String?, actionTitle: String,  _ action: (() -> Void)? = nil) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle.localized, style: .default) { _ in
            action?()
        }
        alertVC.addAction(action)
        forcePresentAlertVC(alertVC)
    }
    
    func present(title: String?, message: String?,  actionTitles: [String]?, handler: ActionHandler? = nil) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let actionsTitles = actionTitles {
            for actionsTitle in actionsTitles {
                let buttonAction = UIAlertAction(title: actionsTitle, style: .default) { (action) in
                    handler?(action)
                }
                alertVC.addAction(buttonAction)
            }
        }
        
        forcePresentAlertVC(alertVC)
    }
    
    func showMessage(_ message: String?) {
        let alertView = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        forcePresentAlertVC(alertView)
    }
    
//    func showAlertViewController(_ alertViewController: BaseViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
//        present(alertViewController, animated: animated, completion: completion)
//    }
    
    func showErrorAlert(_ error: Error?) {
        if let _ = error {
            return
        }
        showErrorLocalizedAlert(error)
    }
    
    func showErrorLocalizedAlert(_ error: Error?, action: (() -> Void)? = nil) {
        guard let message = error?.localizedDescription, !message.isEmpty else {
            action?()
            return
        }
        showErrorMessageAlert(message, action: action)
    }
    
    func showErrorMessageAlert(_ message: String? = nil, action: (() -> Void)? = nil) {
        showAlert(title: "", message: message, actionTitle: "Close")
    }
    
    func add(_ childController: UIViewController) {
        childController.willMove(toParent: self)
        addChild(childController)
        view.addSubview(childController.view)
        childController.didMove(toParent: self)
    }
    
    func removeBackButtonTitle() {
        if let topItem = navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension Reactive where Base: UIViewController {
    var isError: Binder<Error> {
        return Binder(base) { vc, error in
          vc.showErrorAlert(error)
        }
    }
}
