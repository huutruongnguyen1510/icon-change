//
//  UIColor.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var lightCoral: UIColor {
        return hex("FF8282")
    }
    
    static var nero: UIColor {
        return hex("242424")
    }
    
    static var wePeep: UIColor {
        return hex("FED3D5")
    }
    
    static var none: UIColor {
        return hex("E0E0E0")
    }

    static func rgb(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) -> UIColor {
        let denominator: CGFloat = 255.0
        let color = UIColor(red: CGFloat(red) / denominator,
                            green: CGFloat(green) / denominator,
                            blue: CGFloat(blue) / denominator,
                            alpha: alpha)
        return color
    }
    
    static func hex(_ hexStr: String, alpha: CGFloat = 1) -> UIColor {
        let scanner = Scanner(string: hexStr.replacingOccurrences(of: "#", with: ""))
        var color: UInt32 = 0
        guard scanner.scanHexInt32(&color) else {
            return .white
        }
        let red = CGFloat((color & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((color & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(color & 0x0000FF) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    var hexString: String {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        let rgb: Int = (Int)(red * 255) << 16 | (Int)(green * 255) << 8 | (Int)(blue * 255) << 0
        
        return String(format: "#%06x", rgb)
    }
}
