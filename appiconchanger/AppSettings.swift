//
//  AppSettings.swift
//  BaseProject
//
//  Created by nguyen.viet.luy on 7/17/20.
//  Copyright © 2020 nguyen.viet.luy. All rights reserved.
//

import UIKit
import WidgetKit

enum AppSettings {
    static func openUrl(urlString: String) {
        print("appRelate urlScheme: ", urlString)
        if let url = URL(string: urlString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    static func writeContents(widget: WidgetObj) {
        guard widget.isSelected else {
            return
        }
        
        let backgroundImage = UIImage(data: widget.backgroundImage?.photo ?? Data())?.resizedToMB(MB: 5)?.pngData()
        
        let widgetContents = [
            WidgetContent(backgroundImage: backgroundImage,
                          textIconColor: widget.textIconColor,
                          backgroundColor: widget.backgroundColor,
                          arrayIconObj: widget.arrayIcon.map {
                            let appIcon = UIImage(data: $0.appIcon.photo)?.resizedToMB(MB: 1)?.pngData() ?? Data()
                            return IconInWidgetModel(appIcon: appIcon,
                                                     appName: $0.appName,
                                                     appRelateScheme: $0.appRelateScheme,
                                                     launcherType: $0.launcherType)})]
        var pathComponent: String = ""
        switch widget.widgetType {
        case .Medium:
            pathComponent = PathContentWidgetType.medium
        case .Large:
            pathComponent = PathContentWidgetType.large
        }
        let archiveURL = FileManager.sharedContainerURL()
            .appendingPathComponent(pathComponent)
        print(">>> \(archiveURL)")
        let encoder = JSONEncoder()
        if let dataToSave = try? encoder.encode(widgetContents) {
            do {
                try dataToSave.write(to: archiveURL)
            } catch {
                print("Error: Can't write contents")
                return
            }
        }

        if #available(iOS 14.0, *) {
            WidgetCenter.shared.reloadAllTimelines()
        } else {
            // Fallback on earlier versions
        }
    }
//    static func writeContents(widget: WidgetObj) {
//        guard widget.isSelected else {
//            return
//        }
//        let widgetContents = [WidgetContent(
//                                backgroundImage: widget.backgroundImage?.photo,
//                                arrayIconObj: widget.arrayIcon
//                                    .map { IconInWidgetModel(appIcon: $0.appIcon.photo,
//                                                             appName: $0.appName,
//                                                             appRelateName: $0.appRelateName,
//                                                             appRelateScheme: $0.appRelateScheme,
//                                                             textIcon: "",
//                                                             photoIcon: $0.photoIcon.photo)})]
//        var pathComponent: String = ""
//        switch widget.widgetType {
//        case .Medium:
//            pathComponent = PathContentWidgetType.medium
//        case .Large:
//            pathComponent = PathContentWidgetType.large
//        }
//        let archiveURL = FileManager.sharedContainerURL()
//            .appendingPathComponent(pathComponent)
//        print(">>> \(archiveURL)")
//        let encoder = JSONEncoder()
//        if let dataToSave = try? encoder.encode(widgetContents) {
//            do {
//                try dataToSave.write(to: archiveURL)
//            } catch {
//                print("Error: Can't write contents")
//                return
//            }
//        }
//        
//        WidgetCenter.shared.reloadAllTimelines()
//    }
}
